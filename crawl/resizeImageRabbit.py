import pika
import time
import json
import os
import glob
import boto3
from multiprocessing import Pool
import numpy as np 
from PIL import Image
import io
import os
credentials = pika.PlainCredentials('guest', 'guest')
parameters = pika.ConnectionParameters(host='localhost', port=5672, credentials=credentials, heartbeat=600, blocked_connection_timeout=300)
s3 = boto3.client('s3', aws_access_key_id='AKIAQ2Z7QFBSXS7EFHWN', aws_secret_access_key='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')

def make_square(im, min_size=384, fill_color=(0, 0, 0)):
    x, y = im.size
    size = max(x, y)
    new_im = Image.new('RGB', (size, size), fill_color)
    new_im.paste(im, (int((size - x) / 2), int((size - y) / 2)))
    new_im = new_im.resize((min_size, min_size), Image.Resampling.LANCZOS)
    return new_im

def resizeImage(info):
    path, save_path = info[0], info[1]
    try:
        image = Image.open(path)
        image = make_square(image)
        image.save(save_path)
    except Exception as e:
        print("Faile {}".format(info))
def on_receive_message(ch, method, props, body):
    data = body.decode('utf8')
    
    print(data)
    speciesName, imageName = os.path.basename(data).split("_")[0], os.path.basename(data).split("_")[1]
    if not os.path.exists(f'/mnt/89bd7cdc-f33c-4124-b444-df205e725bcd/plantTrain/data/{speciesName}'):
        os.mkdir(f'/mnt/89bd7cdc-f33c-4124-b444-df205e725bcd/plantTrain/data/{speciesName}')
    resizeImage((data, f'/mnt/89bd7cdc-f33c-4124-b444-df205e725bcd/plantTrain/data/{speciesName}/{imageName}'))
    ch.basic_ack(delivery_tag=method.delivery_tag)
    os.system('rm  "{}"'.format(data))
def consumer():
    try:
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.basic_qos(prefetch_count=1)
        channel.basic_consume('resizeImage', on_receive_message)
        print('Start receive message...')
        try:
            if channel:
                channel.start_consuming()
        except KeyboardInterrupt:
            channel.stop_consuming()
        if connection:
            connection.close()
    except Exception as e:
        print("Caught exception: %s" % repr(e))
        time.sleep(30)
        consumer()


consumer()