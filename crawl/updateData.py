import requests
import json
import urllib
from multiprocessing import Pool
import os
import glob

def crawlImage(info):
    url, local_path = info[0], info[1]
    try:
        urllib.request.urlretrieve(url, local_path)
    except Exception as e:
        print(url)
def speciesUpdate(path_json):
    with open(path_json, "r") as file:
        df_species = json.load(file)
        file.close()
    name = os.path.basename(path_json)[:-5]
    if (not os.path.exists(f'../plantTrain/data/{name}')):
        os.mkdir(f'../plantTrain/data/{name}')
    images = df_species["images"]
    p = Pool(20)
    list_images = []
    for subimage in images:
        for img in images[subimage][:(min(300, len(images[subimage]) - 1))]:
            list_images.append((img["o"], f'../plantTrain/data/{name}/{img["id"]}.jpg'))
    p.map(crawlImage, list_images)

def listSpeciesLessImage():
    df_final_species = json.load(open("json/Summary_Final_Species.json"))
    finalSpecies = {}
    for species in df_final_species:
        finalSpecies.update({species["name"] : species["Region"]})
    listSpeciesTrainDowloaded = glob.glob(f'../plantTrain/data/*')
    
