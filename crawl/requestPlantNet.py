import requests

# url = "https://api.plantnet.org/v1/projects/weeds/species/Abutilon%20theophrasti%20Medik."
def requestPlant(url):
  payload={}
  headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'vi,vi-VN;q=0.9,en;q=0.8',
    'Cache-Control': 'max-age=0',
    'Connection': 'keep-alive',
    'Cookie': '_gid=GA1.2.1619522854.1653272389; _ga=GA1.2.675500818.1653272389; _ga_RP3CVD3ZD1=GS1.1.1653275556.2.0.1653275556.0',
    'Host': 'api.plantnet.org',
    'If-Modified-Since': 'Mon, 23 May 2022 07:34:56 GMT',
    'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="101", "Google Chrome";v="101"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Linux"',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'none',
    'Sec-Fetch-User': '?1',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.64 Safari/537.36'
  }

  response = requests.request("GET", url, headers=headers, data=payload)
  return response.json()




