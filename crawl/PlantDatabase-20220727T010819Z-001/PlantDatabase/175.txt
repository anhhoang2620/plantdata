<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Fagus grandifolia</h1>
						<h2>American Beech</h2>
						<h3>Fagaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/faggra00.jpg' target='_blank'><img src='plantPhotos/faggra00.jpg' /></a><a href='plantPhotos/faggra01.jpg' target='_blank'><img src='plantPhotos/faggra01.jpg' /></a><a href='plantPhotos/faggra02.jpg' target='_blank'><img src='plantPhotos/faggra02.jpg' /></a><a href='plantPhotos/faggra03.jpg' target='_blank'><img src='plantPhotos/faggra03.jpg' /></a><a href='plantPhotos/faggra04.jpg' target='_blank'><img src='plantPhotos/faggra04.jpg' /></a><a href='plantPhotos/faggra05.jpg' target='_blank'><img src='plantPhotos/faggra05.jpg' /></a><a href='plantPhotos/faggra06.jpg' target='_blank'><img src='plantPhotos/faggra06.jpg' /></a><a href='plantPhotos/faggra07.jpg' target='_blank'><img src='plantPhotos/faggra07.jpg' /></a><a href='plantPhotos/faggra08.jpg' target='_blank'><img src='plantPhotos/faggra08.jpg' /></a><a href='plantPhotos/faggra09.jpg' target='_blank'><img src='plantPhotos/faggra09.jpg' /></a><a href='plantPhotos/faggra10.jpg' target='_blank'><img src='plantPhotos/faggra10.jpg' /></a><a href='plantPhotos/faggra11.jpg' target='_blank'><img src='plantPhotos/faggra11.jpg' /></a><a href='plantPhotos/faggra12.jpg' target='_blank'><img src='plantPhotos/faggra12.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT></P>    <UL>      <LI>native to eastern North America, from New Brunswick to Florida</LI>      <LI>zone 4</LI>    </UL>     <P><FONT SIZE="4">Habit      and Form</FONT></P>    <UL>      <LI>a wide-spreading, deciduous tree</LI>      <LI>trunk is short and branches generally touch ground</LI>      <LI>50&#39; to 70&#39; tall and up to 120&#39; wide</LI>      <LI>if tree is in crowded conditions, tree will assume a more upright        habit</LI>      <LI>slow growth rate</LI>      <LI>medium texture</LI>    </UL>                <P><FONT SIZE="4">Summer      Foliage</FONT></P>    <UL>      <LI>alternate leaf arrangement</LI>      <LI>simple, serrate leaves</LI>      <LI>glossy dark green leaf above &amp; light green below</LI>      <LI>leaves are glabrous on underside with tufts of hair in the axils of        the veins &amp; along midrib</LI>      <LI>11 - 15 vein pairs </LI>    </UL>        <P></P>        <P> <FONT SIZE="4">Autumn      Foliage</FONT></P>    <UL>      <LI>fall color is bronze</LI>      <LI>very attractive</LI>      <LI>leaves tend to persist through winter</LI>    </UL><P></P>           <P><FONT SIZE="4">Flowers</FONT></P>    <UL>      <LI>monoecious</LI>      <LI>usually flowers in April or Early May</LI>      <LI>not ornamentally important </LI>    </UL>        <P><FONT SIZE="4">Fruit</FONT></P>    <UL>      <LI>three-winged nut</LI>      <LI>nut is enclosed in a spiky involucre</LI>      <LI>spikes are recurved</LI>      <LI>found singly or in 2 &amp; 3&#39;s</LI>      <LI>edible</LI>      <LI>persist into winter</LI>    </UL><P>   </P>           <P><FONT SIZE="4">Bark</FONT></P>    <UL>      <LI>light gray bark</LI>      <LI>smooth</LI>      <LI>wrinkled appearance to bark </LI>    </UL>        <P>   </P>        <P><FONT SIZE="4">Culture</FONT></P>    <UL>      <LI>transplant during dormant season</LI>      <LI>moist, well-drained, acidic soil</LI>      <LI>does not like excessively wet soils</LI>      <LI>full sun best</LI>      <LI>shallow, wide root system</LI>      <LI>prune in early summer or early fall</LI>    </UL>        <P><FONT SIZE="4">Landscape      Use</FONT></P>    <UL>      <LI>naturalized areas</LI>      <LI>large open spaces</LI>      <LI>lawn tree</LI>      <LI>parks</LI>      <LI>golf courses</LI>    </UL>        <P><FONT SIZE="4">Liabilities</FONT></P>    <UL>      <LI>branches generally touch ground</LI>      <LI>grass tends not to grow under tree</LI>      <LI>does not like wet soils</LI>      <LI>tends to sucker</LI>      <LI>fruit can be a litter problem</LI>      <LI>few minor pest problems including: powdery mildew, aphids, canker,        and beech bark disease</LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT></P>    <UL>      <LI>serrated leaf margins</LI>      <LI>smooth, gray wrinkled bark</LI>      <LI>dead leaves persist on tree through winter</LI>      <LI>tree is very wide and branches generally touch ground</LI>      <LI>alternate leaf arrangement</LI>      <LI>three-winged fruit with a spiky involucre</LI>    </UL><P>      </P>   <P><FONT SIZE="4">Propagation</FONT></P>    <UL>      <LI>by seed</LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT>    </P>    <UL>      <LI>none</LI>    </UL>  </div>
					<script type="text/javascript">document.title = "Fagus grandifolia, American Beech - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>