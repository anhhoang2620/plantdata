<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Viburnum plicatum var. tomentosum</h1>
						<h2>Doublefile Viburnum</h2>
						<h3>Caprifoliaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/vibpli00.jpg' target='_blank'><img src='plantPhotos/vibpli00.jpg' /></a><a href='plantPhotos/vibpli01.jpg' target='_blank'><img src='plantPhotos/vibpli01.jpg' /></a><a href='plantPhotos/vibpli02.jpg' target='_blank'><img src='plantPhotos/vibpli02.jpg' /></a><a href='plantPhotos/vibpli03.jpg' target='_blank'><img src='plantPhotos/vibpli03.jpg' /></a><a href='plantPhotos/vibpli04.jpg' target='_blank'><img src='plantPhotos/vibpli04.jpg' /></a><a href='plantPhotos/vibpli05.jpg' target='_blank'><img src='plantPhotos/vibpli05.jpg' /></a><a href='plantPhotos/vibpli06.jpg' target='_blank'><img src='plantPhotos/vibpli06.jpg' /></a><a href='plantPhotos/vibpli07.jpg' target='_blank'><img src='plantPhotos/vibpli07.jpg' /></a><a href='plantPhotos/vibpli08.jpg' target='_blank'><img src='plantPhotos/vibpli08.jpg' /></a><a href='plantPhotos/vibpli09.jpg' target='_blank'><img src='plantPhotos/vibpli09.jpg' /></a><a href='plantPhotos/vibpli10.jpg' target='_blank'><img src='plantPhotos/vibpli10.jpg' /></a><a href='plantPhotos/vibpli100.jpg' target='_blank'><img src='plantPhotos/vibpli100.jpg' /></a><a href='plantPhotos/vibpli103.jpg' target='_blank'><img src='plantPhotos/vibpli103.jpg' /></a><a href='plantPhotos/vibpli11.jpg' target='_blank'><img src='plantPhotos/vibpli11.jpg' /></a><a href='plantPhotos/vibpli12.jpg' target='_blank'><img src='plantPhotos/vibpli12.jpg' /></a><a href='plantPhotos/vibpli13.jpg' target='_blank'><img src='plantPhotos/vibpli13.jpg' /></a><a href='plantPhotos/vibpli14.jpg' target='_blank'><img src='plantPhotos/vibpli14.jpg' /></a><a href='plantPhotos/vibpli15.jpg' target='_blank'><img src='plantPhotos/vibpli15.jpg' /></a><a href='plantPhotos/vibpli16.jpg' target='_blank'><img src='plantPhotos/vibpli16.jpg' /></a><a href='plantPhotos/vibpli17.jpg' target='_blank'><img src='plantPhotos/vibpli17.jpg' /></a><a href='plantPhotos/vibpli18.jpg' target='_blank'><img src='plantPhotos/vibpli18.jpg' /></a><a href='plantPhotos/vibpli19.jpg' target='_blank'><img src='plantPhotos/vibpli19.jpg' /></a><a href='plantPhotos/vibpli23.jpg' target='_blank'><img src='plantPhotos/vibpli23.jpg' /></a><a href='plantPhotos/vibpli24.jpg' target='_blank'><img src='plantPhotos/vibpli24.jpg' /></a><a href='plantPhotos/vibpli25.jpg' target='_blank'><img src='plantPhotos/vibpli25.jpg' /></a><a href='plantPhotos/vibpli26.jpg' target='_blank'><img src='plantPhotos/vibpli26.jpg' /></a><a href='plantPhotos/vibpli27.jpg' target='_blank'><img src='plantPhotos/vibpli27.jpg' /></a><a href='plantPhotos/vibpli28.jpg' target='_blank'><img src='plantPhotos/vibpli28.jpg' /></a><a href='plantPhotos/vibpli29.jpg' target='_blank'><img src='plantPhotos/vibpli29.jpg' /></a><a href='plantPhotos/vibpli30.jpg' target='_blank'><img src='plantPhotos/vibpli30.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'><P><font size="4">Special   Note: </font>This profile page covers both <i>Viburnum plicatum</i> and <i>Viburnum   plicatum </i>var. <i>tomentosum</i>. These plants are identical in most respects,   except <i>V. plicatum</i> bears clusters of solely sterile flowers 2-3 weeks   later than <i>V. p.</i> var. <i>tomentosum</i>. The flat-topped flower clusters   of var.<i> tomentosum</i>-types generally feature a ring of large, sterile florets   that surround the central mass of small, fertile flowers. </P><P><FONT SIZE="4">Habitat</FONT></P>    <UL>        <LI>native to China and Japan</LI>      <LI>zone 5</LI>    </UL>        <P><FONT SIZE="4">Habit      and Form</FONT></P>    <UL>      <LI>a deciduous medium-sized shrub</LI>      <LI>multistemmed</LI>      <LI>8&#39; to 10&#39; tall with a wider width</LI>      <LI>horizontal branching gives a layered appearance</LI>      <LI>broad rounded form</LI>      <LI>medium texture</LI>      <LI>medium growth rate</LI>    </UL>        <P>                                    </P>        <P><FONT SIZE="4">Summer      Foliage</FONT></P>    <UL>      <LI>deciduous, simple leaves</LI>      <LI>opposite leaf arrangement</LI>      <LI>ovate leaf shape, 2&quot; to 4&quot; in length and up to 2&quot; wide</LI>      <LI>pointed apex and rounded leaf base</LI>      <LI>serrated leaf margin</LI>      <LI>8 to 12 pairs of veins</LI>      <LI>pubescent underside</LI>      <LI>dark green leaf color</LI>      <LI>leaves emerge in early spring</LI>    </UL>        <P>            </P>        <P> <FONT SIZE="4">Autumn      Foliage</FONT></P>    <UL>      <LI>reddish purple fall color </LI>    </UL>        <P>                  </P>        <P><FONT SIZE="4">Flowers</FONT></P>    <UL>      <LI>white flowers</LI>      <LI>individual flowers form a large flat-topped cyme</LI>      <LI>cymes are composed of showy infertile flowers and unshowy fertile        flowers</LI>      <LI>sterile flowers surround fertile flowers</LI>      <LI>up to 4&quot; in diameter</LI>      <LI>blooms in May</LI>      <LI>no fragrance</LI>    </UL>        <P>                                          </P>         <P><FONT SIZE="4">Fruit</FONT></P>    <UL>      <LI>small, egg-shaped drupe</LI>      <LI>red maturing to black</LI>      <LI>matures August</LI>      <LI>birds love fruit</LI>    </UL><p>   </P>              <P><FONT SIZE="4">Bark</FONT></P>    <UL>      <LI>gray-brown color</LI>      <LI>young stems are tomentose</LI>      <LI>orange lenticels</LI>      <LI>fishbone pattern to branching</LI>      <LI>2 types of buds: vegetative and reproductive</LI>      <LI>vegetative buds are naked and pubescent</LI>      <LI>flower buds are valvate, hairy and appressed to stem</LI>    </UL>        <P><FONT SIZE="4">Culture</FONT></P>    <UL>      <LI>transplants easily</LI>      <LI>prefers moist, well-drained soil</LI>      <LI>soil adaptable</LI>      <LI>full sun to partial shade</LI>    </UL>        <P><FONT SIZE="4">Landscape      Use</FONT></P>    <UL>      <LI>shrub borders</LI>      <LI>for flowers</LI>      <LI>mass plantings</LI>      <LI>for fruiting effect</LI>      <LI>to attract birds</LI>      <LI>specimen</LI>      <LI>fall color</LI>    </UL><P>      </P>            <P><FONT SIZE="4">Liabilities</FONT></P>    <UL>      <LI>none serious</LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT></P>    <UL>      <LI>vegetative buds are naked and pubescent</LI>      <LI>flower buds are valvate, hairy and appressed to stem</LI>      <LI>small, black, egg-shaped drupe </LI>      <LI>cymes are composed of showy infertile flowers and unshowy fertile        flowers</LI>      <LI>sterile flowers surround fertile flowers </LI>      <LI>8&#39; to 10&#39; tall with a wider width</LI>      <LI>horizontal branching gives a layered appearance</LI>    </UL>        <P>      </P>     <P><FONT SIZE="4">Propagation</FONT></P>    <UL>      <LI>by cuttings </LI>      <LI>by seed</LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT> </P><P>Various cultivars are assigned to either <i>Viburnum plicatum</i> and <i>Viburnum   plicatum</i> var. <i>tomentosum</i>. For the purpose of simplicity, all are   listed here under <i>V. p.</i> var. <i>tomentosum</i>. In general, the only   difference between the two types are the slightly later bloom period of the   var.<i> tomentosum</i>-types and the rounded balls of sterile flowers found   on <i>V. plicatum</i> (these result in little or no fruit). The flower heads   of var. <i>tomentosum</i>-types tend to be &quot;lacecap&quot; in their organization,   with an outer ring of large sterile florets and a central mass of small fertile   flowers that result in red-black fruit. The association of each cultivar will   be indicated, though the cultivation needs of all types are identical.</P><P><b>&#39;Fireworks&#39;</b> - A var. <i>tomentosum</i> selection, this large-growing   8&#39;-12&#39; tall plant has large flower clusters and persistent red pedicels (flower   stalks) that lend added interest.</P><P><b>&#39;Kern&#39;s Pink&#39;</b> (probably the same as <b>&#39;Roseace&#39;</b> and <b>&#39;Pink Sensation&#39;</b>;   similar to<b> &#39;Mary Milton&#39;</b>, which is also known as <b>&#39;Mary Melton&#39;</b>)   - This old <i>V. plicatum</i>-form has seen a recent resurgence in the catalogs   of specialty nurseries. It may grow to 10&#39; tall and is most notable for its   round heads of pinkish flowers. The degree of pink color in the bloom clusters   may vary, but in general the plants are attractive in flower.</P><P><b>&#39;Leach&#39;s Compacta&#39;</b> - This <i>V. plicatum</i> variant is notable for   its compact, dense habit to 4&#39; tall and wide with white &quot;snowball&quot;   flower clusters and reddish fall color.</P><P><b>&#39;Mariesii&#39; </b>- An old, large-growing (to 10&#39; tall) selection of var. <i>tomentosum</i>,   this plant has distinctly layered horizontal branches that serve as a good stage   for the large, white sterile blooms that are raised on longer stems. Fruit set   can be very good and the foliage may turn a deep red in fall.</P><P><b>&#39;Newzam&#39;</b> (<b>Newport</b>&reg;, also listed as<b> &#39;Nanum Newport&#39;</b>)   - This an older, dwarf form of <i>V. plicatum</i> that grows densely and compactly   to 4&#39; tall with a spread of 6&#39;. It blooms with white &quot;snowball&quot;-type   clusters in May and offers good red fall color. It rarely fruits. <b>&#39;Trizam&#39;</b>   (<b>Triumph</b>&#153;) is derived from this selection and appears very similar   in most regards.</P><P><b>&#39;Pink Beauty&#39;</b> - An unusual var.<i> tomentosum</i> selection, this plant   has light pink flower petals or white petals that are edged pink. The effect   may vary from season to season and depending on sun exposure, but nonetheless   it is unique. The plant tends to grow smaller than the species and may set good   quantities of red fruit.</P><P><b>&#39;Popcorn&#39; </b>- Probably a <i>V. plicatum</i>-type, this cultivar is notable   for its rounded clusters of pure white flowers that apparently resemble popcorn   to some degree. It is a smaller grower to 8&#39; tall with sporadic production of   red fruit.</P><P><b>&#39;Shasta&#39; </b>- A var. <i>tomentosum</i>-type, this large-growing plant sets   the standard against which all other selections are measured. This U.S. National   Arboretum has become tremendously popular since its 1979 introduction and is   widely available in the trade. The spreading, layered horizontal branches are   covered by clusters of pure white flowers in May and are followed by red-black   fruit. The plant grows large, to at least 10&#39; tall with a wide spread. It should   not be pruned to maintain best appearance. <b>&#39;Shasta Variegated&#39;</b> is a sport   with leaves that are margined yellowish green.</P><P><b>&#39;Shoshoni&#39;</b> - This variant of &#39;Shasta&#39; offers similar ornamental attributes   on a much smaller scale. The plant grows wider than tall, to 5&#39; high with a   spread of 8&#39;. It is a dense, compact plant common in the industry and suitable   for small garden situations.</P><P><b>&#39;Summer Snowflake&#39;</b> (also known as <b>&#39;Fujisanensis&#39;</b>, probably identical   to <b>&#39;Everblooming&#39;</b>, <b>&#39;Nanum Semperflorens&#39;</b>, <b>&#39;Nanum&#39;</b>, <b>&#39;Watanabei&#39;   </b>and<b> &#39;Watanabe&#39;</b>) - This popular var. <i>tomentosum</i>-type plant   is thoroughly confused in the trade, as it can be sold with up to seven different   names. By most accounts, these monikers all seem to refer to the same plant.   The plant itself is a fine selection, most notable for its white blooms that   are produced continuously from May to almost Thanksgiving. The plant is generally   compact and grows to 6&#39; tall or slightly more. Fruit set can be sporadic and   inconsistent, plus the plant may develop reddish fall color.</P></div>
					<script type="text/javascript">document.title = "Viburnum plicatum var. tomentosum, Doublefile Viburnum - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>