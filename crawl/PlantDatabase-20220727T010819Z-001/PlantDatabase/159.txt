<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Diospyros virginiana</h1>
						<h2>Common Persimmon</h2>
						<h3>Ebenaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/diovir00.jpg' target='_blank'><img src='plantPhotos/diovir00.jpg' /></a><a href='plantPhotos/diovir01.jpg' target='_blank'><img src='plantPhotos/diovir01.jpg' /></a><a href='plantPhotos/diovir09.jpg' target='_blank'><img src='plantPhotos/diovir09.jpg' /></a><a href='plantPhotos/diovir10.jpg' target='_blank'><img src='plantPhotos/diovir10.jpg' /></a><a href='plantPhotos/diovir18.jpg' target='_blank'><img src='plantPhotos/diovir18.jpg' /></a><a href='plantPhotos/diovir50.jpg' target='_blank'><img src='plantPhotos/diovir50.jpg' /></a><a href='plantPhotos/diovir51.jpg' target='_blank'><img src='plantPhotos/diovir51.jpg' /></a><a href='plantPhotos/diovir52.jpg' target='_blank'><img src='plantPhotos/diovir52.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT>    </P>    <UL>      <LI>native to eastern North America</LI>      <LI>hardy to zone 4 </LI>    </UL>                <P><FONT SIZE="4">Habit      and Form</FONT> </P>    <UL>      <LI>a deciduous tree </LI>      <LI>35&#39; to 40&#39; tall</LI>      <LI>pyramidal in youth becoming open with age</LI>      <LI>medium coarse texture</LI>      <LI>fast growth rate</LI>    </UL>        <P><FONT SIZE="4">Summer   Foliage</FONT> </P>    <UL>      <LI>alternate leaf arrangement</LI>      <LI>simple, deciduous leaves</LI>      <LI>2&quot; to 6&quot; long</LI>      <LI>ovate leaf shape</LI>      <LI>dark, glossy green leaf color</LI>      <LI>pubescent, lighter underside</LI>    </UL><P>   </P>            <P> <FONT SIZE="4">Autumn      Foliage</FONT> </P>    <UL>      <LI>yellow to red fall color</LI>      <LI>variable</LI>    </UL>        <P><FONT SIZE="4">Flowers</FONT>    </P>    <UL>      <LI>dioecious</LI>      <LI>blooms early spring</LI>      <LI>not ornamentally important</LI>    </UL>        <P><FONT SIZE="4">Fruit</FONT>    </P>    <UL>      <LI>only on female trees</LI>      <LI>red orange multiple fruit</LI>      <LI>1&quot; to 2&quot; long</LI>      <LI>matures in September</LI>      <LI>edible</LI>      <LI>attracts wildlife</LI>    </UL>        <P><FONT SIZE="4">Bark</FONT> </P>    <UL>  <LI>one of the most distinctive features of the species</LI>  <LI>mature bark develops a deep, blocky pattern</LI>  <LI>dark gray or brown; almost black at times</LI>  <LI>looks much like alligator hide</LI>  <LI>similar in appearance to old <i>Cornus florida</i> bark, but more dramatic</LI></UL><P></P>        <P><FONT SIZE="4">Culture</FONT>    </P>    <UL>  <LI>prefers moist, well-drained soil</LI>  <LI>often found growing in sandy, infertile soils</LI>  <LI>pH adaptable</LI>  <LI>full sun </LI></UL>        <P><FONT SIZE="4">Landscape      Uses</FONT> </P>    <UL>      <LI>for attractive foliage</LI>      <LI>for fall color</LI>      <LI>park tree</LI>      <LI>for fruit</LI>      <LI>to attract wildlife</LI>    </UL>        <P><FONT SIZE="4">Liabilities</FONT>    </P>    <UL>      <LI>leaf spot</LI>      <LI>difficult to transplant</LI>      <LI>suckers </LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT> </P>    <UL>      <LI>no terminal buds</LI>      <LI>lateral buds are small and black</LI>    </UL>        <P><FONT SIZE="4">Propagation</FONT>    </P>    <UL>      <LI>by seed</LI>      <LI>by grafting</LI>      <LI>by root cutting </LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT></P><P><b>&#39;Meader&#39; </b>- The most commonly available cultivar, popular for its extreme   hardiness and ability to fruit without a pollinator. Reaches 30&#39;-40&#39; tall and   also serves as a good ornamental plant with handsome fall foliage. Developed   at the University of New Hampshire.</P><P>Other common fruiting varieties include <b>&#39;John Rick&#39;</b>, <b>&#39;Early Golden&#39;</b>,   <b>&#39;Garrettson&#39; </b>and<b> &#39;Killen&#39;</b>. </P></div>
					<script type="text/javascript">document.title = "Diospyros virginiana, Common Persimmon - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>