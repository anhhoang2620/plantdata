<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Populus alba</h1>
						<h2>White Poplar, Silver-leaved Poplar</h2>
						<h3>Salicaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/popalb02.jpg' target='_blank'><img src='plantPhotos/popalb02.jpg' /></a><a href='plantPhotos/popalb50.jpg' target='_blank'><img src='plantPhotos/popalb50.jpg' /></a><a href='plantPhotos/popalb51.jpg' target='_blank'><img src='plantPhotos/popalb51.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT></P>    <UL>  <LI>native to central and southern Europe to western Siberia</LI>  <LI>hardy to zone 3</LI>  <LI><font color="#FF0000"><u>Special Note:</u> This species has demonstrated     an invasive tendency in Connecticut, meaning it may escape from cultivation     and naturalize in minimally managed areas. For more information, .</font></LI></UL>        <P><FONT SIZE="4">Habit      and Form</FONT></P>    <UL>      <LI>a deciduous tree</LI>      <LI>widespreading</LI>      <LI>60&#39; to 100&#39; tall</LI>      <LI>40&#39; to 60&#39; wide</LI>      <LI>oval, open irregular crown</LI>      <LI>coarse texture</LI>      <LI>rapid growth rate</LI>    </UL>        <P><FONT SIZE="4">Summer   Foliage</FONT></P>    <UL>      <LI>alternate leaf arrangement</LI>      <LI>simple, deciduous leaves</LI>      <LI>lobed leaves with undulate leaf margins</LI>      <LI>2&quot; to 5&quot; long</LI>      <LI>pubescent, white underside</LI>      <LI>tomentose petiole</LI>      <LI>coarsely toothed</LI>      <LI>dark green leaf color</LI>    </UL>                <P> <FONT SIZE="4">Autumn      Foliage</FONT></P>    <UL>      <LI>yellow fall color</LI>    </UL>        <P><FONT SIZE="4">Flowers</FONT></P>    <UL>      <LI>flowers before leafs out</LI>      <LI>catkins</LI>      <LI>not ornamentally important</LI>      <LI>yellow</LI>    </UL>        <P><FONT SIZE="4">Fruit</FONT></P>    <UL>      <LI>elongated capsule</LI>      <LI>0.2&quot; to 0.3&quot; long</LI>      <LI>dry, brown </LI>      <LI>litter</LI>      <LI>two-valved</LI>    </UL>        <P><FONT SIZE="4">Bark</FONT></P>    <UL>      <LI>showy, white bark</LI>      <LI>smooth</LI>      <LI>older bark becomes ridged and furrowed</LI>      <LI>gray stems</LI>      <LI>marked with dark splotches</LI>    </UL>        <P><FONT SIZE="4">Culture</FONT></P>    <UL>      <LI>grows best in full sun</LI>      <LI>salt and drought tolerant</LI>      <LI>suckers</LI>      <LI>prune in summer</LI>      <LI>air pollution tolerant</LI>      <LI>prefers moist, deep, loam</LI>      <LI>pH adaptable</LI>    </UL>        <P><FONT SIZE="4">Landscape      Use</FONT></P>    <UL>      <LI>large shade tree</LI>      <LI>park tree</LI>      <LI>good urban tree </LI>      <LI>for decoration from showy bark</LI>    </UL>        <P><FONT SIZE="4">Liabilities</FONT></P>    <UL>      <LI>crown gall</LI>      <LI>trunk cankers</LI>      <LI>brittle wood</LI>      <LI>roots can cause problems with drainage, sidewalks, etc.</LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT></P>    <UL>      <LI>small, imbricate buds appressed</LI>      <LI>light chestnut brown buds, tomentose</LI>      <LI>showy, white bark</LI>      <LI>pubescent and white leaf undersides</LI>      <LI>coarsely toothed leaf margins</LI>      <LI>catkins</LI>    </UL>                <P><FONT SIZE="4">Propagation</FONT></P>    <UL>      <LI>by cuttings </LI>      <LI>by seed</LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT>    </P>        <P><b>&#39;Globosa&#39; </b>- Forming a large rounded multi-stemmed shrub, this dwarf   cultivar bears young leaves that are pinkish with a gray pubescence underneath.</P><P><B>&#39;Pyramidalis&#39;</B> - Called &quot;Bolleana Poplar&quot;, this is perhaps   the most popular cultivar. It grows as a harshly fastigiate form with vertical   branching. It can reach 60&#39; tall and only 10&#39; wide. It is prone to many diseases   and is not a viable landscape choice.</P><P><b>&#39;Richardii&#39;</b> - A slow-growing tree or cut-back shrub, this plant is notable   for its yellow leaves.</P></div>
					<script type="text/javascript">document.title = "Populus alba, White Poplar, Silver-leaved Poplar - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>