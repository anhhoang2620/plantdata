<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Acer platanoides</h1>
						<h2>Norway Maple</h2>
						<h3>Aceraceae</h3>
					<div id='photoStrip'><a href='plantPhotos/acepla00.jpg' target='_blank'><img src='plantPhotos/acepla00.jpg' /></a><a href='plantPhotos/acepla01.jpg' target='_blank'><img src='plantPhotos/acepla01.jpg' /></a><a href='plantPhotos/acepla02.jpg' target='_blank'><img src='plantPhotos/acepla02.jpg' /></a><a href='plantPhotos/acepla03.jpg' target='_blank'><img src='plantPhotos/acepla03.jpg' /></a><a href='plantPhotos/acepla04.jpg' target='_blank'><img src='plantPhotos/acepla04.jpg' /></a><a href='plantPhotos/acepla05.jpg' target='_blank'><img src='plantPhotos/acepla05.jpg' /></a><a href='plantPhotos/acepla06.jpg' target='_blank'><img src='plantPhotos/acepla06.jpg' /></a><a href='plantPhotos/acepla08.jpg' target='_blank'><img src='plantPhotos/acepla08.jpg' /></a><a href='plantPhotos/acepla10.jpg' target='_blank'><img src='plantPhotos/acepla10.jpg' /></a><a href='plantPhotos/acepla101.jpg' target='_blank'><img src='plantPhotos/acepla101.jpg' /></a><a href='plantPhotos/acepla102.jpg' target='_blank'><img src='plantPhotos/acepla102.jpg' /></a><a href='plantPhotos/acepla11.jpg' target='_blank'><img src='plantPhotos/acepla11.jpg' /></a><a href='plantPhotos/acepla12.jpg' target='_blank'><img src='plantPhotos/acepla12.jpg' /></a><a href='plantPhotos/acepla13.jpg' target='_blank'><img src='plantPhotos/acepla13.jpg' /></a><a href='plantPhotos/acepla14.jpg' target='_blank'><img src='plantPhotos/acepla14.jpg' /></a><a href='plantPhotos/acepla15.jpg' target='_blank'><img src='plantPhotos/acepla15.jpg' /></a><a href='plantPhotos/acepla16.jpg' target='_blank'><img src='plantPhotos/acepla16.jpg' /></a><a href='plantPhotos/acepla17.jpg' target='_blank'><img src='plantPhotos/acepla17.jpg' /></a><a href='plantPhotos/acepla18.jpg' target='_blank'><img src='plantPhotos/acepla18.jpg' /></a><a href='plantPhotos/acepla19.jpg' target='_blank'><img src='plantPhotos/acepla19.jpg' /></a><a href='plantPhotos/acepla20.jpg' target='_blank'><img src='plantPhotos/acepla20.jpg' /></a><a href='plantPhotos/acepla21.jpg' target='_blank'><img src='plantPhotos/acepla21.jpg' /></a><a href='plantPhotos/acepla22.jpg' target='_blank'><img src='plantPhotos/acepla22.jpg' /></a><a href='plantPhotos/acepla23.jpg' target='_blank'><img src='plantPhotos/acepla23.jpg' /></a><a href='plantPhotos/acepla25.jpg' target='_blank'><img src='plantPhotos/acepla25.jpg' /></a><a href='plantPhotos/acepla26.jpg' target='_blank'><img src='plantPhotos/acepla26.jpg' /></a><a href='plantPhotos/acepla36.jpg' target='_blank'><img src='plantPhotos/acepla36.jpg' /></a><a href='plantPhotos/acepla51.jpg' target='_blank'><img src='plantPhotos/acepla51.jpg' /></a><a href='plantPhotos/acepla52.jpg' target='_blank'><img src='plantPhotos/acepla52.jpg' /></a><a href='plantPhotos/acepla53.jpg' target='_blank'><img src='plantPhotos/acepla53.jpg' /></a><a href='plantPhotos/acepla54.jpg' target='_blank'><img src='plantPhotos/acepla54.jpg' /></a><a href='plantPhotos/acepla55.jpg' target='_blank'><img src='plantPhotos/acepla55.jpg' /></a><a href='plantPhotos/acepla56.jpg' target='_blank'><img src='plantPhotos/acepla56.jpg' /></a><a href='plantPhotos/acepla81.jpg' target='_blank'><img src='plantPhotos/acepla81.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'><P><FONT SIZE="4">Habitat</FONT>   </P>   <UL>  <LI>native to Europe </LI>  <LI>widely naturalized in the United States</LI>  <LI>hardy to zone 3 </LI>  <LI><font color="#FF0000"><u>Special Note:</u> This species has demonstrated     an invasive tendency in Connecticut, meaning it may escape from cultivation     and naturalize in minimally managed areas. For more information, .</font></LI></UL>      <P><FONT SIZE="4">Habit    and Form</FONT></P>   <UL>    <LI>medium to large shade tree </LI>    <LI>deciduous </LI>    <LI>rounded crown, densely branched </LI>    <LI>typically 40&#39; to 60&#39; tall, but can reach 90&#39; </LI>    <LI>often wider than tall when open grown </LI>    <LI>grows rapidly when young </LI>    <LI>medium-coarse texture </LI>   </UL>      <P>            </P>      <P><FONT SIZE="4">Summer    Foliage</FONT> </P>   <UL>    <LI>opposite, 5-lobed leaves, 4&quot; to 7&quot; across </LI>    <LI>uniform dark green color </LI>    <LI>milky sap visible when petiole is broken</LI>   </UL>      <P>                </P>      <P><FONT SIZE="4">Autumn    Foliage</FONT> </P>   <UL>    <LI>often colors late (end of October) </LI>    <LI>can be a good, uniform yellow </LI>   </UL>      <P> </P>      <P><FONT SIZE="4">Flowers</FONT>   </P>   <UL>    <LI>blooms in April </LI>    <LI>numerous small, yellow-green flowers </LI>    <LI>showy, especially for early spring </LI>   </UL>      <P>    </P>      <P><FONT SIZE="4">Fruit</FONT></P>   <UL>    <LI>samaras, 1.5&quot; to 2&quot; long </LI>    <LI>turn green to tan </LI>   </UL>      <P>    </P>      <P><FONT SIZE="4">Bark</FONT>   </P>   <UL>    <LI>greyish-black </LI>    <LI>shallow ridges and furrows </LI>   </UL>      <P>            </P>      <P><FONT SIZE="4">Culture</FONT>   </P>   <UL>    <LI>adaptable to many soils </LI>    <LI>easily transplanted </LI>    <LI>full sun is best </LI>    <LI>tolerant of urban conditions for a maple </LI>   </UL>      <P><FONT SIZE="4">Landscape    Uses</FONT> </P>   <UL>    <LI>shade tree for lawns, parks, industrial parks </LI>    <LI>needs adequate space to develop </LI>   </UL>      <P>   </P>      <P><FONT SIZE="4">Liabilities</FONT>   </P>   <UL>    <LI>creates very dense shade which kills out turf </LI>    <LI>grows too large for residential landscapes </LI>    <LI>shallow root system damages sidewalks </LI>    <LI>verticillium wilt </LI>    <LI>can be prone to bark splitting </LI>    <LI>seeds freely, get many weed seedling trees</LI>   </UL>      <P>   </P>      <P><FONT SIZE="4">ID    Features</FONT> </P>   <UL>    <LI>milky sap in petiole and leaf veins </LI>    <LI>large, plump green-maroon buds (compare with <I>Acer pseudoplatanus</I>     which are always green) </LI>    <LI>leaves larger than those of <I>Acer campestre</I> </LI>    <LI>leaves broader than those of <I>Acer saccharum</I> </LI>   </UL>      <P>        </P>      <P><FONT SIZE="4">Propagation</FONT>   </P>   <UL>    <LI>by seed </LI>    <LI>cultivars by grafting, some by tissue culture </LI>   </UL>      <P><FONT SIZE="4">Cultivars/Varieties    </FONT>    <BR></P>      <P><B>&#39;Columnare&#39;, &#39;Erectum&#39;, &#39;Olmsted&#39;</B> - Some confusion exists as to    the exact characteristics of these 3 cultivars. All are upright, columnar    forms 10&#39; to 20&#39; wide and 40&#39; to 60&#39; tall. Generally comprised of a single    central trunk with short lateral branches. These forms of Norway maple are    good choices for street trees.    <BR>    <BR>             </P>      <P><B>&#39;Crimson King&#39;</B> - A very popular cultivar that many consider to    be overused. Leaves emerge bright crimson and deepen to maroon which    persists throughout the growing season. Slower growing than the species.    Fall color usually a bizarre brownish-maroon. </P>      <P>                    </P>      <P><B>&#39;Drummondii&#39;</B> - Possesses light green leaves with with a    prominent white edge. Somewhat slow growing, developing an oval to rounded    head. Frequently reverts and green sections must be removed or they will    overgrow variegated sections because of their greater vigor</P>      <P>       </P>      <P><b>&#39;Emerald Queen&#39; </b>- Increasingly popular for its upright, rounded habit   ( to 50&#39; tall and 40&#39; wide) and yellow fall color. Commonly planted.</P><P><B>&#39;Globosum&#39; </B>- A truly dwarf form (15&#39; to 18&#39;). Develops a compact, rounded,   dense head. Typically grafted at around 6 feet. An excellent choice for planting   beneath utility lines. <BR></P>      <P><B>&#39;Schwedleri&#39; </B>- An old cultivar that is a parent of many of the newer   red foliage forms. Grows larger and more rapidly than &#39;Crimson King&#39;. Foliage   emerges maroon, but turns black-green as it matures. Color effect is more subtle   than &#39;Crimson King&#39; and is easier to use in landscapes.</P><P><b>&#39;Summershade&#39;</b> - A handsome shade tree with a rounded mature form (to   40&#39; tall and wide) and good foliage substance. Marketed as being fast-growing   and resistant to summer heat. Fall color has proved to be inferior to other   forms.</P> </div>
					<script type="text/javascript">document.title = "Acer platanoides, Norway Maple - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>