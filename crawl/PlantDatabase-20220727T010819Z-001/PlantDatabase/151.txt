<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Cytisus scoparius</h1>
						<h2>Scotch Broom, Common Broom</h2>
						<h3>Leguminosae</h3>
					<div id='photoStrip'><a href='plantPhotos/cytsco00.jpg' target='_blank'><img src='plantPhotos/cytsco00.jpg' /></a><a href='plantPhotos/cytsco01.jpg' target='_blank'><img src='plantPhotos/cytsco01.jpg' /></a><a href='plantPhotos/cytsco02.jpg' target='_blank'><img src='plantPhotos/cytsco02.jpg' /></a><a href='plantPhotos/cytsco03.jpg' target='_blank'><img src='plantPhotos/cytsco03.jpg' /></a><a href='plantPhotos/cytsco04.jpg' target='_blank'><img src='plantPhotos/cytsco04.jpg' /></a><a href='plantPhotos/cytsco05.jpg' target='_blank'><img src='plantPhotos/cytsco05.jpg' /></a><a href='plantPhotos/cytsco06.jpg' target='_blank'><img src='plantPhotos/cytsco06.jpg' /></a><a href='plantPhotos/cytsco07.jpg' target='_blank'><img src='plantPhotos/cytsco07.jpg' /></a><a href='plantPhotos/cytsco08.jpg' target='_blank'><img src='plantPhotos/cytsco08.jpg' /></a><a href='plantPhotos/cytsco35.jpg' target='_blank'><img src='plantPhotos/cytsco35.jpg' /></a><a href='plantPhotos/cytsco36.jpg' target='_blank'><img src='plantPhotos/cytsco36.jpg' /></a><a href='plantPhotos/cytsco45.jpg' target='_blank'><img src='plantPhotos/cytsco45.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT></P>    <UL>  <LI>native to central and southern Europe</LI>  <LI>hardy to zone 5, performs better from zone 6</LI>  <LI><font color="#FF0000"><u>Special Note:</u> This species has demonstrated     an invasive tendency in Connecticut, meaning it may escape from cultivation     and naturalize in minimally managed areas. For more information, .</font> </LI></UL>        <P><FONT SIZE="4">Habit      and Form</FONT></P>    <UL>      <LI>4&#39; to 6&#39; tall deciduous shrub</LI>      <LI>shape is rounded to irregular</LI>      <LI>fine textured and twiggy</LI>      <LI>branches green and in sprays</LI>      <LI>branching is upright to arching</LI>    </UL>        <P>                  </P>        <P><FONT SIZE="4">Summer      Foliage</FONT></P>    <UL>  <LI>deciduous</LI>  <LI>leaf arrangement alternate</LI>  <LI>small trifoliate leaves</LI>  <LI>upper leaves often with only 1 leaflet</LI>  <LI>color is medium to bright green</LI>  <LI>stems are green and more conspicuous than the foliage</LI>  <LI>stems are angled and flexible </LI></UL>        <P> <FONT SIZE="4">Autumn      Foliage</FONT></P>    <UL>      <LI>fall color not ornamentally important</LI>    </UL>        <P><FONT SIZE="4">Flowers</FONT></P>    <UL>      <LI>blooms in May and into early June</LI>      <LI>pea-like flowers are vivid yellow</LI>      <LI>blooming is profuse and very showy</LI>    </UL>        <P>                  </P>        <P><FONT SIZE="4">Fruit</FONT></P>    <UL>  <LI>1.5&quot; to 2&quot; pea-like pods</LI>  <LI>not ornamentally significant</LI></UL><P><FONT SIZE="4">Bark</FONT></P>    <UL>      <LI>stems are green year-round</LI>      <LI>very old stems are gray-brown</LI>    </UL>        <P><FONT SIZE="4">Culture</FONT></P>    <UL>      <LI>needs full sun</LI>      <LI>best in dry, sand soils</LI>      <LI>cut-back some growth after blooming </LI>    </UL>        <P><FONT SIZE="4">Landscape      Use</FONT></P>    <UL>      <LI>as an accent for stunning bloom</LI>      <LI>for borders</LI>      <LI>specimen</LI>      <LI>along highways</LI>    </UL>        <P>      </P>        <P><FONT SIZE="4">Liabilities</FONT></P>    <UL>      <LI>tends to be somewhat short-lived</LI>      <LI>twig kill in severe winters</LI>      <LI>tends to self-sow</LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT></P>    <UL>      <LI>year-round green stem color</LI>      <LI>angled stems</LI>      <LI>trifoliate leaves</LI>      <LI>yellow pea-like flowers</LI>    </UL>        <P><FONT SIZE="4">Propagation</FONT></P>    <UL>      <LI>by cuttings for cultivars</LI>      <LI>by seed</LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT>    </P>        <P>There are perhaps 100 or more cultivars in existence, but most are not available   in the U.S. Listed below are some of the more commonly-available cultivars.</P>                <P><B>&#39;Andreanus&#39;</B> - A bicolor with red and yellow petals. An upright      form to 8&#39; tall.</P>        <P><B>&#39;Burkwoodii&#39;</B> - An upright form with red-brown flowers. A      vigorous, bushy grower.</P>        <P><B>&#39;Lena&#39;</B> - Red and yellow petals. Only 3&#39; to 4&#39; tall and wide. A      <I>C. dallimorei</I> hybrid..</P>        <P><B>&#39;Lilac Time&#39;</B> - Another <i>C. dallimorei</i> hybrid. A compact grower   with reddish-pink and purple flowers.</P>        <P><B>&#39;Minstead&#39;</B> - Small white flowers flushed with lilac and purple. Slender,   arching habit.</P>        <P><B>&#39;Moonlight&#39;</B> - Creamy-white or primrose flowers. Upright habit to 5&#39;   or 6&#39; tall. Very common in the trade.</P>        <P><B>&#39;Nova Scotia&#39;</B> - An extra-hardy form with yellow flowers.</P>        <P><B>&#39;Pink Beauty&#39;</B> - The best pink-flowered selection.</P>        <P><B>&#39;San Francisco&#39;</B> - Flowers are dark red.</P>        <P><B>&#39;Stanford&#39;</B> - Flowers are orange and red, but appear orange from      a distance.</P>  </div>
					<script type="text/javascript">document.title = "Cytisus scoparius, Scotch Broom, Common Broom - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>