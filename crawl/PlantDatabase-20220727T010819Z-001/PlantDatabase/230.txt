<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Juniperus procumbens</h1>
						<h2>Japgarden Juniper</h2>
						<h3>Cupressaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/junpro21.jpg' target='_blank'><img src='plantPhotos/junpro21.jpg' /></a><a href='plantPhotos/junpro42.jpg' target='_blank'><img src='plantPhotos/junpro42.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT></P>    <UL>  <LI>hardy to zone 4</LI>  <LI>native to Japan </LI></UL>        <P><FONT SIZE="4">Habit      and Form</FONT></P>    <UL>  <LI>a dwarf, evergreen shrub</LI>  <LI>8&quot; to 12&quot; tall, sometimes 2&#39; high</LI>  <LI>up to 15&#39; wide</LI>  <LI>stiff, wide-spreading branches</LI>  <LI>ascending branch tips</LI>  <LI>slow growth rate</LI>  <LI>medium to coarse texture</LI></UL><P><FONT SIZE="4">Summer      Foliage</FONT></P>    <UL>      <LI>evergreen needle-like leaves</LI>      <LI>found in 3&#39;s</LI>      <LI>pointed apex, concave</LI>      <LI>bluish underside with 2 white dots at base</LI>      <LI>bluish green leaf color</LI>    </UL>        <P> <FONT SIZE="4">Autumn      Foliage</FONT></P>    <UL>      <LI>evergreen, no fall color </LI>    </UL>        <P><FONT SIZE="4">Flowers</FONT></P>    <UL>      <LI>dioecious </LI>      <LI>no ornamental value </LI>    </UL>        <P><FONT SIZE="4">Fruit</FONT></P>    <UL>      <LI>small cones</LI>      <LI>0.33&quot; in diameter</LI>    </UL>        <P><FONT SIZE="4">Bark</FONT></P>    <UL>      <LI>not ornamentally important </LI>    </UL>        <P><FONT SIZE="4">Culture</FONT></P>    <UL>      <LI>easily transplanted</LI>      <LI>adaptable to most conditions</LI>      <LI>full sun </LI>      <LI>pH adaptable </LI>    </UL>        <P><FONT SIZE="4">Landscape      Use</FONT> </P>    <UL>      <LI>groundcover</LI>      <LI>border</LI>      <LI>rock garden</LI>      <LI>mass plantings</LI>    </UL>        <P><FONT SIZE="4">Liabilities</FONT></P>    <UL>      <LI>Juniper blight and spider mites do cause some problems</LI>      <LI>Phomopsis</LI>      <LI>not many serious problems</LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT></P>    <UL>      <LI>two types of leaves present </LI>      <LI>needles pointed, concave</LI>      <LI>ascending branches</LI>      <LI>blue-green summer color </LI>      <LI>spreading growth habit</LI>    </UL>        <P><FONT SIZE="4">Propagation</FONT></P>    <UL>      <LI> by cuttings</LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT></P>        <P><FONT><B>&#39;Greenmound&#39; - </B>This form is a low groundcover reaching 8&quot;   tall and 6&#39; wide. The foliage is a fresh light green color.</FONT></P>        <P><FONT><B>&#39;Nana&#39;</B> - One of the finest groundcover junipers, this old favorite   grows similar to the species, but is more dwarfed. Plants appear as a neat,   mounded mat of small branchlets 2&#39; tall and up to 10&#39; wide. The plant mounds   on itself with age and develops a slight purple cast in winter. </FONT></P><P><b>&#39;Variegata&#39; </b>- This type is similar to the other cultivars, but it features   foliage streaked with creamy coloring.</P>  </div>
					<script type="text/javascript">document.title = "Juniperus procumbens, Japgarden Juniper - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>