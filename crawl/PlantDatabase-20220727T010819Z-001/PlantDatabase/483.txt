<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Symplocos paniculata</h1>
						<h2>Sapphireberry, Asiatic Sweetleaf</h2>
						<h3>Symplocaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/sympan00.jpg' target='_blank'><img src='plantPhotos/sympan00.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT></P>    <UL>      <LI>native to China and Japan</LI>      <LI>hardy to zone 4</LI>    </UL>        <P><FONT SIZE="4">Habit      and Form</FONT></P>    <UL>      <LI>a deciduous large shrub or small tree</LI>      <LI>wide-spreading</LI>      <LI>10&#39; to 20&#39; tall</LI>      <LI>equal spread</LI>      <LI>medium texture</LI>      <LI>slow growth rate</LI>    </UL>        <P><FONT SIZE="4">Summer      Foliage</FONT></P>    <UL>      <LI>alternate leaf arrangement</LI>      <LI>simple, deciduous leaves</LI>      <LI>obovate leaf shape</LI>      <LI>1.5&quot; to 3.5&quot; long</LI>      <LI>minute serrations</LI>      <LI>pubescent</LI>      <LI>dark green leaf color</LI>    </UL>                <P> <FONT SIZE="4">Autumn      Foliage</FONT></P>    <UL>      <LI>not ornamentally important </LI>    </UL>        <P><FONT SIZE="4">Flowers</FONT></P>    <UL>      <LI>creamy white flowers</LI>      <LI>fragrant</LI>      <LI>borne in panicles</LI>      <LI>2&quot; to 3&quot; long</LI>      <LI>blooms in June</LI>    </UL>        <P><FONT SIZE="4">Fruit</FONT></P>    <UL>      <LI>drupe</LI>      <LI>0.33&quot; in diameter</LI>      <LI>blue, bright</LI>      <LI>matures in September </LI>    </UL>        <P><FONT SIZE="4">Bark</FONT></P>    <UL>      <LI>ridged and furrowed</LI>      <LI>gray bark color</LI>    </UL>        <P><FONT SIZE="4">Culture</FONT></P>    <UL>      <LI>transplant easily</LI>      <LI>prefers well-drained soil</LI>      <LI>full sun to partial shade</LI>    </UL>        <P><FONT SIZE="4">Landscape      Use</FONT></P>    <UL>      <LI>border</LI>      <LI>screen</LI>      <LI>specimen</LI>      <LI>attract birds</LI>    </UL>        <P><FONT SIZE="4">Liabilities</FONT></P>    <UL>      <LI>need two for good fruit set</LI>      <LI>no serious problems</LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT></P>    <UL>      <LI>fat ovoid, imbricate, gray-brown buds</LI>      <LI>bright blue fruit</LI>      <LI>creamy white flowers</LI>      <LI>deciduous shrub</LI>    </UL>        <P><FONT SIZE="4">Propagation</FONT></P>    <UL>      <LI>by cuttings</LI>      <LI>by seed</LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT>    </P>    <UL>      <LI>none</LI>    </UL>  </div>
					<script type="text/javascript">document.title = "Symplocos paniculata, Sapphireberry, Asiatic Sweetleaf - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>