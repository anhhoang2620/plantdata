<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Acer rubrum</h1>
						<h2>Red Maple, Swamp Maple</h2>
						<h3>Aceraceae</h3>
					<div id='photoStrip'><a href='plantPhotos/acerub00.jpg' target='_blank'><img src='plantPhotos/acerub00.jpg' /></a><a href='plantPhotos/acerub01.jpg' target='_blank'><img src='plantPhotos/acerub01.jpg' /></a><a href='plantPhotos/acerub02.jpg' target='_blank'><img src='plantPhotos/acerub02.jpg' /></a><a href='plantPhotos/acerub03.jpg' target='_blank'><img src='plantPhotos/acerub03.jpg' /></a><a href='plantPhotos/acerub04.jpg' target='_blank'><img src='plantPhotos/acerub04.jpg' /></a><a href='plantPhotos/acerub05.jpg' target='_blank'><img src='plantPhotos/acerub05.jpg' /></a><a href='plantPhotos/acerub06.jpg' target='_blank'><img src='plantPhotos/acerub06.jpg' /></a><a href='plantPhotos/acerub08.jpg' target='_blank'><img src='plantPhotos/acerub08.jpg' /></a><a href='plantPhotos/acerub09.jpg' target='_blank'><img src='plantPhotos/acerub09.jpg' /></a><a href='plantPhotos/acerub10.jpg' target='_blank'><img src='plantPhotos/acerub10.jpg' /></a><a href='plantPhotos/acerub11.jpg' target='_blank'><img src='plantPhotos/acerub11.jpg' /></a><a href='plantPhotos/acerub12.jpg' target='_blank'><img src='plantPhotos/acerub12.jpg' /></a><a href='plantPhotos/acerub13.jpg' target='_blank'><img src='plantPhotos/acerub13.jpg' /></a><a href='plantPhotos/acerub15.jpg' target='_blank'><img src='plantPhotos/acerub15.jpg' /></a><a href='plantPhotos/acerub16.jpg' target='_blank'><img src='plantPhotos/acerub16.jpg' /></a><a href='plantPhotos/acerub17.jpg' target='_blank'><img src='plantPhotos/acerub17.jpg' /></a><a href='plantPhotos/acerub19.jpg' target='_blank'><img src='plantPhotos/acerub19.jpg' /></a><a href='plantPhotos/acerub20.jpg' target='_blank'><img src='plantPhotos/acerub20.jpg' /></a><a href='plantPhotos/acerub21.jpg' target='_blank'><img src='plantPhotos/acerub21.jpg' /></a><a href='plantPhotos/acerub22.jpg' target='_blank'><img src='plantPhotos/acerub22.jpg' /></a><a href='plantPhotos/acerub23.jpg' target='_blank'><img src='plantPhotos/acerub23.jpg' /></a><a href='plantPhotos/acerub24.jpg' target='_blank'><img src='plantPhotos/acerub24.jpg' /></a><a href='plantPhotos/acerub25.jpg' target='_blank'><img src='plantPhotos/acerub25.jpg' /></a><a href='plantPhotos/acerub26.jpg' target='_blank'><img src='plantPhotos/acerub26.jpg' /></a><a href='plantPhotos/acerub27.jpg' target='_blank'><img src='plantPhotos/acerub27.jpg' /></a><a href='plantPhotos/acerub31.jpg' target='_blank'><img src='plantPhotos/acerub31.jpg' /></a><a href='plantPhotos/acerub32.jpg' target='_blank'><img src='plantPhotos/acerub32.jpg' /></a><a href='plantPhotos/acerub33.jpg' target='_blank'><img src='plantPhotos/acerub33.jpg' /></a><a href='plantPhotos/acerub40.jpg' target='_blank'><img src='plantPhotos/acerub40.jpg' /></a><a href='plantPhotos/acerub56.jpg' target='_blank'><img src='plantPhotos/acerub56.jpg' /></a><a href='plantPhotos/acerub64.jpg' target='_blank'><img src='plantPhotos/acerub64.jpg' /></a><a href='plantPhotos/acerub71.jpg' target='_blank'><img src='plantPhotos/acerub71.jpg' /></a><a href='plantPhotos/acerub72.jpg' target='_blank'><img src='plantPhotos/acerub72.jpg' /></a><a href='plantPhotos/acerub82.jpg' target='_blank'><img src='plantPhotos/acerub82.jpg' /></a><a href='plantPhotos/acerub83.jpg' target='_blank'><img src='plantPhotos/acerub83.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT>    </P>    <UL>      <LI>very large geographic and climatic range </LI>      <LI>eastern United States and adjacent Canada </LI>      <LI>zone 3, but plants must have originated from the northern part of its        range </LI>    </UL>        <P>    </P>        <P><FONT SIZE="4">Habit      and Form</FONT> </P>    <UL>      <LI>deciduous </LI>      <LI>pyramidal or elliptical when young </LI>      <LI>becomes more spreading with age, eventually developing a more or less        rounded or oval outline </LI>      <LI>a medium to large tree, 40&#39; to 70&#39; tall, but can be over 100&#39; tall      </LI>      <LI>texture is medium </LI>      <LI>relatively fast growing </LI>    </UL>        <P>                                          </P>        <P><FONT SIZE="4">Summer      Foliage</FONT> </P>    <UL>      <LI>opposite, 2&quot; to 4&quot; long, 3 or 5-lobed </LI>      <LI>medium green upper leaf color, gray-green or frosty underside </LI>    </UL>        <P>                        </P>        <P><FONT SIZE="4">Autumn      Foliage</FONT> </P>    <UL>      <LI>varies from greenish yellow to vibrant scarlet to burgundy </LI>      <LI>can be very striking; many cultivars selected for red fall foliage      </LI>      <LI>often one of the first trees to color </LI>    </UL>        <P>                                                </P>        <P><FONT SIZE="4">Flowers</FONT>    </P>    <UL>      <LI>red or orange, in late March and April, showy </LI>        <LI>monoecious with male and female flowers on separate branches of a tree;     some flowers may be perfect as well</LI>      <LI>female flowers more showy with redder color </LI>    </UL>        <P>                        </P>        <P><FONT SIZE="4">Fruit</FONT>    </P>    <UL>      <LI>samaras, 0.75&quot; long </LI>      <LI>often display some red color and ornamental appeal </LI>    </UL>        <P>                  </P>        <P><FONT SIZE="4">Bark</FONT>    </P>    <UL>      <LI>attractive and a useful ornamental feature </LI>      <LI>young bark is smooth and light, ash-gray, almost silver </LI>      <LI>older branches and trunk are covered with scaly gray brown bark </LI>      <LI>bark contrasts well with early spring flowers </LI>    </UL>        <P>                  </P>        <P><FONT SIZE="4">Culture</FONT>    </P>    <UL>      <LI>easy to transplant and establish </LI>      <LI>tolerant of many conditions and adaptable </LI>      <LI>prefers moist, acidic soils </LI>      <LI>tolerates occasional flooding and wet soils </LI>      <LI>on alkaline soils develops manganese chlorosis </LI>      <LI>full sun best for landscape development, but can tolerate partial        shade </LI>    </UL>        <P><FONT SIZE="4">Landscape      Uses</FONT> </P>    <UL>      <LI>for shade in lawns, parks, campuses </LI>      <LI>good for fall foliage color and bark </LI>      <LI>good street tree </LI>    </UL>        <P><FONT SIZE="4">Liabilities</FONT>    </P>    <UL>      <LI>can be somewhat weak wooded and may suffer storm damage </LI>      <LI>because the species is so genetically diverse careful attention is        needed to insure adequate cold hardiness and the desired red fall        foliage </LI>      <LI>manganese chlorosis </LI>      <LI>tar spot, verticillium wilt leaf hoppers </LI>    </UL>        <P>      <FONT SIZE="4">ID Features</FONT></P>    <UL>      <LI>ashy-gray bark </LI>      <LI>clusters of &quot;Christmas ornament&quot; flower buds along with        smaller vegetative buds </LI>      <LI>twigs produce an odor when broken </LI>      <LI>confused with <I>Acer saccharinum</I>. In leaf, leaf shapes separate        species. Out of leaf <I>A</I>. <I>rubrum</I> doesn&#39;t have &quot;droop        and swoop&quot; branch tips like <I>A. saccharinum</I> </LI>      <LI>old bark not as shaggy or silvery as <I>A. saccharinum</I> </LI>    </UL>        <P>      </P>        <P><FONT SIZE="4">Propagation</FONT>    </P>    <UL>      <LI>by seed </LI>      <LI>no longer grafted due to incompatibilities </LI>      <LI>cultivars are by stem cuttings or tissue culture </LI>    </UL>        <P> <FONT SIZE="4">Cultivars/Varieties</FONT></P>        <P><B>&#39;Armstrong&#39;</B>- An upright or fastigiate selection that grows to around   60&#39; tall and 15&#39; wide. Lacks consistent orange or red fall color. May be an   <I>A. rubrum</I> x <I>A. saccharinum</I> hybrid (<I>Acer x freemanii</I>).</P><P><b>&#39;Autumn Flame&#39; </b>- Notable for its pleasant rounded habit as a young tree,   growing to 60&#39; tall. Good fall color and smaller leaves than normal. May lack   the hardiness of other cultivars.</P>        <P><B>&#39;Bowhall&#39;</B> - An upright, pyramidal form that is significantly      wider than &#39;Armstrong&#39; or &#39;Columnare&#39;. Fall color is yellow-orange with      red highlights.</P>        <P><B>&#39;Columnare&#39;</B> - Similar to &#39;Armstrong&#39;, but slower growing and      more compact. Has better summer foliage quality and dependable orange-red      fall color.</P>        <P><b>&#39;Franksred&#39;</b> (<b>Red Sunset</b>&reg;) - Pyramidal to rounded form. Striking   red fall color. Earlier color development than &#39;October Glory&#39; and more cold   hardy. Grows to 45 to 50&#39; tall. </P><P><b>&#39;Northwood&#39;</b> (<b>Northwood</b>&reg;) - A cultivar selected for its fine   tolerance of harsh winter conditions. The crown is oval and the branches ascend   upwards. Fall color may not be as effective as other selections, and some have   suggested that the growth form may become irregular with age.</P><P><B>&#39;October Glory&#39;</B> <b>(October Glory</b>&reg;) - Develops good red to burgundy   fall color and has dark green summer foliage. Colors late in October. May have   limitations as far as cold hardiness and winter twig kill susceptibility. Oval   shape, 40&#39; to 50&#39; tall.</P><P>&nbsp;</P>  </div>
					<script type="text/javascript">document.title = "Acer rubrum, Red Maple, Swamp Maple - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>