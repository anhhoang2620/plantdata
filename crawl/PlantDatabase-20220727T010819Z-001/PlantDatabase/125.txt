<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Cornus kousa</h1>
						<h2>Kousa Dogwood</h2>
						<h3>Cornaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/corkou00.jpg' target='_blank'><img src='plantPhotos/corkou00.jpg' /></a><a href='plantPhotos/corkou01.jpg' target='_blank'><img src='plantPhotos/corkou01.jpg' /></a><a href='plantPhotos/corkou02.jpg' target='_blank'><img src='plantPhotos/corkou02.jpg' /></a><a href='plantPhotos/corkou03.jpg' target='_blank'><img src='plantPhotos/corkou03.jpg' /></a><a href='plantPhotos/corkou04.jpg' target='_blank'><img src='plantPhotos/corkou04.jpg' /></a><a href='plantPhotos/corkou05.jpg' target='_blank'><img src='plantPhotos/corkou05.jpg' /></a><a href='plantPhotos/corkou06.jpg' target='_blank'><img src='plantPhotos/corkou06.jpg' /></a><a href='plantPhotos/corkou07.jpg' target='_blank'><img src='plantPhotos/corkou07.jpg' /></a><a href='plantPhotos/corkou08.jpg' target='_blank'><img src='plantPhotos/corkou08.jpg' /></a><a href='plantPhotos/corkou09.jpg' target='_blank'><img src='plantPhotos/corkou09.jpg' /></a><a href='plantPhotos/corkou10.jpg' target='_blank'><img src='plantPhotos/corkou10.jpg' /></a><a href='plantPhotos/corkou101.jpg' target='_blank'><img src='plantPhotos/corkou101.jpg' /></a><a href='plantPhotos/corkou11.jpg' target='_blank'><img src='plantPhotos/corkou11.jpg' /></a><a href='plantPhotos/corkou12.jpg' target='_blank'><img src='plantPhotos/corkou12.jpg' /></a><a href='plantPhotos/corkou13.jpg' target='_blank'><img src='plantPhotos/corkou13.jpg' /></a><a href='plantPhotos/corkou14.jpg' target='_blank'><img src='plantPhotos/corkou14.jpg' /></a><a href='plantPhotos/corkou15.jpg' target='_blank'><img src='plantPhotos/corkou15.jpg' /></a><a href='plantPhotos/corkou16.jpg' target='_blank'><img src='plantPhotos/corkou16.jpg' /></a><a href='plantPhotos/corkou17.jpg' target='_blank'><img src='plantPhotos/corkou17.jpg' /></a><a href='plantPhotos/corkou18.jpg' target='_blank'><img src='plantPhotos/corkou18.jpg' /></a><a href='plantPhotos/corkou19.jpg' target='_blank'><img src='plantPhotos/corkou19.jpg' /></a><a href='plantPhotos/corkou20.jpg' target='_blank'><img src='plantPhotos/corkou20.jpg' /></a><a href='plantPhotos/corkou21.jpg' target='_blank'><img src='plantPhotos/corkou21.jpg' /></a><a href='plantPhotos/corkou22.jpg' target='_blank'><img src='plantPhotos/corkou22.jpg' /></a><a href='plantPhotos/corkou23.jpg' target='_blank'><img src='plantPhotos/corkou23.jpg' /></a><a href='plantPhotos/corkou24.jpg' target='_blank'><img src='plantPhotos/corkou24.jpg' /></a><a href='plantPhotos/corkou25.jpg' target='_blank'><img src='plantPhotos/corkou25.jpg' /></a><a href='plantPhotos/corkou26.jpg' target='_blank'><img src='plantPhotos/corkou26.jpg' /></a><a href='plantPhotos/corkou27.jpg' target='_blank'><img src='plantPhotos/corkou27.jpg' /></a><a href='plantPhotos/corkou28.jpg' target='_blank'><img src='plantPhotos/corkou28.jpg' /></a><a href='plantPhotos/corkou29.jpg' target='_blank'><img src='plantPhotos/corkou29.jpg' /></a><a href='plantPhotos/corkou30.jpg' target='_blank'><img src='plantPhotos/corkou30.jpg' /></a><a href='plantPhotos/corkou31.jpg' target='_blank'><img src='plantPhotos/corkou31.jpg' /></a><a href='plantPhotos/corkou32.jpg' target='_blank'><img src='plantPhotos/corkou32.jpg' /></a><a href='plantPhotos/corkou33.jpg' target='_blank'><img src='plantPhotos/corkou33.jpg' /></a><a href='plantPhotos/corkou34.jpg' target='_blank'><img src='plantPhotos/corkou34.jpg' /></a><a href='plantPhotos/corkou35.jpg' target='_blank'><img src='plantPhotos/corkou35.jpg' /></a><a href='plantPhotos/corkou36.jpg' target='_blank'><img src='plantPhotos/corkou36.jpg' /></a><a href='plantPhotos/corkou37.jpg' target='_blank'><img src='plantPhotos/corkou37.jpg' /></a><a href='plantPhotos/corkou38.jpg' target='_blank'><img src='plantPhotos/corkou38.jpg' /></a><a href='plantPhotos/corkou39.jpg' target='_blank'><img src='plantPhotos/corkou39.jpg' /></a><a href='plantPhotos/corkou40.jpg' target='_blank'><img src='plantPhotos/corkou40.jpg' /></a><a href='plantPhotos/corkou41.jpg' target='_blank'><img src='plantPhotos/corkou41.jpg' /></a><a href='plantPhotos/corkou42.jpg' target='_blank'><img src='plantPhotos/corkou42.jpg' /></a><a href='plantPhotos/corkou61.jpg' target='_blank'><img src='plantPhotos/corkou61.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT></P>    <UL>      <LI>native to Japan, Korea and China</LI>      <LI>hardy to zone 5</LI>    </UL>        <P><FONT SIZE="4">Habit      and Form</FONT></P>    <UL>      <LI>a small, deciduous tree</LI>      <LI>reaching about 30&#39; tall</LI>      <LI>width equal to or greater than height</LI>      <LI>shape is rounded at maturity</LI>      <LI>young plants are vase-shaped</LI>      <LI>branching is upright and spreading but becomes more horizontal and        tiered with age</LI>    </UL>        <P>                        </P>        <P><FONT SIZE="4">Summer      Foliage</FONT></P>    <UL>      <LI>opposite, simple leaves</LI>      <LI>elliptic to ovate leaf shape</LI>      <LI>acuminate leaf tip</LI>      <LI>leaves 2&quot; to 4&quot; long and 0.75&quot; to 1&quot; wide</LI>      <LI>tufts of hairs in the vein axils and the leaf undersides</LI>      <LI>leaf color is a dark medium green</LI>    </UL>        <P>                        </P>        <P> <FONT SIZE="4">Autumn      Foliage</FONT></P>    <UL>      <LI>leaves turn red or red-purple</LI>      <LI>color holds for at least a few weeks</LI>    </UL>        <P>                        </P>        <P><FONT SIZE="4">Flowers</FONT></P>    <UL>      <LI>flowers are small and greenish-yellow</LI>      <LI>the flowers are surrounded by four large, showy, pointed bracts</LI>      <LI>bracts are white, but age to pink</LI>      <LI>bloom time is early June</LI>      <LI>bracts last for about 6 weeks, making for a very long effective bloom        time</LI>      <LI>individual bracts are 1&quot; to 2&quot; long and about 0.5&quot; to        0.75&quot; wide</LI>      <LI>flowers are held upright along stems</LI>    </UL>        <P>                                                      </P>        <P><FONT SIZE="4">Fruit</FONT></P>    <UL>      <LI>dull, raspberry red, pendant fruits when ripe</LI>      <LI>ripe fruits look somewhat like large raspberries</LI>      <LI>unripe, green fruits, stand upright along the stems</LI>      <LI>ripening occurs form late august through October</LI>      <LI>fruits are mealy, but edible</LI>      <LI>fruit display can be showy</LI>    </UL>        <P>                              </P>        <P><FONT SIZE="4">Bark</FONT></P>    <UL>      <LI>older bark develops an exfoliating character, revealing a mix of        gray-tan and mahogany brown</LI>      <LI>the quality of the bark seems to be somewhat variable, but usually of        ornamental interest </LI>    </UL>        <P>            </P>        <P><FONT SIZE="4">Culture</FONT></P>    <UL>      <LI>full sun to partial shade</LI>      <LI>prefers a moist, fertile, acidic , well-drained soil high in organic        matter</LI>      <LI>shows better drought resistance than <I>C. florida</I></LI>      <LI>it may be desirable to remove some lower branches to reveal the bark</LI>    </UL>        <P><FONT SIZE="4">Landscape      Use</FONT></P>    <UL>      <LI>specimen</LI>      <LI>lawn tree</LI>      <LI>small groups or groves</LI>      <LI>patio tree</LI>      <LI>useful as a multiseason plant</LI>      <LI>ornamental appeal form habit, bark, flowering, fruiting and fall        foliage color</LI>    </UL>        <P>       </P>        <P><FONT SIZE="4">Liabilities</FONT></P>    <UL>      <LI>relatively problem free </LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT></P>    <UL>      <LI>a small, vase-shaped tree with horizontal branching</LI>      <LI>mottled exfoliating bark</LI>      <LI>pointed bract tips in comparison to the rounded bract tips of <I>C.        florida</I></LI>      <LI>blooms about 2 or 3 weeks after <I>C. florida</I></LI>      <LI>red, raspberry-like fruits</LI>      <LI>flower buds pointed and shaped like and onion</LI>    </UL>                <P><FONT SIZE="4">Propagation</FONT></P>    <UL>      <LI>by cuttings, although not particularly easy</LI>      <LI>by seed </LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT> </P><P><B>var. <I>chinensis</I></B> - This form supposedly flowers more freely and   produces larger flower bracts, with leaves that are also said to be larger than   average. The validity of this variety, however, is questioned by some authorities.</P>        <P><b>&#39;Beni Fuji&#39; </b>- This form bears the deepest red-pink bracts of any <i>C.   kousa</i> cultivar, even stronger than &#39;Satomi&#39;. The color may not be as strong   in warm summer areas.</P><P><b>&#39;Elizabeth Lustgarten&#39;</b> and <b>&#39;Lustgarten Weeping&#39;</b> - Notable for   their weeping habits, this duo of cultivars grow to 15&#39; with branches that arch   downwards and bear white blooms. The habit is rounded and gentle, thus a mature   specimen is attractive.</P><P><B>&#39;Gold Star&#39;</B> - The center of each leaf has a broad gold band on this   cultivar, with stems that are somewhat reddish. It is relatively slow-growing,   but in time does form a small-medium rounded tree. The flower bracts are white,   and the contrast between the red fruit and gold-splashed foliage can be striking.</P><P><b>&#39;Little Beauty&#39;</b> - This plant forms a small, densely branched tree that   may never exceed 15&#39; tall. Other traits are as per the species.</P><P><B>&#39;Milky Way&#39; </B>- One of the most common cultivars in the trade, this selection   of var. <I>chinensis</I> is, however, probably composed of over a dozen similar   clones. The plant is extremely floriferous and sets a very heavy crop of fruit.   When in bloom, the pure white bracts can conceal the foliage.</P>        <P><B>&#39;Satomi&#39; </B>(also listed as<B> &#39;Rosabella&#39;</B>)<B> </B>- This is a very   popular cultivar said to develop bright pink bracts. In the United States, however,   the warm summers seem to dull the color. As a result, most plants bloom light   pink or white-pink. The color can vary from season to season, but &#39;Beni Fuji&#39;   probably has deeper pink bracts.</P>        <P>      </P>        <P><B>&#39;Snowboy&#39;</B> - This form sports gray-green leaves that are edged in white,   with occasional splashes of variegation throughout the foliage. While the plant   is attractive when well grown, it is very slow-growing and is best sited in   a shady location to avoid leaf scorch.</P><B>&#39;Summer Stars&#39;</B> - The flower bracts on this selection are not as large as those of other cultivars, but they are retained longer. Blooms heavily and grows to 25&#39; tall with a vase shape. <P><b>&#39;Temple Jewel&#39;</b> - This is an interesting variegated form with leaves   that show a light marbling of green, gold and light pink that turns mostly green   with age. It grows well to 20&#39; tall and wider with a dense habit. The bracts   are white.</P><P><b>&#39;Variegata&#39;</b> - Various clones exist with differing degrees of yellow   or white variegation. The pattern may be unstable, plus the plants can produce   green growth reversions. Most are slower growing and benefit from siting in   some shade.</P><P><B>&#39;Wolf Eyes&#39;</B> - This is a very popular variegated form with leaves that   show a uniform white margin. The leaf margins are often prominently wavy, as   well. The variegation pattern is quite stable and resistant to burning, though   a shaded planting site is still desirable. In fall, the leaves develop attractive   pink to red coloration. The plant is shrubby and slow-growing, to 10&#39; tall and   wide.</P>  </div>
					<script type="text/javascript">document.title = "Cornus kousa, Kousa Dogwood - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>