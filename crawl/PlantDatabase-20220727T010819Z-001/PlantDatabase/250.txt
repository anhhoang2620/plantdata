<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Leucothoe fontanesiana</h1>
						<h2>Drooping Leucothoe, Fetterbush</h2>
						<h3>Ericaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/leufon01.jpg' target='_blank'><img src='plantPhotos/leufon01.jpg' /></a><a href='plantPhotos/leufon02.jpg' target='_blank'><img src='plantPhotos/leufon02.jpg' /></a><a href='plantPhotos/leufon03.jpg' target='_blank'><img src='plantPhotos/leufon03.jpg' /></a><a href='plantPhotos/leufon04.jpg' target='_blank'><img src='plantPhotos/leufon04.jpg' /></a><a href='plantPhotos/leufon05.jpg' target='_blank'><img src='plantPhotos/leufon05.jpg' /></a><a href='plantPhotos/leufon06.jpg' target='_blank'><img src='plantPhotos/leufon06.jpg' /></a><a href='plantPhotos/leufon07.jpg' target='_blank'><img src='plantPhotos/leufon07.jpg' /></a><a href='plantPhotos/leufon08.jpg' target='_blank'><img src='plantPhotos/leufon08.jpg' /></a><a href='plantPhotos/leufon10.jpg' target='_blank'><img src='plantPhotos/leufon10.jpg' /></a><a href='plantPhotos/leufon11.jpg' target='_blank'><img src='plantPhotos/leufon11.jpg' /></a><a href='plantPhotos/leufon12.jpg' target='_blank'><img src='plantPhotos/leufon12.jpg' /></a><a href='plantPhotos/leufon63.jpg' target='_blank'><img src='plantPhotos/leufon63.jpg' /></a><a href='plantPhotos/leufon73.jpg' target='_blank'><img src='plantPhotos/leufon73.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat:</FONT></P>    <UL>      <LI>zone 4 to 6 </LI>      <LI>mountainous regions of southeastern U.S. </LI>      <LI>often found in cool, shady ravines along streams</LI>    </UL>        <P><FONT SIZE="4">Habitat      and Form:</FONT></P>    <UL>      <LI>a suckering, colonizing shrub </LI>      <LI>stout stems from the base, limited lateral branches </LI>      <LI>graceful, arching stems </LI>      <LI>3 to 6&#39; tall with equal spread </LI>      <LI>broadleaf evergreen</LI>    </UL>        <P>      </P>        <P><FONT SIZE="4">Summer      Foliage:</FONT></P>    <UL>      <LI>alternate leaves, 2-5&quot; long </LI>      <LI>dark shiny green mature foliage; leathery </LI>      <LI>emerging shoots bronzy or reddish </LI>      <LI>leaves sharp pointed with fine, wide-spread serrations</LI>    </UL>        <P>            </P>        <P><FONT SIZE="4">Fall      Foliage:</FONT></P>    <UL>      <LI>with sufficient exposure to sun, a burgundy-purple color develops      </LI>      <LI>some cultivars selected for fall-winter coloration</LI>    </UL>        <P><FONT SIZE="4">Flowers:</FONT></P>    <UL>      <LI>in May </LI>      <LI>small white, urn-shaped in 2-3&quot; long racemes </LI>      <LI>fragrant </LI>      <LI>borne in leaf axils </LI>      <LI>showy, but somewhat obscured by foliage</LI>    </UL>        <P>                        </P>        <P><FONT SIZE="4">Fruit:</FONT></P>    <UL>      <LI>capsule </LI>      <LI>not ornamentally important</LI>    </UL>        <P>    </P>        <P><FONT SIZE="4">Bark:</FONT></P>    <UL>      <LI>stems mix of green and red </LI>      <LI>glabrous and shiny</LI>    </UL>        <P>    </P>        <P><FONT SIZE="4">Culture:</FONT></P>    <UL>      <LI>prefers moist, cool, acid soils </LI>      <LI>best in partial shade </LI>      <LI>can rejuvenate old plants by pruning to ground in spring </LI>      <LI>protect from winter wind</LI>    </UL>        <P><FONT SIZE="4">Landscape      use:</FONT></P>    <UL>      <LI>naturalizing </LI>      <LI>borders with other ericaceous plants </LI>      <LI>facer plants with taller plants </LI>      <LI>shady bank stabilization</LI>    </UL>        <P>    </P>        <P><FONT SIZE="4">Liabilities:</FONT></P>    <UL>      <LI>desiccation burn in winter in exposed sites </LI>      <LI>leaf spot can be a problem in area with poor air circulation</LI>    </UL>        <P><FONT SIZE="4">ID      Features:</FONT></P>    <UL>      <LI>stout, glabrous green-red stems with little lateral branching </LI>      <LI>colonizing habit </LI>      <LI>evergreen, alternate, 2-5&quot; long leaves with long, sharp point        and fine serrations on margin </LI>      <LI>racemes of white urn-shaped flowers in axil</LI>    </UL>        <P><FONT SIZE="4">Propagation:</FONT></P>    <UL>      <LI>cutting root readily in June or winter root easily with hormone </LI>      <LI>seed</LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties:</FONT></P>        <P><B>&#39;Girard&#39;s Rainbow&#39;</B> - The foliage emerges white, pink and coppery on   this selection, maturing to green streaked with cream. It is less vigorous than   the species, but handsome.</P><P><b>&#39;Mary Elizabeth&#39;</b> - This is a dwarf form with finely-textured, narrow   foliage that bronzes in winter.</P><P><b>&#39;Nana&#39;</b> (perhaps the same as<b> &#39;Compacta&#39;</b>) - This is a dense, dwarf   form reaching 2&#39; tall and wider.</P>        <P> <B>&#39;Scarletta&#39;</B> - A common form in commerce, this form is notable for its   deep red new growth and bronzy-purple winter hue.</P><P><b>&#39;Silver Run&#39;</b> - A variegated form that appears to be hardier than &#39;Girard&#39;s   Rainbow&#39;, this selection offers foliage marked with creamy white areas.</P>  </div>
					<script type="text/javascript">document.title = "Leucothoe fontanesiana, Drooping Leucothoe, Fetterbush - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>