<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Aralia elata</h1>
						<h2>Japanese Angelica-tree</h2>
						<h3>Araliaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/araela103.jpg' target='_blank'><img src='plantPhotos/araela103.jpg' /></a><a href='plantPhotos/araela104.jpg' target='_blank'><img src='plantPhotos/araela104.jpg' /></a><a href='plantPhotos/araela105.jpg' target='_blank'><img src='plantPhotos/araela105.jpg' /></a><a href='plantPhotos/araela106.jpg' target='_blank'><img src='plantPhotos/araela106.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT></P>    <UL>      <LI>Japan, Korea, Manchuria, Russian Far East </LI>    </UL>        <P><FONT SIZE="4">Habit      and Form</FONT> </P>    <UL>      <LI>deciduous</LI>      <LI>large shrub or small tree </LI>      <LI>coarse, thick stems with prickles</LI>      <LI>few side branches </LI>      <LI>suckers from base and spreads</LI>    </UL>        <P><FONT SIZE="4">Summer      Foliage</FONT> </P>    <UL>      <LI>alternate </LI>      <LI>very large leaves (2&#39;-4&#39; long) </LI>      <LI>bi- or tri-pinnate, dark green</LI>    </UL>        <P> <FONT SIZE="4">Autumn      Foliage</FONT> </P>    <UL>      <LI>poor yellow or no color</LI>      <LI>leaves may drop early in season</LI>    </UL>        <P><FONT SIZE="4">Flowers</FONT>    </P>    <UL>      <LI>large terminal panicles of whitish blooms</LI>      <LI>blooms late in season, July-August</LI>    </UL>        <P><FONT SIZE="4">Fruit</FONT>    </P>    <UL>      <LI>small blackish drupes, taken by birds or drop early</LI>      <LI>infrutescence often colored pink and persists </LI>    </UL>        <P><FONT SIZE="4">Bark</FONT>    </P>    <UL>      <LI> rough gray with prickles</LI>      <LI>prominent, large leaf scars</LI>    </UL>        <P><FONT SIZE="4">Culture</FONT>    </P>    <UL>      <LI>easy to grow, adapted to any well-drained soil</LI>      <LI>sun to light shade, pH tolerant</LI>      <LI>pollution and neglect tolerant </LI>    </UL>        <P><FONT SIZE="4">Landscape      Uses</FONT> </P>    <UL>      <LI>large shrub borders</LI>      <LI>background plantings</LI>      <LI>plant in areas with poor, lean soil</LI>      <LI>specimen where growth is restrained </LI>    </UL>        <P><FONT SIZE="4">Liabilities</FONT>    </P>    <UL>      <LI>coarse appearance in winter</LI>      <LI>stems are full of sharp prickles</LI>      <LI>vigorous, running habit needs room</LI>      <LI>difficult to find commercially; cultivars very expensive</LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT> </P>    <UL>      <LI>thick, coarse unbranched stems</LI>      <LI>stems are covered with prickles </LI>      <LI>huge bi- or tri-pinnate leaves </LI>      <LI>large clusters of white blooms in late summer </LI>    </UL>        <P><FONT SIZE="4">Propagation</FONT>    </P>    <UL>      <LI>by seed </LI>      <LI>division of suckers</LI>      <LI>root cuttings</LI>      <LI>cultivars are grafted</LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT> </P><P>The variegated cultivars of Aralia elata are among the most spectacular variegated   plants, but they must be grafted and are therefore rare and expensive.</P><p><B>&#39;Aureovariegata&#39; </B>- This form bears yellow-edged leaflets and is the   most common cultivar.</p><p><b>&#39;Golden Umbrella&#39; </b>- A new, rare form featuring refined yellow-margined   leaflets that fade to white, this plant is not common.</p><P><B>&#39;Silver Umbrellas&#39;</B> - This plant is similar to &#39;Variegata&#39;, but is smaller   in all respects and more finely textured. </P><P>&#39;<b>Variegata</b>&#39; - Most notable for its white-edged leaflets, this plant   appears very similar to &#39;Aureovariegata&#39;.</P>  </div>
					<script type="text/javascript">document.title = "Aralia elata, Japanese Angelica-tree - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>