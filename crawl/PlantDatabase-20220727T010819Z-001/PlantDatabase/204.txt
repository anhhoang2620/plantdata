<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Hibiscus syriacus</h1>
						<h2>Rose-of-sharon, Shrub Althea</h2>
						<h3>Malvaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/hibsyr00.jpg' target='_blank'><img src='plantPhotos/hibsyr00.jpg' /></a><a href='plantPhotos/hibsyr01.jpg' target='_blank'><img src='plantPhotos/hibsyr01.jpg' /></a><a href='plantPhotos/hibsyr02.jpg' target='_blank'><img src='plantPhotos/hibsyr02.jpg' /></a><a href='plantPhotos/hibsyr03.jpg' target='_blank'><img src='plantPhotos/hibsyr03.jpg' /></a><a href='plantPhotos/hibsyr04.jpg' target='_blank'><img src='plantPhotos/hibsyr04.jpg' /></a><a href='plantPhotos/hibsyr05.jpg' target='_blank'><img src='plantPhotos/hibsyr05.jpg' /></a><a href='plantPhotos/hibsyr06.jpg' target='_blank'><img src='plantPhotos/hibsyr06.jpg' /></a><a href='plantPhotos/hibsyr07.jpg' target='_blank'><img src='plantPhotos/hibsyr07.jpg' /></a><a href='plantPhotos/hibsyr08.jpg' target='_blank'><img src='plantPhotos/hibsyr08.jpg' /></a><a href='plantPhotos/hibsyr09.jpg' target='_blank'><img src='plantPhotos/hibsyr09.jpg' /></a><a href='plantPhotos/hibsyr10.jpg' target='_blank'><img src='plantPhotos/hibsyr10.jpg' /></a><a href='plantPhotos/hibsyr101.jpg' target='_blank'><img src='plantPhotos/hibsyr101.jpg' /></a><a href='plantPhotos/hibsyr11.jpg' target='_blank'><img src='plantPhotos/hibsyr11.jpg' /></a><a href='plantPhotos/hibsyr112.jpg' target='_blank'><img src='plantPhotos/hibsyr112.jpg' /></a><a href='plantPhotos/hibsyr12.jpg' target='_blank'><img src='plantPhotos/hibsyr12.jpg' /></a><a href='plantPhotos/hibsyr13.jpg' target='_blank'><img src='plantPhotos/hibsyr13.jpg' /></a><a href='plantPhotos/hibsyr14.jpg' target='_blank'><img src='plantPhotos/hibsyr14.jpg' /></a><a href='plantPhotos/hibsyr15.jpg' target='_blank'><img src='plantPhotos/hibsyr15.jpg' /></a><a href='plantPhotos/hibsyr16.jpg' target='_blank'><img src='plantPhotos/hibsyr16.jpg' /></a><a href='plantPhotos/hibsyr17.jpg' target='_blank'><img src='plantPhotos/hibsyr17.jpg' /></a><a href='plantPhotos/hibsyr18.jpg' target='_blank'><img src='plantPhotos/hibsyr18.jpg' /></a><a href='plantPhotos/hibsyr20.jpg' target='_blank'><img src='plantPhotos/hibsyr20.jpg' /></a><a href='plantPhotos/hibsyr31.jpg' target='_blank'><img src='plantPhotos/hibsyr31.jpg' /></a><a href='plantPhotos/hibsyr33.jpg' target='_blank'><img src='plantPhotos/hibsyr33.jpg' /></a><a href='plantPhotos/hibsyr50.jpg' target='_blank'><img src='plantPhotos/hibsyr50.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT></P>    <UL>  <LI>native to China and India</LI>  <LI>hardy to zone 5</LI>  <LI><font color="#FF0000"><u>Special Note:</u> This species has demonstrated     an invasive tendency in Connecticut, meaning it may escape from cultivation     and naturalize in minimally managed areas. For more information, .</font> </LI></UL>        <P><FONT SIZE="4">Habit      and Form</FONT></P>    <UL>      <LI>a deciduous, flowering shrub</LI>      <LI>8&#39; to 10&#39; tall</LI>      <LI>6&#39; to 8&#39; wide</LI>      <LI>upright, spreading branching</LI>      <LI>develops a vase-shaped outline</LI>      <LI>often leggy at the base</LI>      <LI>multi-stemmed with lots of vertical branches</LI>    </UL>        <P>            </P>        <P><FONT SIZE="4">Summer      Foliage</FONT></P>    <UL>      <LI>alternate, simple, deciduous leaves</LI>      <LI>leaves 3-lobed</LI>      <LI>margins coarsely toothed</LI>      <LI>2&quot; to 4&quot; long</LI>      <LI>medium to dark green</LI>      <LI>leaf surface glabrous sometimes shiny</LI>      <LI>late to leaf out in spring</LI>    </UL>                <P> <FONT SIZE="4">Autumn      Foliage</FONT></P>    <UL>      <LI>yellow-green</LI>      <LI>leave hold late</LI>      <LI>overall poor effect</LI>    </UL>        <P><FONT SIZE="4">Flowers</FONT></P>    <UL>      <LI>large, showy blossoms in July, August, and September</LI>      <LI>white, pink, magenta, violet, blue, and combinations of these</LI>      <LI>5-petaled</LI>      <LI>2&quot; to 4&quot; across</LI>      <LI>single or double</LI>      <LI>flowers are solitary</LI>      <LI>produced on current season&#39;s growth</LI>    </UL>        <P>                                    </P>        <P><FONT SIZE="4">Fruit</FONT></P>    <UL>      <LI>a brown capsule</LI>      <LI>0.75&quot; long</LI>      <LI>persists through the winter</LI>      <LI>5-valved</LI>    </UL>        <P><FONT SIZE="4">Bark</FONT></P>    <UL>      <LI>light gray </LI>    </UL>        <P><FONT SIZE="4">Culture</FONT></P>    <UL>      <LI>full sun is best, but tolerant of partial shade</LI>      <LI>soils are not critical</LI>      <LI>easily transplanted</LI>      <LI>annual pruning back will result in increases shoot vigor and larger        flowers</LI>      <LI>likes hot weather </LI>      <LI>may need to remove winter killed stems</LI>    </UL>        <P><FONT SIZE="4">Landscape      Use</FONT></P>    <UL>  <LI>shrub border</LI>  <LI>screen</LI>  <LI>groupings and mass plantings</LI>  <LI>useful for late season bloom</LI>  <LI>do not use as a specimen plant</LI>  <LI>standard forms may be used as small trees</LI></UL>               <P><FONT SIZE="4">Liabilities</FONT></P>    <UL>      <LI>does not have multi-season ornamental appeal</LI>      <LI>winter injury and twig dieback</LI>      <LI>late to leaf out in spring</LI>      <LI>leaf spot, cankers, rust, aphids, spider mites</LI>    </UL>           <P><FONT SIZE="4">ID      Features</FONT></P>    <UL>      <LI>3-lobed leaves</LI>      <LI>late summer large flowers</LI>      <LI>distinctive vase-shaped habit</LI>      <LI>persistent 5-valved capsules</LI>      <LI>indistinct buds</LI>      <LI>shelf-like projections mark the position of previous flowers and        fruit</LI>      <LI>thread-like stipules are persistent</LI>    </UL>           <P><FONT SIZE="4">Propagation</FONT></P>    <UL>      <LI>by cuttings </LI>      <LI>by seed</LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT> </P><P>Dozens of forms are available, but the following listing encompasses cultivars   that indicate the range of types offered.</P>        <P><B>&#39;Aphrodite&#39;</B> - This U.S. National Arboretum introduction features dark   pink flowers exhibiting a dark red eyespot. The foliage is heavily textured.   As this plant is a triploid, it should not produce as many seedlings. Some observations   indicate ample seed production.</P>        <p><b>&#39;Ardens&#39;</b>, <b>&#39;Blushing Bride&#39;</b> and <b>&#39;Peoniflora&#39;</b> - These cultivars   are just a few of the many forms sold at nurseries with double blooms of a pink   or rose hue. </p><p><B>&#39;Blue Bird&#39; </B>(also listed as <B>&#39;Bluebird&#39;</B>) - A form with single   flowers that are a blue with a reddish base, this old form is still popular   in the trade. </p><P><B>&#39;Diana&#39; </B>- This triploid U.S. National Arboretum introduction is one   of the best white-flowered forms. The blooms are large and lack a central blotch.   They remain open at night and are produced over a long period due to little   or no seed production. This plant appears to be less vigorous than other forms.</P>        <P><B>&#39;Helene&#39;</B> - A triploid form producing white flowers that are maroon at   the base, this U.S. National Arboretum introduction flowers heaviliy and sets   little fruit.</P><p><B>&#39;Minerva&#39; </B>- A heavy-blooming cultivar with lavender flowers overcast   with some pink, this plant remains smaller and more shrubby (to 9&#39; tall). The   flowers have a red eye and the foliage is higher quality than the species. A   U.S. National Arboretum introduction reportedly performing well into USDA zone   5. </p><p><b>&#39;Pink Giant&#39;</b> - This form bears large, 5&quot; wide pink blooms with   a deep red central blotch. It grows 8&#39; tall and wide.</p><p><b>&#39;Purpureus Variegatus&#39;</b> and<b> &#39;Meehanii&#39; </b>- These are two of the   more common variegated forms, featuring leaves variously edged or mottled with   white, yellow, and gray. The flowers are of secondary interest on these plants. </p><p><B>&#39;Tri-color&#39;</B> - This is a very unusual cultivar with double flowers colored   pink, red and purple on one plant. </p></div>
					<script type="text/javascript">document.title = "Hibiscus syriacus, Rose-of-sharon, Shrub Althea - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>