<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Ostrya virginiana</h1>
						<h2>American Hophornbeam, Ironwood</h2>
						<h3>Betulaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/ostvir00.jpg' target='_blank'><img src='plantPhotos/ostvir00.jpg' /></a><a href='plantPhotos/ostvir01.jpg' target='_blank'><img src='plantPhotos/ostvir01.jpg' /></a><a href='plantPhotos/ostvir02.jpg' target='_blank'><img src='plantPhotos/ostvir02.jpg' /></a><a href='plantPhotos/ostvir03.jpg' target='_blank'><img src='plantPhotos/ostvir03.jpg' /></a><a href='plantPhotos/ostvir04.jpg' target='_blank'><img src='plantPhotos/ostvir04.jpg' /></a><a href='plantPhotos/ostvir05.jpg' target='_blank'><img src='plantPhotos/ostvir05.jpg' /></a><a href='plantPhotos/ostvir06.jpg' target='_blank'><img src='plantPhotos/ostvir06.jpg' /></a><a href='plantPhotos/ostvir07.jpg' target='_blank'><img src='plantPhotos/ostvir07.jpg' /></a><a href='plantPhotos/ostvir08.jpg' target='_blank'><img src='plantPhotos/ostvir08.jpg' /></a><a href='plantPhotos/ostvir09.jpg' target='_blank'><img src='plantPhotos/ostvir09.jpg' /></a><a href='plantPhotos/ostvir10.jpg' target='_blank'><img src='plantPhotos/ostvir10.jpg' /></a><a href='plantPhotos/ostvir11.jpg' target='_blank'><img src='plantPhotos/ostvir11.jpg' /></a><a href='plantPhotos/ostvir12.jpg' target='_blank'><img src='plantPhotos/ostvir12.jpg' /></a><a href='plantPhotos/ostvir13.jpg' target='_blank'><img src='plantPhotos/ostvir13.jpg' /></a><a href='plantPhotos/ostvir14.jpg' target='_blank'><img src='plantPhotos/ostvir14.jpg' /></a><a href='plantPhotos/ostvir15.jpg' target='_blank'><img src='plantPhotos/ostvir15.jpg' /></a><a href='plantPhotos/ostvir30.jpg' target='_blank'><img src='plantPhotos/ostvir30.jpg' /></a><a href='plantPhotos/ostvir31.jpg' target='_blank'><img src='plantPhotos/ostvir31.jpg' /></a><a href='plantPhotos/ostvir32.jpg' target='_blank'><img src='plantPhotos/ostvir32.jpg' /></a><a href='plantPhotos/ostvir33.jpg' target='_blank'><img src='plantPhotos/ostvir33.jpg' /></a><a href='plantPhotos/ostvir62.jpg' target='_blank'><img src='plantPhotos/ostvir62.jpg' /></a><a href='plantPhotos/ostvir64.jpg' target='_blank'><img src='plantPhotos/ostvir64.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT></P>    <UL>      <LI>eastern United States</LI>      <LI>zone 4</LI>      <LI>naturally occurs as an understory tree in dry woodlands</LI>    </UL>        <P><FONT SIZE="4">Habit      and Form</FONT></P>    <UL>      <LI>a small to medium-sized tree</LI>      <LI>reaches 30&#39; to 50&#39; tall</LI>      <LI>overall shape is ovate to pyramidal when young</LI>      <LI>older trees are rounded</LI>      <LI>branching is upright and spreading</LI>      <LI>old trees exhibit more irregular branching</LI>    </UL>        <P>                  </P>        <P><FONT SIZE="4">Summer      Foliage</FONT></P>    <UL>      <LI>simple deciduous leaves</LI>      <LI>alternate leaf arrangement</LI>      <LI>leaves are 2&quot; to 5&quot; long and half as wide</LI>      <LI>more or less oval shape to leaves</LI>      <LI>acuminate leaf tip</LI>      <LI>doubly serrate leaf margins</LI>      <LI>medium to dark green leaves</LI>    </UL>        <P>       </P>        <P> <FONT SIZE="4">Autumn      Foliage</FONT></P>    <UL>      <LI>yellowish brown to orange</LI>      <LI>not particularly impressive </LI>    </UL>        <P><FONT SIZE="4">Flowers</FONT></P>    <UL>      <LI>male flowers are catkins in 3&#39;s</LI>      <LI>not highly ornamental </LI>    </UL>        <P><FONT SIZE="4">Fruit</FONT></P>    <UL>      <LI>small inflated pods in clusters</LI>      <LI>a hard nutlet is inside each pod</LI>      <LI>fruit clusters look like fruit of hops, hence the common name        Hophornbeam</LI>      <LI>fruit change from green to tan </LI>    </UL>        <P>                  </P>        <P><FONT SIZE="4">Bark</FONT></P>    <UL>      <LI>forms vertical strips which exfoliate at the ends</LI>      <LI>color is gray brown</LI>      <LI>trunks and main branches develop a fluted or &quot;muscle-like&quot;        appearance</LI>      <LI>bark and trunk features are ornamentally attractive</LI>    </UL>        <P>                        </P>        <P><FONT SIZE="4">Culture</FONT></P>    <UL>      <LI>full sun to partial shade</LI>      <LI>best in slightly acid soil that is moist, cool and fertile</LI>      <LI>can tolerate dry gravelly soils in partial shade once established</LI>      <LI>can be difficult to transplant and slow to establish</LI>      <LI>easily transplanted form containers are B&amp;B</LI>    </UL>        <P><FONT SIZE="4">Landscape      Use</FONT></P>    <UL>      <LI>lawn tree</LI>      <LI>street tree</LI>      <LI>naturalistic areas</LI>      <LI>for bark and fruit </LI>    </UL>        <P>      </P>        <P><FONT SIZE="4">Liabilities</FONT></P>    <UL>      <LI>not tolerant of salt at all</LI>      <LI>avoid roadside or seaside uses</LI>      <LI>can be slow growing</LI>      <LI>slow to establish following transplanting</LI>      <LI>hard to find in trade</LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT></P>    <UL>      <LI>veins on leaves branch before reaching the leaf margin</LI>      <LI>hop-like fruit clusters</LI>      <LI>fluted trunk</LI>      <LI>exfoliating vertical strips of bark</LI>    </UL>        <P>   </P>        <P><FONT SIZE="4">Propagation</FONT></P>    <UL>      <LI>by seed</LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT>    </P>    <UL>      <LI>none</LI>    </UL>  </div>
					<script type="text/javascript">document.title = "Ostrya virginiana, American Hophornbeam, Ironwood - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>