<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Acer davidii</h1>
						<h2>David Maple</h2>
						<h3>Aceraceae</h3>
					<div id='photoStrip'><a href='plantPhotos/acedav01.jpg' target='_blank'><img src='plantPhotos/acedav01.jpg' /></a><a href='plantPhotos/acedav02.jpg' target='_blank'><img src='plantPhotos/acedav02.jpg' /></a><a href='plantPhotos/acedav03.jpg' target='_blank'><img src='plantPhotos/acedav03.jpg' /></a><a href='plantPhotos/acedav04.jpg' target='_blank'><img src='plantPhotos/acedav04.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT>    </P>    <UL>      <LI>native to Central China </LI>      <LI>zone 5 </LI>    </UL>        <P><FONT SIZE="4">Habit      and Form</FONT> </P>    <UL>      <LI>small to medium sized tree, 20 to 50&#39; tall </LI>      <LI>deciduous </LI>      <LI>typically multistemmed, slightly spreading</LI>      <LI>medium to fine texture </LI>    </UL>                <P><FONT SIZE="4">Summer      Foliage</FONT> </P>    <UL>      <LI>opposite, 3&quot; to 7&quot; long and 1.5&quot; to 4&quot; wide </LI>      <LI>simple, ovate, with acuminate tip and rounded base</LI>      <LI>normally unlobed, but can have small basal lobes</LI>      <LI>dark green </LI>      <LI>early to leaf out in spring </LI>      <LI>petiole can be up to 2&quot; long </LI>    </UL>        <P>      </P>        <P><FONT SIZE="4">Autumn      Foliage</FONT> </P>    <UL>      <LI>typically yellow tinged with purple </LI>    </UL>        <P><FONT SIZE="4">Flowers</FONT>    </P>    <UL>      <LI>yellow, racemes 1.5 to 2.5&quot; long</LI>      <LI>not especially showy</LI>    </UL>        <P><FONT SIZE="4">Fruit</FONT>    </P>    <UL>      <LI>samaras 1.25&quot; long </LI>      <LI>wings 0.25&quot; wide</LI>      <LI>almost horizontal spread </LI>    </UL>        <P><FONT SIZE="4">Bark</FONT>    </P>    <UL>      <LI>bark is green with a purplish-red highlights and striped with white      </LI>    </UL>                <P><FONT SIZE="4">Culture</FONT></P>    <UL>      <LI>easily transplanted</LI>      <LI>full sun or partial shade </LI>      <LI>prefers acidic, moist soils</LI>      <LI>does best in regions with cool, mild climates</LI>    </UL>        <P><FONT SIZE="4">Landscape      Uses</FONT> </P>    <UL>      <LI>small specimen </LI>      <LI>hedges or screens </LI>      <LI>groupings or mini-groves </LI>    </UL>        <P><FONT SIZE="4">Liabilities</FONT>    </P>    <UL>      <LI>relatively trouble-free </LI>      <LI>hard to locate plants to buy</LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT> </P>    <UL>      <LI>opposite leaves</LI>      <LI>unlobed leaf shape distinctive for a maple </LI>      <LI>white stripped bark</LI>      <LI>new leaves covered in red down</LI>    </UL>        <P><FONT SIZE="4">Propagation</FONT>    </P>    <UL>      <LI>by seed </LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT>    </P>    <UL>      <LI>none of importance</LI>    </UL>  </div>
					<script type="text/javascript">document.title = "Acer davidii, David Maple - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>