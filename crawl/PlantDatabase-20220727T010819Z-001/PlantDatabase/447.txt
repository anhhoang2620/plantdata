<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Salix purpurea</h1>
						<h2>Purpleosier Willow, Basket Willow</h2>
						<h3>Salicaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/salpur01.jpg' target='_blank'><img src='plantPhotos/salpur01.jpg' /></a><a href='plantPhotos/salpur02.jpg' target='_blank'><img src='plantPhotos/salpur02.jpg' /></a><a href='plantPhotos/salpur03.jpg' target='_blank'><img src='plantPhotos/salpur03.jpg' /></a><a href='plantPhotos/salpur04.jpg' target='_blank'><img src='plantPhotos/salpur04.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT></P>    <UL>      <LI>native to Europe, northern Africa to central Asia and Japan</LI>      <LI>zone 3 </LI>    </UL>        <P><FONT SIZE="4">Habit      and Form</FONT></P>    <UL>      <LI>a deciduous, large shrub</LI>      <LI>8&#39; to 10&#39; tall</LI>      <LI>rounded, dense shape</LI>      <LI>shrub composed of thin branches</LI>      <LI>fast growth rate</LI>      <LI>fine texture</LI>    </UL>                <P><FONT SIZE="4">Summer      Foliage</FONT></P>    <UL>      <LI>alternate (occasionally opposite) leaf arrangement</LI>      <LI>simple, deciduous leaves</LI>      <LI>oblanceolate leaves</LI>      <LI>leaf tips are serrated</LI>      <LI>leaves are dark blue-green</LI>      <LI>glaucous beneath</LI>      <LI>leaves are 2&quot; to 4&quot; long</LI>    </UL>        <P> </P>        <P> <FONT SIZE="4">Autumn      Foliage</FONT></P>    <UL>      <LI>leaves turn purplish black</LI>      <LI>not ornamentally important</LI>    </UL>        <P><FONT SIZE="4">Flowers</FONT></P>    <UL>      <LI>attractive male flowers </LI>    </UL>                <P><FONT SIZE="4">Fruit</FONT></P>    <UL>      <LI>not ornamentally important </LI>    </UL>        <P><FONT SIZE="4">Bark</FONT></P>    <UL>      <LI>purplish stem color on upperside of stem</LI>      <LI>green stem color on underside of stem</LI>      <LI>buds are small, appressed, purplish</LI>      <LI>stems are very thin</LI>      <LI>stems turn a light gray when mature</LI>    </UL>        <P><FONT SIZE="4">Culture</FONT></P>    <UL>      <LI>fast growing</LI>      <LI>easily transplanted form containers are B&amp;B</LI>      <LI>prefers moist soil</LI>      <LI>prefers to grow along water sources</LI>      <LI>full sun</LI>      <LI>pH adaptable </LI>    </UL>        <P><FONT SIZE="4">Landscape      Use</FONT></P>    <UL>      <LI>good tree for wet sites</LI>      <LI>for color</LI>      <LI>for fine texture</LI>      <LI>for flowers</LI>      <LI>stabilize stream banks</LI>      <LI>basket weaving</LI>    </UL>        <P><FONT SIZE="4">Liabilities</FONT></P>    <UL>      <LI>numerous insect and disease problems </LI>      <LI>suckers</LI>      <LI>short-lived </LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT></P>    <UL>  <LI>purple stems</LI>  <LI>alternate or opposite leaf arrangement</LI>  <LI>linear leaves with serrated leaf tips</LI>  <LI>fine texture</LI></UL><P><FONT SIZE="4">Propagation</FONT></P>    <UL>      <LI>by cuttings </LI>      <LI>by seed</LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT></P><P><b>&#39;Gracilis&#39;</b> - This plant grows larger than &#39;Nana&#39;, but features similar   blue-green leaves. It is a spreading plant that grows 6&#39; tall and wider, with   slender twigs.</P><P><b>&#39;Nana&#39;</b> - Notable both for its striking blue-green leaves and more compact   habit, this plant usually fails to grow taller than 6&#39;.</P><P><b>&#39;Pendula&#39;</b> - A form with spreading, slightly pendant branches, this plant   is often grafted on a standard to form an unusual small tree.</P><P><B>&#39;Streamco&#39;</B> - This USDA selection is very vigorous with sucker production   and self-layering. Therefore, it is very effective at bank stabilization. It   grows to at least 12&#39; tall and wide.</P>  </div>
					<script type="text/javascript">document.title = "Salix purpurea, Purpleosier Willow, Basket Willow - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>