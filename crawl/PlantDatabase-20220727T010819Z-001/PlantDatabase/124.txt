<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Cornus florida</h1>
						<h2>Flowering Dogwood</h2>
						<h3>Cornaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/corflo00.jpg' target='_blank'><img src='plantPhotos/corflo00.jpg' /></a><a href='plantPhotos/corflo01.jpg' target='_blank'><img src='plantPhotos/corflo01.jpg' /></a><a href='plantPhotos/corflo02.jpg' target='_blank'><img src='plantPhotos/corflo02.jpg' /></a><a href='plantPhotos/corflo03.jpg' target='_blank'><img src='plantPhotos/corflo03.jpg' /></a><a href='plantPhotos/corflo04.jpg' target='_blank'><img src='plantPhotos/corflo04.jpg' /></a><a href='plantPhotos/corflo05.jpg' target='_blank'><img src='plantPhotos/corflo05.jpg' /></a><a href='plantPhotos/corflo06.jpg' target='_blank'><img src='plantPhotos/corflo06.jpg' /></a><a href='plantPhotos/corflo07.jpg' target='_blank'><img src='plantPhotos/corflo07.jpg' /></a><a href='plantPhotos/corflo08.jpg' target='_blank'><img src='plantPhotos/corflo08.jpg' /></a><a href='plantPhotos/corflo09.jpg' target='_blank'><img src='plantPhotos/corflo09.jpg' /></a><a href='plantPhotos/corflo10.jpg' target='_blank'><img src='plantPhotos/corflo10.jpg' /></a><a href='plantPhotos/corflo11.jpg' target='_blank'><img src='plantPhotos/corflo11.jpg' /></a><a href='plantPhotos/corflo12.jpg' target='_blank'><img src='plantPhotos/corflo12.jpg' /></a><a href='plantPhotos/corflo13.jpg' target='_blank'><img src='plantPhotos/corflo13.jpg' /></a><a href='plantPhotos/corflo14.jpg' target='_blank'><img src='plantPhotos/corflo14.jpg' /></a><a href='plantPhotos/corflo15.jpg' target='_blank'><img src='plantPhotos/corflo15.jpg' /></a><a href='plantPhotos/corflo16.jpg' target='_blank'><img src='plantPhotos/corflo16.jpg' /></a><a href='plantPhotos/corflo17.jpg' target='_blank'><img src='plantPhotos/corflo17.jpg' /></a><a href='plantPhotos/corflo18.jpg' target='_blank'><img src='plantPhotos/corflo18.jpg' /></a><a href='plantPhotos/corflo19.jpg' target='_blank'><img src='plantPhotos/corflo19.jpg' /></a><a href='plantPhotos/corflo20.jpg' target='_blank'><img src='plantPhotos/corflo20.jpg' /></a><a href='plantPhotos/corflo21.jpg' target='_blank'><img src='plantPhotos/corflo21.jpg' /></a><a href='plantPhotos/corflo22.jpg' target='_blank'><img src='plantPhotos/corflo22.jpg' /></a><a href='plantPhotos/corflo23.jpg' target='_blank'><img src='plantPhotos/corflo23.jpg' /></a><a href='plantPhotos/corflo24.jpg' target='_blank'><img src='plantPhotos/corflo24.jpg' /></a><a href='plantPhotos/corflo25.jpg' target='_blank'><img src='plantPhotos/corflo25.jpg' /></a><a href='plantPhotos/corflo26.jpg' target='_blank'><img src='plantPhotos/corflo26.jpg' /></a><a href='plantPhotos/corflo27.jpg' target='_blank'><img src='plantPhotos/corflo27.jpg' /></a><a href='plantPhotos/corflo28.jpg' target='_blank'><img src='plantPhotos/corflo28.jpg' /></a><a href='plantPhotos/corflo29.jpg' target='_blank'><img src='plantPhotos/corflo29.jpg' /></a><a href='plantPhotos/corflo30.jpg' target='_blank'><img src='plantPhotos/corflo30.jpg' /></a><a href='plantPhotos/corflo31.jpg' target='_blank'><img src='plantPhotos/corflo31.jpg' /></a><a href='plantPhotos/corflo32.jpg' target='_blank'><img src='plantPhotos/corflo32.jpg' /></a><a href='plantPhotos/corflo33.jpg' target='_blank'><img src='plantPhotos/corflo33.jpg' /></a><a href='plantPhotos/corflo34.jpg' target='_blank'><img src='plantPhotos/corflo34.jpg' /></a><a href='plantPhotos/corflo35.jpg' target='_blank'><img src='plantPhotos/corflo35.jpg' /></a><a href='plantPhotos/corflo82.jpg' target='_blank'><img src='plantPhotos/corflo82.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT>    </P>    <UL>      <LI>native to the eastern and central United States</LI>      <LI>zone 5 with selection of proper genetic material</LI>      <LI>grows naturally as an understory tree </LI>    </UL>                <P><FONT SIZE="4">Habit      and Form</FONT> </P>    <UL>      <LI>a small deciduous tree</LI>      <LI>grows to about 30&#39; tall with an equal or greater spread</LI>      <LI>shape is rounded to somewhat flat-topped</LI>      <LI>branching is approaching horizontal</LI>      <LI>branches low to the ground with a short trunk</LI>    </UL>        <P>      </P>        <P><FONT SIZE="4">Summer      Foliage</FONT> </P>    <UL>      <LI>opposite, simple leaves</LI>      <LI>oval to ovate shape</LI>      <LI>leaves 3&quot; to 6&quot; long</LI>      <LI>leaves 1.5&#39; to 3&quot; wide</LI>      <LI>leaf color is medium green </LI>    </UL>                <P><FONT SIZE="4">Autumn      Foliage</FONT> </P>    <UL>      <LI>red or reddish purple</LI>      <LI>generally quite showy</LI>      <LI>colors early and color holds for an extended period </LI>    </UL>        <P>                  </P>        <P><FONT SIZE="4">Flowers</FONT>    </P>    <UL>      <LI>flowers are small and yellowish green</LI>      <LI>the showy part of the bloom the four white bracts</LI>      <LI>each bract is nearly 2&quot; long</LI>      <LI>the diameter of the bracts combined is about 4&quot; across</LI>      <LI>bloom time is mid-May</LI>      <LI>bloom is effective for 2 weeks</LI>      <LI>flowering occurs before leaf out </LI>    </UL>        <P>                                          </P>        <P><FONT SIZE="4">Fruit</FONT>    </P>    <UL>      <LI>bright, shiny red fruits</LI>      <LI>held in tight clusters of 3 to 4</LI>      <LI>each fruit is 0.33&quot; to 0.5&quot; long and elongated</LI>      <LI>some trees retain fruit into the winter</LI>      <LI>birds often eat the fruit</LI>    </UL>        <P>                        </P>        <P><FONT SIZE="4">Bark</FONT>    </P>    <UL>      <LI>quite attractive</LI>      <LI>develops small square or rectangular blocks</LI>      <LI>resembles alligator skin</LI>      <LI>color is dark gray, brown or black</LI>    </UL>        <P>                  </P>        <P><FONT SIZE="4">Culture</FONT>    </P>    <UL>      <LI>prefers a cool, moist, acidic soil that contains organic matter</LI>      <LI>full sun promotes greatest flowering but tolerates partial shade well</LI>      <LI>not tolerant of stresses such as heat, drought, pollution, road salt</LI>      <LI>best transplanted at a small size</LI>      <LI>can be slow to reestablish following transplanting</LI>    </UL>        <P><FONT SIZE="4">Landscape      Uses</FONT> </P>    <UL>      <LI>small groupings</LI>      <LI>lawn tree</LI>      <LI>specimen</LI>      <LI>border</LI>      <LI>naturalistic areas</LI>      <LI>edge of woods</LI>      <LI>patio tree</LI>      <LI>has four season appeal in flowers, fruits, fall, color, bark and        branching character</LI>    </UL>        <P>            </P>        <P><FONT SIZE="4">Liabilities</FONT>    </P>    <UL>      <LI>flower buds can be killed or injured by cold in zone 5</LI>      <LI>dogwood borer</LI>      <LI>dogwood anthracnose</LI>      <LI>powdery mildew</LI>      <LI>crown rot and canker</LI>      <LI>almost overused</LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT> </P>    <UL>      <LI>flower buds large and flattened</LI>      <LI>twigs reddish purple</LI>      <LI>alligator-like bark on large branches</LI>      <LI>horizontal branching</LI>      <LI>white flowers comprised of 4 bracts in May </LI>    </UL>        <P>      </P>        <P><FONT SIZE="4">Propagation</FONT>    </P>    <UL>      <LI>by seed</LI>      <LI>by bud grafting</LI>      <LI>by cuttings</LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT>    </P>        <P>Dwarf forms (such as &#39;Compacta&#39;) and fastigiate plants (such as &#39;Fastigiata&#39;)   are known, but they are rarely available and perhaps best reserved for collectors.</P><P><b>&#39;Cherokee Brave&#39;</b> - Considered one of the best red forms, with deep pink   bracts that have a white center. Will grow 15&#39; tall.</P><P><B>&#39;Cherokee Chief&#39; </B>- A popular selection with bracts that are deep red   and reddish new growth. The most popular red-pink dogwood.</P>        <P><b>&#39;Cherokee Daybreak&#39;</b> (<b>&#39;Cherokee Daybreak</b>&#153;) - A variegated   selection with white-margined leaves that age to pink in fall. Flower bracts   are white.</P><P><B>&#39;Cherokee Princess&#39;</B> - This form blooms at an early age, with very large   white flowers -- up to 5&quot; across. It blooms heavily and reliably every   year. It&#39;s always ranked near the top of dogwood evaluations.</P><P><b>&#39;Cherokee Sunset&#39;</b> (<b>Cherokee Sunset</b>&#153;) - A red-flowered form   with variegated foliage that is marked with pink/yellow. A good performer, with   strong growth to 25&#39; tall and disease resistance.</P>        <P><B>&#39;Cloud 9&#39;</B> - A slow-growing cultivar with white overlapping bracts. Very   heavy bloomer that starts producing as a young plant. Shows good cold hardiness.</P><P><b>&#39;Pendula&#39;</b> - This rare form features branches that are strongly weeping.   Flower bracts are white.</P><P><B>&#39;Plena&#39;</B> - Somewhat of a catch-all name for double, white-bracted forms.</P>        <P><B>var rubra (&#39;Rubra&#39;)</B> - This variety has flowers that range from pink   to reddish with considerable variation in color. Very common in the trade.</P>        <P>   </P>        <P><B>&#39;Welchii&#39;</B> - The leaves are variegated with a mixture of green, cream   and pink. Flowers are white and the fall color is rose red. It benefits from   a location in partial shade.</P>                <P><B>&#39;Xanthocarpa&#39;</B> - A form with unusual yellow fruits and white-bracted   flowers.</P>          </div>
					<script type="text/javascript">document.title = "Cornus florida, Flowering Dogwood - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>