<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Cornus alternifolia</h1>
						<h2>Pagoda Dogwood</h2>
						<h3>Cornaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/coralt00.jpg' target='_blank'><img src='plantPhotos/coralt00.jpg' /></a><a href='plantPhotos/coralt01.jpg' target='_blank'><img src='plantPhotos/coralt01.jpg' /></a><a href='plantPhotos/coralt02.jpg' target='_blank'><img src='plantPhotos/coralt02.jpg' /></a><a href='plantPhotos/coralt03.jpg' target='_blank'><img src='plantPhotos/coralt03.jpg' /></a><a href='plantPhotos/coralt04.jpg' target='_blank'><img src='plantPhotos/coralt04.jpg' /></a><a href='plantPhotos/coralt05.jpg' target='_blank'><img src='plantPhotos/coralt05.jpg' /></a><a href='plantPhotos/coralt06.jpg' target='_blank'><img src='plantPhotos/coralt06.jpg' /></a><a href='plantPhotos/coralt07.jpg' target='_blank'><img src='plantPhotos/coralt07.jpg' /></a><a href='plantPhotos/coralt08.jpg' target='_blank'><img src='plantPhotos/coralt08.jpg' /></a><a href='plantPhotos/coralt09.jpg' target='_blank'><img src='plantPhotos/coralt09.jpg' /></a><a href='plantPhotos/coralt10.jpg' target='_blank'><img src='plantPhotos/coralt10.jpg' /></a><a href='plantPhotos/coralt11.jpg' target='_blank'><img src='plantPhotos/coralt11.jpg' /></a><a href='plantPhotos/coralt12.jpg' target='_blank'><img src='plantPhotos/coralt12.jpg' /></a><a href='plantPhotos/coralt13.jpg' target='_blank'><img src='plantPhotos/coralt13.jpg' /></a><a href='plantPhotos/coralt41.jpg' target='_blank'><img src='plantPhotos/coralt41.jpg' /></a><a href='plantPhotos/coralt42.jpg' target='_blank'><img src='plantPhotos/coralt42.jpg' /></a><a href='plantPhotos/coralt91.jpg' target='_blank'><img src='plantPhotos/coralt91.jpg' /></a><a href='plantPhotos/coralt92.jpg' target='_blank'><img src='plantPhotos/coralt92.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT></P>    <UL>      <LI>native to the eastern United States</LI>      <LI>zone 4 </LI>    </UL>                <P><FONT SIZE="4">Habit      and Form</FONT></P>    <UL>      <LI>a small deciduous tree</LI>      <LI>reaches a maximum height of around 25&#39;</LI>      <LI>has horizontal or tiered branching</LI>      <LI>branching is also sympodial</LI>      <LI>shape is often somewhat irregular, but can be more or less rounded</LI>      <LI>loose and open density</LI>    </UL>        <P>            </P>        <P><FONT SIZE="4">Summer      Foliage</FONT></P>    <UL>      <LI>alternate leaf arrangement</LI>      <LI>foliage typically whorled near the branch tips</LI>      <LI>leaves are simple, elliptic with an acuminate tip</LI>      <LI>leaves are 2&quot; to 5&quot; long and 1&quot; to 2.5&quot; wide</LI>      <LI>fairly long petiole</LI>      <LI>leaf color is medium to dark green</LI>    </UL>        <P>            </P>        <P> <FONT SIZE="4">Autumn      Foliage</FONT></P>    <UL>      <LI>not developing an outstanding impact</LI>      <LI>a mix of yellow with reddish purple mixing in </LI>    </UL>        <P><FONT SIZE="4">Flowers</FONT></P>    <UL>      <LI>small, white flowers</LI>      <LI>borne in flat clusters</LI>      <LI>flowering occurs in late May and early June</LI>      <LI>fragrant</LI>      <LI>flowering can be described as moderately showy</LI>    </UL>        <P>            </P>        <P><FONT SIZE="4">Fruit</FONT></P>    <UL>      <LI>changes from green to blue-black, passing through a reddish stage</LI>      <LI>showy color develops in late July and august, but fruits don&#39;t        persist long</LI>      <LI>fruit stalks remains and turn a pleasing coral red color</LI>    </UL>        <P>      </P>        <P><FONT SIZE="4">Bark</FONT></P>    <UL>      <LI>older bark is gray brown and lightly ridged and furrowed</LI>      <LI>younger bark is smooth and reddish brown </LI>    </UL>        <P>                  </P>        <P><FONT SIZE="4">Culture</FONT></P>    <UL>      <LI>cool, moist, acidic soils are best</LI>      <LI>partial shade is ideal</LI>      <LI>full sun is acceptable if the site is not hot and dry</LI>      <LI>performs best in colder climates</LI>    </UL>        <P><FONT SIZE="4">Landscape      Use</FONT></P>    <UL>      <LI>works best in naturalistic areas</LI>      <LI>edges of woods</LI>      <LI>edges of shaded waterways</LI>      <LI>useful for its interesting horizontal branching</LI>      <LI>as a specimen</LI>    </UL>        <P>            </P>        <P><FONT SIZE="4">Liabilities</FONT></P>    <UL>      <LI>somewhat short-lived</LI>      <LI>needs specific site requirements</LI>      <LI>twig blight and canker are significant problems in some locations</LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT></P>    <UL>      <LI>a small tree or large shrub</LI>      <LI>sympodial branching</LI>      <LI>leaves whorled at the tips</LI>      <LI>reddish frit stalks</LI>      <LI>flat clusters of small white flowers in late spring</LI>      <LI>smooth reddish-brown bark on young branches</LI>    </UL>        <P><FONT SIZE="4">Propagation</FONT></P>    <UL>      <LI>by seed</LI>      <LI>by cuttings</LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT> </P><P><b>&#39;Argentea&#39; </b>(also sometimes listed as <b>&#39;Variegata&#39;</b>) - The only   commonly available cultivar, it is a rare form that is difficult to propagate   and therefore fetches a high price. It is a smaller, shrubby plant (to 15&#39; tall)   with layered branches and leaves that are handsomely variegated with a white   margin. Specialty nurseries are beginning to offer this selection in greater   numbers.</P>    </div>
					<script type="text/javascript">document.title = "Cornus alternifolia, Pagoda Dogwood - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>