<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Potentilla fruticosa</h1>
						<h2>Bush Cinquefoil</h2>
						<h3>Rosaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/potfru00.jpg' target='_blank'><img src='plantPhotos/potfru00.jpg' /></a><a href='plantPhotos/potfru01.jpg' target='_blank'><img src='plantPhotos/potfru01.jpg' /></a><a href='plantPhotos/potfru02.jpg' target='_blank'><img src='plantPhotos/potfru02.jpg' /></a><a href='plantPhotos/potfru03.jpg' target='_blank'><img src='plantPhotos/potfru03.jpg' /></a><a href='plantPhotos/potfru04.jpg' target='_blank'><img src='plantPhotos/potfru04.jpg' /></a><a href='plantPhotos/potfru05.jpg' target='_blank'><img src='plantPhotos/potfru05.jpg' /></a><a href='plantPhotos/potfru06.jpg' target='_blank'><img src='plantPhotos/potfru06.jpg' /></a><a href='plantPhotos/potfru07.jpg' target='_blank'><img src='plantPhotos/potfru07.jpg' /></a><a href='plantPhotos/potfru08.jpg' target='_blank'><img src='plantPhotos/potfru08.jpg' /></a><a href='plantPhotos/potfru31.jpg' target='_blank'><img src='plantPhotos/potfru31.jpg' /></a><a href='plantPhotos/potfru51.jpg' target='_blank'><img src='plantPhotos/potfru51.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT></P>    <UL>      <LI>native to the Northern Hemisphere</LI>      <LI>hardy to zone 2; does not perform well in warmer climates (zone 7 and        higher)</LI>    </UL>        <P><FONT SIZE="4">Habit      and Form</FONT></P>    <UL>      <LI>a small, deciduous shrub </LI>      <LI>dense shrub with numerous upright branches</LI>      <LI>rounded habit</LI>      <LI>1&#39; to 4&#39; tall with an equal or greater spread</LI>      <LI>fine texture</LI>      <LI>slow growth rate</LI>    </UL>        <P>      </P>        <P><FONT SIZE="4">Summer      Foliage</FONT></P>    <UL>      <LI>alternate leaf arrangement</LI>      <LI>compound, pinnate leaves</LI>      <LI>leaflets are elliptic and linear</LI>      <LI>3 to 7 leaflets per leaf</LI>      <LI>medium blue-green to dark green leaf color</LI>      <LI>plant leafs out in early Spring</LI>      <LI>leaves are about 1&quot; long</LI>    </UL>                <P> <FONT SIZE="4">Autumn      Foliage</FONT></P>    <UL>      <LI>fall color is yellow-brown</LI>      <LI>not ornamentally important </LI>    </UL>        <P><FONT SIZE="4">Flowers</FONT></P>    <UL>      <LI>yellow, buttercup flowers</LI>      <LI>blooms from June until the first frost</LI>      <LI>about 1&quot; to 1.5&quot; in diameter</LI>    </UL>        <P>                              </P>        <P><FONT SIZE="4">Fruit</FONT></P>    <UL>      <LI>dry, brown achene</LI>      <LI>not showy</LI>      <LI>persist through winter</LI>    </UL>        <P><FONT SIZE="4">Bark</FONT></P>    <UL>      <LI>not ornamentally important </LI>      <LI>peeling bark</LI>    </UL>        <P><FONT SIZE="4">Culture</FONT></P>    <UL>      <LI>easy to grow</LI>      <LI>easily transplanted</LI>      <LI>soil adaptable</LI>      <LI>does well in extremely cold temperatures </LI>      <LI>full sun is best </LI>      <LI>to keep dense round habit, remove canes during winter or cut to        ground</LI>    </UL>        <P><FONT SIZE="4">Landscape      Use</FONT></P>    <UL>      <LI>shrub borders</LI>      <LI>foundation plant</LI>      <LI>facer plant </LI>      <LI>for flowers</LI>      <LI>mass plantings</LI>    </UL>        <P>      </P>        <P><FONT SIZE="4">Liabilities</FONT></P>    <UL>      <LI>somewhat difficult plant to keep looking tidy</LI>      <LI>few pest problems</LI>      <LI>spider mites</LI>      <LI>does not like warm night temperatures</LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT></P>    <UL>      <LI>alternate leaf arrangement</LI>      <LI>pinnately compound leaves with 3 to 7 leaflets</LI>      <LI>yellow buttercup flowers June until frost</LI>      <LI>persistent achene</LI>    </UL>        <P><FONT SIZE="4">Propagation</FONT></P>    <UL>      <LI>by cuttings </LI>      <LI>by seed</LI>      <LI>by tissue culture</LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT></P><P>Literally dozens of cultivars have been selected, mostly for variations in   flower color. Presented below are abbreviated descriptions of a representative   sampling of the most common forms.</P><P><b>&#39;Abbotswood&#39;</b> - Considered one of the best, this low 3&#39; form is disease-free   and bears white blooms over a long period. <b>&#39;Abbotswood Silver&#39;</b> is similar,   but bears leaves variegated with white margins. <b>&#39;McKay&#39;s White&#39;</b> is an   alternative with creamy-white blooms that only grows 2.5&quot; tall.</P><P><b>&#39;Absaraka&#39;</b> (<b>Dakota Goldrush</b>&reg;) and <b>&#39;Fargo&#39;</b> (<b>Dakota   Sunspot</b>&reg;) - These two selections come out of North Dakota and feature   larger yellow flowers that are produced over a long period on dwarf, compact   rounded 3&#39; plants.</P><P><b>&#39;Coronation Triumph&#39;</b> - This 4&#39; mounded form begins blooming early in   the season and continues for a very long period with yellow blooms.</P><P><b>&#39;Gold Drop&#39;</b> (also known as <b>&#39;Farreri&#39;</b>) - A classic cultivar that   is still considered one of the best, this rounded 3&#39; plant bears profuse yellow   blooms all season.</P><P><b>&#39;Goldfinger&#39;</b> - Common in the trade, this 3&#39; mound bears large 1.5&quot;   yellow blooms over an extended period.</P><P><b>&#39;Jackmannii&#39;</b> - A larger rounded selection, this classic form displays   large deep yellow flowers all summer.</P><P><b>&#39;Katherine Dykes&#39;</b> - An old selection, this form has a 2&#39;-3&#39; spreading-arching   habit studded with light yellow blooms all summer.</P><P><b>&#39;Longacre&#39; </b>and <b>&#39;Yellow Gem&#39;</b> - Useful for their low, spreading   groundcover habits, these two selections bear yellow blooms all season. &#39;Yellow   Gem&#39; has the added bonus of reddish young twigs and gray-green foliage.</P><P><b>&#39;Pink Beauty&#39; </b>- A breakthough in this species, this plant bears clear   pink blooms on a 2&#39; rounded plant. In climates with warm summer nights, the   flower color may not hold as well. <b>&#39;Pink Pearl&#39;</b> and <b>&#39;Pink Whisper&#39;</b>   are two other pink-flowered forms, though the blooms may fade to yellow in warm   climates.</P><P><b>&#39;Primrose Beauty&#39;</b> - Producing light yellow blooms from late spring to   frost, this 3&#39; spreading plant is also notable for its silvery-gray foliage.</P><P><b>&#39;Snowbird&#39;</b> - One of the few double-flowered cultivars available, this   3&#39; upright form also has good lustrous foliage. The white double blooms may   not show extra petals until the plant is established.</P><P><b>&#39;Sunset&#39; </b>- Depending on exposure and climate, this newer 16&quot; tall   spreading form bears yellow blooms suffused with orange-reddish hues.</P><P><b>&#39;Tangerine&#39;</b> - A color breakthough, this selection bears yellow flowers   flushed with orange-copper. It is a mounded-spreading plant that grows to 2&#39;   tall and wider.</P></div>
					<script type="text/javascript">document.title = "Potentilla fruticosa, Bush Cinquefoil - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>