<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Hydrangea arborescens</h1>
						<h2>Smooth Hydrangea</h2>
						<h3>Hydrangeaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/hydarb00.jpg' target='_blank'><img src='plantPhotos/hydarb00.jpg' /></a><a href='plantPhotos/hydarb01.jpg' target='_blank'><img src='plantPhotos/hydarb01.jpg' /></a><a href='plantPhotos/hydarb02.jpg' target='_blank'><img src='plantPhotos/hydarb02.jpg' /></a><a href='plantPhotos/hydarb03.jpg' target='_blank'><img src='plantPhotos/hydarb03.jpg' /></a><a href='plantPhotos/hydarb04.jpg' target='_blank'><img src='plantPhotos/hydarb04.jpg' /></a><a href='plantPhotos/hydarb06.jpg' target='_blank'><img src='plantPhotos/hydarb06.jpg' /></a><a href='plantPhotos/hydarb07.jpg' target='_blank'><img src='plantPhotos/hydarb07.jpg' /></a><a href='plantPhotos/hydarb08.jpg' target='_blank'><img src='plantPhotos/hydarb08.jpg' /></a><a href='plantPhotos/hydarb09.jpg' target='_blank'><img src='plantPhotos/hydarb09.jpg' /></a><a href='plantPhotos/hydarb104.jpg' target='_blank'><img src='plantPhotos/hydarb104.jpg' /></a><a href='plantPhotos/hydarb50.jpg' target='_blank'><img src='plantPhotos/hydarb50.jpg' /></a><a href='plantPhotos/hydarbgrandiflora50.jpg' target='_blank'><img src='plantPhotos/hydarbgrandiflora50.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT></P>    <UL>      <LI>native to Southern New England across through the Midwest and down        through the southern states</LI>      <LI>hardy to zone 4, and warmer parts of 3</LI>    </UL>        <P><FONT SIZE="4">Habit      and Form</FONT></P>    <UL>      <LI>a deciduous shrub</LI>      <LI>low growing, rounded habit</LI>      <LI>shrub is formed by numerous unbranched canes</LI>      <LI>3&#39; to 5&#39; tall and generally wider than tall</LI>      <LI>suckers</LI>      <LI>coarse texture</LI>      <LI>fast growth rate</LI>    </UL>        <P>      </P>        <P><FONT SIZE="4">Summer      Foliage</FONT></P>    <UL>      <LI>deciduous simple leaves</LI>      <LI>opposite leaf arrangement</LI>      <LI>elliptical leaf shape</LI>      <LI>2&quot; to 8&quot; long</LI>      <LI>2&quot; to 5&quot; wide</LI>      <LI>cordate leaf base</LI>      <LI>serrated leaf margins</LI>      <LI>dark green leaf color</LI>    </UL>        <P>      </P>        <P> <FONT SIZE="4">Autumn      Foliage</FONT></P>    <UL>      <LI>leaves turn pale yellow and fall off</LI>      <LI>not ornamentally significant</LI>    </UL>        <P><FONT SIZE="4">Flowers</FONT></P>    <UL>      <LI>large flowers composed of sterile and fertile flowers</LI>      <LI>dull white to pinkish color</LI>      <LI>blooms in June and continues throughout the summer</LI>      <LI>clusters are 4&quot; to 6&quot; wide</LI>      <LI>showy</LI>    </UL>        <P>            </P>        <P><FONT SIZE="4">Fruit</FONT></P>    <UL>      <LI>dry capsule</LI>      <LI>persists through winter</LI>      <LI>not ornamentally important </LI>    </UL>        <P><FONT SIZE="4">Bark</FONT></P>    <UL>      <LI>shredding bark</LI>      <LI>unbranched canes</LI>      <LI>orange-brown color</LI>      <LI>stout gray older stems</LI>    </UL>        <P><FONT SIZE="4">Culture</FONT></P>    <UL>      <LI>transplants well from containers</LI>      <LI>adaptable to most conditions</LI>      <LI>partial shade</LI>      <LI>flowers on new wood</LI>      <LI>acidic to neutral soil</LI>    </UL>        <P><FONT SIZE="4">Landscape      Use</FONT></P>    <UL>      <LI>in northern areas used as a perennial</LI>      <LI>for flowering effect</LI>      <LI>for summer flowers</LI>      <LI>shrub border, massing</LI>      <LI>for shady areas</LI>    </UL>        <P><FONT SIZE="4">Liabilities</FONT></P>    <UL>      <LI>lack of winter hardiness</LI>      <LI>blight</LI>      <LI>leaf spot</LI>      <LI>powdery mildew</LI>      <LI>aphids</LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT></P>    <UL>      <LI>stems emerge from ground with few branches</LI>      <LI>large opposite, serrated leaves</LI>      <LI>imbricate buds with 4 to 6 scales</LI>      <LI>buds are greenish brown and glabrous</LI>      <LI>balls of flowers covering plant in summer </LI>      <LI>persistent flower panicles</LI>    </UL>                <P><FONT SIZE="4">Propagation</FONT></P>    <UL>      <LI>by cuttings </LI>      <LI>by seed</LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT></P>        <p><b>&#39;Annabelle&#39;</b> - This popular selection is often mislabeled in the trade   or applied falsely. The true cultivar bears huge rounded clusters (up to 12&quot;   wide) of white sterile florets. The flower heads are rounded and held upright,   thus the plant is considered superior to &#39;Grandiflora&#39;.</p><P><B>&#39;Grandiflora&#39;</B> - The common &quot;Hills of Snow Hydrangea&quot;, this   plant offers heads of white sterile florets 6&quot;-8&quot; wide. The number   of flowers in each rounded cluster is less than &#39;Annabelle&#39;, and the clusters   do not age well. <b>&#39;Sterilis&#39;</b> is similar, but the bloom heads are flatter   and the sterile florets are not borne as densely.</P><p>  </div>
					<script type="text/javascript">document.title = "Hydrangea arborescens, Smooth Hydrangea - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>