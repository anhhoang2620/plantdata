<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Cotoneaster horizontalis</h1>
						<h2>Rockspray Cotoneaster, Rock Cotoneaster</h2>
						<h3>Rosaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/cothor00.jpg' target='_blank'><img src='plantPhotos/cothor00.jpg' /></a><a href='plantPhotos/cothor01.jpg' target='_blank'><img src='plantPhotos/cothor01.jpg' /></a><a href='plantPhotos/cothor02.jpg' target='_blank'><img src='plantPhotos/cothor02.jpg' /></a><a href='plantPhotos/cothor03.jpg' target='_blank'><img src='plantPhotos/cothor03.jpg' /></a><a href='plantPhotos/cothor04.jpg' target='_blank'><img src='plantPhotos/cothor04.jpg' /></a><a href='plantPhotos/cothor05.jpg' target='_blank'><img src='plantPhotos/cothor05.jpg' /></a><a href='plantPhotos/cothor06.jpg' target='_blank'><img src='plantPhotos/cothor06.jpg' /></a><a href='plantPhotos/cothor07.jpg' target='_blank'><img src='plantPhotos/cothor07.jpg' /></a><a href='plantPhotos/cothor08.jpg' target='_blank'><img src='plantPhotos/cothor08.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT></P>    <UL>      <LI>native to Western China</LI>      <LI>zone 5 </LI>      <LI>tolerant to -20<SUP>o</SUP>F, but some damage will occur</LI>    </UL>        <P><FONT SIZE="4">Habit      and Form</FONT></P>    <UL>      <LI>semi-evergreen, multi-stemmed shrub</LI>      <LI>5&#39; to 6&#39; tall and 6&#39; to 8&#39; wide</LI>      <LI>spreading, irregular habit</LI>      <LI>branches are slender and drooping</LI>      <LI>fine texture</LI>      <LI>medium to fast growth rate</LI>    </UL>        <P> </P>        <P><FONT SIZE="4">Summer      Foliage</FONT></P>    <UL>  <LI>leaves are alternate and simple</LI>  <LI>leave margins are entire</LI>  <LI>0.20&quot; to 0.50&quot; long</LI>  <LI>glabrous on upperside of leaf</LI>  <LI>pubescent on underside of leaf</LI>  <LI>dark green color</LI>  <LI>leaf blade is flat</LI></UL>        <P> </P><P> <FONT SIZE="4">Autumn   Foliage</FONT></P>    <UL>      <LI>purplish winter color</LI>    </UL>        <P>    </P>        <P><FONT SIZE="4">Flowers</FONT></P>    <UL>      <LI>light pink flowers</LI>      <LI>blooms in late May</LI>      <LI>flowers are small, but in abundance are showy</LI>    </UL>        <P>    </P>        <P><FONT SIZE="4">Fruit</FONT></P>    <UL>      <LI>red small, pome fruits</LI>      <LI>0.25&quot; in diameter</LI>      <LI>held through winter</LI>      <LI>ripen in September through October</LI>      <LI>can be showy</LI>    </UL>        <P><FONT SIZE="4">Bark</FONT></P>    <UL>      <LI>gray</LI>      <LI>smooth</LI>      <LI>stems have a fishbone pattern </LI>    </UL>        <P><FONT SIZE="4">Culture</FONT></P>    <UL>      <LI>transplant from containers only because of sparse root system</LI>      <LI>prefers well-drained, fertile soil</LI>      <LI>needs adequate moisture</LI>      <LI>wind tolerant</LI>      <LI>pH adaptable</LI>      <LI>full sun to partial shade</LI>      <LI>prune tolerant</LI>    </UL>        <P><FONT SIZE="4">Landscape      Use</FONT></P>    <UL>      <LI>groundcover</LI>      <LI>bank cover</LI>      <LI>groupings or mass</LI>      <LI>espalier</LI>      <LI>for flowers or fruit afect</LI>    </UL>                <P><FONT SIZE="4">Liabilities</FONT></P>    <UL>      <LI>bees are attractive to flowers</LI>      <LI>fireblight</LI>      <LI>leaf spot</LI>      <LI>spider mites</LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT></P>    <UL>      <LI>fishbone stem pattern</LI>      <LI>red, pome fruits</LI>      <LI>irregular growth habit</LI>      <LI>long drooping branches</LI>      <LI>alternate leaves</LI>      <LI>pubescence on underside of leaf</LI>    </UL>                <P><FONT SIZE="4">Propagation</FONT></P>    <UL>      <LI>by seed</LI>      <LI>cuttings, wood collected June through August</LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT> </P><P><b>&#39;Perpusillus&#39;</b> - One of the lowest, most prostrate selections. This plant   only grows to 1&#39; tall and up to 7&#39; wide. The glossy deep green leaves are alleged   to be susceptible to fireblight.</P><P><B>&#39;Tom Thumb</B>&#39; (also listed as <b>&#39;Little Gem&#39;</b>) - Has a compact, dense,   closely-branched habit that develops into a broad-spreading mound with dark   green leaves. Makes a good rock garden specimen. Some authorities place this   cultivar under <I>Cotoneaster adpressus</I>.</P>        <P><b>&#39;Variegatus&#39;</b> - A widely available, slow-growing form with tiny leaves   margined with white. Grows slowly to 3&#39; tall and 6&#39; wide with layered branches.   Interesting in that it has some of the smallest variegated leaves of any plant.</P>  </div>
					<script type="text/javascript">document.title = "Cotoneaster horizontalis, Rockspray Cotoneaster, Rock Cotoneaster - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>