<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Dirca palustris</h1>
						<h2>Leatherwood, Wicopy</h2>
						<h3>Thymelaeaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/dirpal00.jpg' target='_blank'><img src='plantPhotos/dirpal00.jpg' /></a><a href='plantPhotos/dirpal01.jpg' target='_blank'><img src='plantPhotos/dirpal01.jpg' /></a><a href='plantPhotos/dirpal02.jpg' target='_blank'><img src='plantPhotos/dirpal02.jpg' /></a><a href='plantPhotos/dirpal03.jpg' target='_blank'><img src='plantPhotos/dirpal03.jpg' /></a><a href='plantPhotos/dirpal04.jpg' target='_blank'><img src='plantPhotos/dirpal04.jpg' /></a><a href='plantPhotos/dirpal05.jpg' target='_blank'><img src='plantPhotos/dirpal05.jpg' /></a><a href='plantPhotos/dirpal06.jpg' target='_blank'><img src='plantPhotos/dirpal06.jpg' /></a><a href='plantPhotos/dirpal07.jpg' target='_blank'><img src='plantPhotos/dirpal07.jpg' /></a><a href='plantPhotos/dirpal12.jpg' target='_blank'><img src='plantPhotos/dirpal12.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT></P>    <UL>      <LI>native to eastern North America</LI>      <LI>zone 4</LI>    </UL>        <P><FONT SIZE="4">Habit      and Form</FONT></P>    <UL>      <LI>a deciduous shrub</LI>      <LI>multistemmed</LI>      <LI>dense, rounded habit</LI>      <LI>3&#39; to 6&#39; tall with equal width</LI>      <LI>slow growth rate</LI>      <LI>medium texture</LI>    </UL>        <P>      </P>        <P><FONT SIZE="4">Summer      Foliage</FONT></P>    <UL>      <LI>alternate leaf arrangement</LI>      <LI>simple, deciduous leaves</LI>      <LI>obovate leaf shape</LI>      <LI>up to 3&quot; long and 1.5&quot; wide</LI>      <LI>entire leaf margin</LI>      <LI>light green leaf color</LI>      <LI>young leaves are pubescent</LI>    </UL>        <P>            </P>        <P> <FONT SIZE="4">Autumn      Foliage</FONT></P>    <UL>      <LI>yellow fall color</LI>      <LI>color varies from tree to tree</LI>    </UL>        <P><FONT SIZE="4">Flowers</FONT></P>    <UL>      <LI>pale yellow flowers</LI>      <LI>clustered in 3 or 4&#39;s</LI>      <LI>blooms early spring</LI>    </UL>        <P>      </P>        <P><FONT SIZE="4">Fruit</FONT></P>    <UL>      <LI>gray to reddish drupe</LI>      <LI>0.33&quot; across</LI>      <LI>1 large brownish seed</LI>      <LI>ripens in mid summer </LI>    </UL>        <P><FONT SIZE="4">Bark</FONT></P>    <UL>      <LI>light greenish brown stems</LI>      <LI>slender</LI>      <LI>white lenticels</LI>      <LI>aromatic stems when crushed</LI>    </UL>        <P>      </P>        <P><FONT SIZE="4">Culture</FONT></P>    <UL>      <LI>full sun</LI>      <LI>prefers moist, deep soils </LI>    </UL>        <P><FONT SIZE="4">Landscape      Use</FONT></P>    <UL>      <LI>naturalized area</LI>      <LI>mass or group</LI>      <LI>wet sites</LI>    </UL>        <P><FONT SIZE="4">Liabilities</FONT></P>    <UL>      <LI>no serious problems </LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT></P>    <UL>      <LI>no terminal buds</LI>      <LI>lateral buds are hidden by petiole</LI>      <LI>lateral buds are conical with 4 hairy scales</LI>      <LI>alternate leaf arrangement </LI>      <LI>yellow flowers</LI>      <LI>obovate leaf shape</LI>    </UL>        <P><FONT SIZE="4">Propagation</FONT></P>    <UL>      <LI>by seed</LI>      <LI>by layering</LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT>    </P>    <UL>      <LI>none</LI>    </UL>  </div>
					<script type="text/javascript">document.title = "Dirca palustris, Leatherwood, Wicopy - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>