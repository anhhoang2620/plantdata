<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Sorbus aucuparia</h1>
						<h2>European Mountainash, Common Mountainash</h2>
						<h3>Rosaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/sorauc00.jpg' target='_blank'><img src='plantPhotos/sorauc00.jpg' /></a><a href='plantPhotos/sorauc01.jpg' target='_blank'><img src='plantPhotos/sorauc01.jpg' /></a><a href='plantPhotos/sorauc02.jpg' target='_blank'><img src='plantPhotos/sorauc02.jpg' /></a><a href='plantPhotos/sorauc03.jpg' target='_blank'><img src='plantPhotos/sorauc03.jpg' /></a><a href='plantPhotos/sorauc04.jpg' target='_blank'><img src='plantPhotos/sorauc04.jpg' /></a><a href='plantPhotos/sorauc05.jpg' target='_blank'><img src='plantPhotos/sorauc05.jpg' /></a><a href='plantPhotos/sorauc06.jpg' target='_blank'><img src='plantPhotos/sorauc06.jpg' /></a><a href='plantPhotos/sorauc07.jpg' target='_blank'><img src='plantPhotos/sorauc07.jpg' /></a><a href='plantPhotos/sorauc08.jpg' target='_blank'><img src='plantPhotos/sorauc08.jpg' /></a><a href='plantPhotos/sorauc09.jpg' target='_blank'><img src='plantPhotos/sorauc09.jpg' /></a><a href='plantPhotos/sorauc10.jpg' target='_blank'><img src='plantPhotos/sorauc10.jpg' /></a><a href='plantPhotos/sorauc11.jpg' target='_blank'><img src='plantPhotos/sorauc11.jpg' /></a><a href='plantPhotos/sorauc12.jpg' target='_blank'><img src='plantPhotos/sorauc12.jpg' /></a><a href='plantPhotos/sorauc13.jpg' target='_blank'><img src='plantPhotos/sorauc13.jpg' /></a><a href='plantPhotos/sorauc14.jpg' target='_blank'><img src='plantPhotos/sorauc14.jpg' /></a><a href='plantPhotos/sorauc15.jpg' target='_blank'><img src='plantPhotos/sorauc15.jpg' /></a><a href='plantPhotos/sorauc16.jpg' target='_blank'><img src='plantPhotos/sorauc16.jpg' /></a><a href='plantPhotos/sorauc17.jpg' target='_blank'><img src='plantPhotos/sorauc17.jpg' /></a><a href='plantPhotos/sorauc18.jpg' target='_blank'><img src='plantPhotos/sorauc18.jpg' /></a><a href='plantPhotos/sorauc19.jpg' target='_blank'><img src='plantPhotos/sorauc19.jpg' /></a><a href='plantPhotos/sorauc20.jpg' target='_blank'><img src='plantPhotos/sorauc20.jpg' /></a><a href='plantPhotos/sorauc21.jpg' target='_blank'><img src='plantPhotos/sorauc21.jpg' /></a><a href='plantPhotos/sorauc22.jpg' target='_blank'><img src='plantPhotos/sorauc22.jpg' /></a><a href='plantPhotos/sorauc23.jpg' target='_blank'><img src='plantPhotos/sorauc23.jpg' /></a><a href='plantPhotos/sorauc24.jpg' target='_blank'><img src='plantPhotos/sorauc24.jpg' /></a><a href='plantPhotos/sorauc25.jpg' target='_blank'><img src='plantPhotos/sorauc25.jpg' /></a><a href='plantPhotos/sorauc26.jpg' target='_blank'><img src='plantPhotos/sorauc26.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT>    </P>    <UL>      <LI>native to northern Europe and Asia</LI>      <LI>zone 3</LI>    </UL>        <P><FONT SIZE="4">Habit      and Form</FONT> </P>    <UL>      <LI>a small to medium-sized, deciduous tree </LI>      <LI>typically 20&#39; to 40&#39; tall</LI>      <LI>shape is ovate to rounded</LI>      <LI>branching is strongly upright and spreading</LI>      <LI>main trunk is often short, becoming very multi-branched</LI>    </UL>        <P>         </P>        <P><FONT SIZE="4">Summer      Foliage</FONT> </P>    <UL>      <LI>leaves are alternately arranged</LI>      <LI>pinnately compound</LI>      <LI>5&quot; to 9&quot; long</LI>      <LI>9 to 15 leaflets is most common</LI>      <LI>leaflets are .75&quot; to 2.5&quot; long with a pointed oval shape</LI>      <LI>margins of leaflets are serrated toward the tips</LI>      <LI>dark green above, frosty green below</LI>    </UL>        <P>         </P>        <P><FONT SIZE="4">Autumn      Foliage</FONT> </P>    <UL>      <LI>leaves change from green to yellow to orange to somewhat reddish</LI>      <LI>can be showy or disappointing</LI>    </UL>                <P><FONT SIZE="4">Flowers</FONT>    </P>    <UL>      <LI>blooms in May</LI>      <LI>rounded, flat-topped clusters</LI>      <LI>white color</LI>      <LI>malodorous</LI>      <LI>individual flowers only 0.33&quot; across.</LI>      <LI>relatively showy, but not spectacular </LI>    </UL>        <P>      </P>        <P><FONT SIZE="4">Fruit</FONT>    </P>    <UL>      <LI>small, orange-red fruits</LI>      <LI>0.25&quot; to 0.38&quot; diameter</LI>      <LI>born in terminal clusters that ripen in late August and September</LI>      <LI>fruit is quite attractive</LI>      <LI>birds like to eat the fruit</LI>    </UL>        <P>               </P>        <P><FONT SIZE="4">Bark</FONT>    </P>    <UL>      <LI>color is light grayish brown</LI>      <LI>generally smooth, but somewhat roughened on older trunks</LI>      <LI>relatively attractive</LI>    </UL>        <P>      </P>        <P><FONT SIZE="4">Culture</FONT>    </P>    <UL>      <LI>prefers cool to cold climates best</LI>      <LI>likes well-drained, loamy acidic soils</LI>      <LI>avoid high pH soils</LI>      <LI>avoid hot, dry locations</LI>      <LI>avoid compacted soils and pollution</LI>      <LI>full sun</LI>    </UL>        <P><FONT SIZE="4">Landscape      Uses</FONT> </P>    <UL>      <LI>excellent for effect in fruit</LI>      <LI>good performer in cold climates</LI>      <LI>as a smaller shade tree</LI>      <LI>fruits attract birds</LI>      <LI>specimen</LI>    </UL>        <P>   </P>        <P><FONT SIZE="4">Liabilities</FONT>    </P>    <UL>      <LI>stress predisposes the plant to disease and insect problems</LI>      <LI>very susceptible to borers</LI>      <LI>frequently develops fire blight</LI>      <LI>other insect and disease pests include cankers, rusts, aphids,        sawflies, and scales</LI>      <LI>often short-lived in the landscape</LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT> </P>    <UL>  <LI>very large terminal buds</LI>  <LI>smooth bark</LI>  <LI>short main trunk dividing into multiple trunks</LI>  <LI>upright oval branching</LI>  <LI>pinnately compound leaves</LI>  <LI>a dark band is beneath each leaf scar</LI>  <LI>white flowers in flat clusters</LI>  <LI>orange red fruit in terminal clusters</LI></UL><P><FONT SIZE="4">Propagation</FONT>    </P>    <UL>      <LI>by seed</LI>      <LI>cultivars generally grafted</LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT>    </P>        <P><b>&#39;Asplenifolia&#39;</b> - A selection for more incised leaflets, this plant does   not differ markedly from the species.</P>        <P><b>&#39;Fastigiata&#39;</b> - This is an upright form with coarse, ascending branches.   It produces red fruit and dark green foliage</P>        <P><b>&#39;Michred&#39;</b> (<b>Cardinal Royal</b>&#153;) - This is perhaps the most common   commercial selection. A introduction out of Michigan, the plant is a vigorous   symmetrical grower to 30&#39; tall and 20&#39; wide. The habit is upright-oval and the   plant bears profuse red fruit amidst glossy green foliage (silvery beneath).</P>        <P><b>&#39;Pendula&#39;</b> - A novelty form that is not widely available, this plant   has drooping branches that are not regular or very graceful. It grows to 20&#39;   tall and fruits well. </P>        <P><b>&#39;Xanthocarpa&#39; </b>-Notable for its yellow-orange fruit, this selection is   otherwise similar to the species. <b>&#39;Brilliant Yellow&#39;</b> is a named selection   with yellow fruit on a oval tree 30&#39; tall with a spread of 20&#39;.</P>          </div>
					<script type="text/javascript">document.title = "Sorbus aucuparia, European Mountainash, Common Mountainash - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>