<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Quercus bicolor</h1>
						<h2>Swamp White Oak</h2>
						<h3>Fagaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/quebic00.jpg' target='_blank'><img src='plantPhotos/quebic00.jpg' /></a><a href='plantPhotos/quebic01.jpg' target='_blank'><img src='plantPhotos/quebic01.jpg' /></a><a href='plantPhotos/quebic02.jpg' target='_blank'><img src='plantPhotos/quebic02.jpg' /></a><a href='plantPhotos/quebic03.jpg' target='_blank'><img src='plantPhotos/quebic03.jpg' /></a><a href='plantPhotos/quebic04.jpg' target='_blank'><img src='plantPhotos/quebic04.jpg' /></a><a href='plantPhotos/quebic05.jpg' target='_blank'><img src='plantPhotos/quebic05.jpg' /></a><a href='plantPhotos/quebic06.jpg' target='_blank'><img src='plantPhotos/quebic06.jpg' /></a><a href='plantPhotos/quebic07.jpg' target='_blank'><img src='plantPhotos/quebic07.jpg' /></a><a href='plantPhotos/quebic08.jpg' target='_blank'><img src='plantPhotos/quebic08.jpg' /></a><a href='plantPhotos/quebic09.jpg' target='_blank'><img src='plantPhotos/quebic09.jpg' /></a><a href='plantPhotos/quebic50.jpg' target='_blank'><img src='plantPhotos/quebic50.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT></P>    <UL>      <LI>native to eastern United States</LI>      <LI>hardy to zone 4</LI>    </UL>                <P><FONT SIZE="4">Habit      and Form</FONT></P>    <UL>      <LI>a medium-sized, deciduous tree</LI>      <LI>upright oval crown, open </LI>      <LI>50 to 60&#39; tall</LI>      <LI>50&#39; to 60&#39; wide</LI>      <LI>coarse texture</LI>      <LI>moderate growth rate</LI>    </UL>        <P>   </P>        <P><FONT SIZE="4">Summer      Foliage</FONT></P>    <UL>      <LI>alternate leaf arrangement</LI>      <LI>simple, deciduous leaves</LI>      <LI>obovate leaf shape</LI>      <LI>4&quot; to 8&quot; long</LI>      <LI>2&quot; to 4        <BR>&quot; wide</LI>      <LI>lobed, rounded</LI>      <LI>white hairs on leaf underside</LI>      <LI>dark green leaf color</LI>    </UL>        <P> <FONT SIZE="4">Autumn      Foliage</FONT></P>    <UL>      <LI>copper to red fall color</LI>    </UL>        <P><FONT SIZE="4">Flowers</FONT></P>    <UL>      <LI>monoecious</LI>      <LI>male flowers are pendulous yellow-green catkins</LI>      <LI>blooms in May</LI>      <LI>messy</LI>    </UL>        <P>            </P>        <P><FONT SIZE="4">Fruit</FONT></P>    <UL>      <LI>rounded acorn</LI>      <LI>0.5&#39; to 1&quot; across</LI>      <LI>usually found in pairs</LI>      <LI>long peduncle</LI>      <LI>involucre covers a third of acorn</LI>      <LI>attracts wildlife</LI>    </UL>        <P><FONT SIZE="4">Bark</FONT></P>    <UL>      <LI>lower branches droop</LI>      <LI>yellowish brown stems</LI>      <LI>deeply furrowed and ridged bark</LI>      <LI>flaky</LI>      <LI>dark brown bark color </LI>    </UL>        <P>                  </P>        <P><FONT SIZE="4">Culture</FONT></P>    <UL>      <LI>part shade to full sun</LI>      <LI>likes swampy situations</LI>      <LI>prefers acidic soil</LI>      <LI>transplant from container</LI>      <LI>drought tolerant</LI>    </UL>        <P><FONT SIZE="4">Landscape      Use</FONT></P>    <UL>      <LI>shade tree</LI>      <LI>lawn tree</LI>      <LI>specimen</LI>      <LI>park tree</LI>      <LI>for large area</LI>    </UL>        <P><FONT SIZE="4">Liabilities</FONT></P>    <UL>      <LI>borers oak slug</LI>      <LI>caterpillars</LI>      <LI>variable oak caterpillars</LI>      <LI>gypsy moth</LI>      <LI>anthracnose</LI>      <LI>canker</LI>      <LI>powdery mildew</LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT></P>    <UL>      <LI>leaves with rounded lobes</LI>      <LI>imbricate, ovate, chestnut brown, 0.25&quot; long buds that are        pubescent</LI>      <LI>dark brown, flaky bark</LI>      <LI>acorns usually found in pairs</LI>    </UL>                <P><FONT SIZE="4">Propagation</FONT></P>    <UL>      <LI>by seed</LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT></P><P><b>&#39;Asjes&#39;</b> (<b>Rosehill</b>&reg;) - This plant is most likely a hybrid   with <i>Q. robur</i> (English oak), which it strongly resembles. It is resistant   to leaf ailments such as mildew and features a somewhat fastigiate, upright   form with lustrous green foliage. The acorns resemble those of English oak.</P><P><b>&#39;Long&#39; </b>(<b>Regal Prince</b>&reg;, a selection of <i>Quercus </i>x <i>warei</i>)   - This new selection is the result of a cross with a fastigiate English oak   (<i>Quercus robur</i> &#39;Fastigiata). It is a vigorous upright-oval grower reaching   60&#39; tall and only 25&#39; wide. The foliage is handsome all season with a silvery   lower surface. Due to its parentage, this plant is tolerant of a wide range   of conditions, including both wet and dry soil.</P></div>
					<script type="text/javascript">document.title = "Quercus bicolor, Swamp White Oak - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>