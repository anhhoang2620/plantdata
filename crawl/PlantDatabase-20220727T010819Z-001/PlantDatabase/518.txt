<!doctype html>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"><title class="cushycms" id="Page Title">Plant Database</title>
  <link href="https://cag.uconn.edu/common/css/banner.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/bootstrap-theme.css" rel="stylesheet">
  <link href="https://cag.uconn.edu/common/css/screen.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://cag.uconn.edu/common/js/bootstrap.min.js"></script>  <style>
  /**
   * The banner assumes a margin of 0px, but is not included
   * in banner.css to avoid collisions with other themes and 
   * admin bars.
   */
  body {
    margin:0px;
  }
  </style>

  <!-- REPLACE THIS WITH YOUR CUSTOM BUILD OF MODERNIZR -->
  <script src="//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>

  <!-- Use if you need responsive layouts for IE -->
  <script src="https://cag.uconn.edu/common/js/respond.js"></script>

</head>

<body>
<div id="uconn-banner">
  <div id="uconn-header-container">
    <div class="container">
      <div class="row-fluid">
        <div id="home-link-container">
          <a id="home-link" href="http://uconn.edu/">
            <span id="wordmark">UCONN</span>
            <span id="university-of-connecticut">UNIVERSITY OF CONNECTICUT</span>
          </a>
        </div>
          <div id="button-container">
            <span>
              <a class="btn" href="http://uconn.edu/search.php" title="Search"><i class="icon-search"></i>
              </a>
            </span>
            <span>
              <a class="btn" href="http://uconn.edu/azindex.php" title="AZ Index"><i class="icon-a-z"></i></a>
            </span>
          </div>
      </div>  
    </div>  
  </div>
  <div class="container" id="site-header">
    <div class="row-fluid">
      <div>
        <p id="super-title" class="left"><a href="http://cag.uconn.edu/">College of Agriculture, Health and Natural Resources</a></p>
        <h1 id="site-title"><a href="index.php">Plant Database</a></h1>
      </div>

      <div id="search-container">
        <div class="titleButtons">
             <a href="search.php">Search by Trait</a>
             <a href="list.php" style="margin-right: 0px;">List by Name</a>
           </div>
      </div>

    </div>
  </div>
</div>

  <div class="white container">
    <div class="row" id="maincontainer">
		
			
						<h1 class='latinName'>Vaccinium angustifolium</h1>
						<h2>Lowbush Blueberry</h2>
						<h3>Ericaceae</h3>
					<div id='photoStrip'><a href='plantPhotos/vacang00.jpg' target='_blank'><img src='plantPhotos/vacang00.jpg' /></a><a href='plantPhotos/vacang01.jpg' target='_blank'><img src='plantPhotos/vacang01.jpg' /></a><a href='plantPhotos/vacang02.jpg' target='_blank'><img src='plantPhotos/vacang02.jpg' /></a><a href='plantPhotos/vacang03.jpg' target='_blank'><img src='plantPhotos/vacang03.jpg' /></a><a href='plantPhotos/vacang04.jpg' target='_blank'><img src='plantPhotos/vacang04.jpg' /></a><a href='plantPhotos/vacang05.jpg' target='_blank'><img src='plantPhotos/vacang05.jpg' /></a><a href='plantPhotos/vacang06.jpg' target='_blank'><img src='plantPhotos/vacang06.jpg' /></a><a href='plantPhotos/vacang07.jpg' target='_blank'><img src='plantPhotos/vacang07.jpg' /></a></div><a href='#' id='photoToggle'>Expand</a>
						<div id='description'>        <P><FONT SIZE="4">Habitat</FONT></P>    <UL>  <LI>native to the northeastern United States</LI>  <LI>zone 2 </LI></UL><P><FONT SIZE="4">Habit      and Form</FONT></P>    <UL>      <LI>a deciduous, twiggy shrub</LI>      <LI>open, leggy crown</LI>      <LI>6&quot; to 2&#39; tall</LI>      <LI>2&#39; wide</LI>      <LI>medium texture</LI>      <LI>moderate growth rate</LI>    </UL>        <P> </P>        <P><FONT SIZE="4">Summer      Foliage</FONT></P>    <UL>      <LI>alternate leaf arrangement</LI>      <LI>simple, deciduous shape</LI>      <LI>lanceolate leaf shape</LI>      <LI>0.33&quot; to .75&quot; long</LI>      <LI>serrulate leaf margins</LI>      <LI>dark green leaf color</LI>    </UL>        <P> <FONT SIZE="4">Autumn      Foliage</FONT></P>    <UL>      <LI>reddish, bronze fall color</LI>      <LI>showy</LI>    </UL>                <P><FONT SIZE="4">Flowers</FONT></P>    <UL>      <LI>white flowers</LI>      <LI>0.25&quot; diameter</LI>      <LI>blooms in May</LI>      <LI>showy</LI>    </UL>        <P>            </P>        <P><FONT SIZE="4">Fruit</FONT></P>    <UL>      <LI>bluish-black fruit</LI>      <LI>sweet and edible</LI>      <LI>up to 0.5&quot; across</LI>        <LI>matures in mid- to late-summer</LI>    </UL>                <P><FONT SIZE="4">Bark</FONT></P>    <UL>      <LI>reddish stems</LI>    </UL>                <P><FONT SIZE="4">Culture</FONT></P>    <UL>      <LI>ideal soil is moist, high in organic matter and well-drained</LI>      <LI>pH must be very acidic (4.5 to 5.5)</LI>      <LI>mulch around the roots</LI>      <LI>sandy soils with adequate moisture support good growth</LI>      <LI>full sun to partial shade</LI>      <LI>more sun translates into more into more blooms, more fruit and        enhanced fall foliage color</LI>    </UL>        <P><FONT SIZE="4">Landscape      Use</FONT></P>    <UL>      <LI>for edible fruit</LI>      <LI>to attract wildlife</LI>      <LI>excellent for fall color</LI>      <LI>useful in a shrub border</LI>      <LI>naturalized plantings </LI>    </UL>        <P><FONT SIZE="4">Liabilities</FONT></P>    <UL>      <LI>iron chlorosis on high pH soils</LI>    </UL>        <P><FONT SIZE="4">ID      Features</FONT></P>    <UL>      <LI>alternate leaf arrangement</LI>      <LI>small urn-shaped flowers in May</LI>      <LI>blue-black fruits in summer</LI>      <LI>red fall color </LI>    </UL>        <P><FONT SIZE="4">Propagation</FONT></P>    <UL>      <LI>by seed</LI>      <LI>by cuttings </LI>    </UL>        <P><FONT SIZE="4">Cultivars/Varieties</FONT></P><P>This plant is an important managed wild fruit crop in northern New England.   It also has ornamental merit, but few selections have been made for any purpose.   Listed below are several cultivars that are only limitedly available.</P><P><b>&#39;Burgundy&#39;</b>, <b>&#39;Claret&#39;</b> and <b>&#39;Jonesboro&#39;</b> - These are selected   University of Maine forms notable for good red fall color. They grow low, under   12&quot; tall.</P><P><b>&#39;Cumberland&#39;</b> and <b>&#39;Fundy&#39;</b> - This pair of cultivars was released   by the Canadian Government for their high quality, large fruit that have a sweet   flavor.</P><P><b>&#39;North Country&#39;</b> - This plant grows 2&#39; tall with sweet, mild fruit and   brilliant red fall color.</P><P><b>&#39;Northsky&#39;</b> - A handsome selection with lustrous leaves that turn red   in fall, this 18&quot; tall plant bears small, blue, flavorful fruit.</P><P><b>&#39;Pretty Yellow&#39;</b>, <b>&#39;Spring&#39;</b> and<b> &#39;Verde&#39;</b> - Selections out   of the University of Maine, this trio grows under 12&quot; tall and bear yellowish   fall color.</P><P><b>&#39;Tophat&#39;</b> - This is a popular selection that bears fruit without a pollinator   and grows to a height under 2&#39;. It is suitable for containers and small gardens.</P></div>
					<script type="text/javascript">document.title = "Vaccinium angustifolium, Lowbush Blueberry - Plant Database - University of Connecticut";</script>		
		
		<link rel="stylesheet" href="css/main.css" type="text/css" charset="utf-8">
		<script type="text/javascript" src="js/detail.js"></script>
	</div>
  </div>
  
  
  <div class="container">
	<div class="row PDfoot"> 
<div class="col-md-12">
<p>&copy; Copyright Mark H. Brand, 1997-2015.</p>
 <p>The digital materials (images and text) available from the UConn Plant Database are protected by copyright. Public use via the Internet for non-profit and educational purposes is permitted. Use of the materials for profit is prohibited.</p>
 <p>Citation and Acknowledgements: University of Connecticut Plant Database, <a href="http://www.hort.uconn.edu/plants">http://hort.uconn.edu/plants</a>, Mark H. Brand, Department of Plant Science and Landscape Architecture, Storrs, CT 06269-4067 USA.</p>
</div>
</div>  </div>
<div class=" container UCfoot">
    <div class="container">
  	<div id="footer">
    	© <a href="http://uconn.edu/">University of Connecticut</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://uconn.edu/disclaimers-and-copyrights.php" id="disHide">Disclaimers, Privacy & Copyright</a>    
    </div>
  </div>
</body>
</html></div>