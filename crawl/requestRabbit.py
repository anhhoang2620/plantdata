import pika

credentials = pika.PlainCredentials('guest', 'guest')
parameters = pika.ConnectionParameters(host='localhost', port=5672, credentials=credentials)

def requestRa(requestBody):
    connection = pika.BlockingConnection(parameters=parameters)
    channel = connection.channel()
    channel.basic_publish(exchange="", routing_key="resizeImage", body=requestBody)
    connection.close()

# request("../dataPlant/usefulData/species/Abelia chinensis")
# import glob
# for species in glob.glob("../dataPlant/usefulData/species/*"):
#     request(species)