import json
import numpy as np
import outcome
import pandas as pd

def summarizeNameSpecies():
    dfweeds = json.load(open("listSpecies weeds.json"))
    dfuseful = json.load(open("listSpecies useful.json"))
    dfamerican = json.load(open("listSpecies namerica.json"))
    listName = []
    for species in dfweeds+dfuseful+dfamerican:
        listName.append(species["name"])
    listName = sorted(list(set(listName)))
    print(len(dfweeds))
    print(len(dfuseful))
    print(len(dfamerican))
    print(len(listName))
    with open("summarize species.txt", "w") as outfile:
        for name in listName:
            outfile.write(f'{name}\n')
        outfile.close()

if __name__=="__main__":
    summarizeNameSpecies()