import os
from reprlib import recursive_repr
from urllib import response
import pandas as pd
import json
import requests
from bs4 import BeautifulSoup
from multiprocessing import Pool, Manager
import pickle
import numpy as np
import glob
listSpeciesDes = Manager().list()
listSPeciesError = Manager().list()
#https://pfaf.org/User/Plant.aspx?LatinName=Cirsium+palustre
def getDesWeed(name):
    url = f'https://pfaf.org/user/Plant.aspx?LatinName={name.replace(" ","+")}'
    print(url)
    payload = ""
    headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.64 Safari/537.36',
        'Cookie': 'ASP.NET_SessionId=pdjindlv2bhtozzdophksmkv',
            
    }
    respone = requests.request("get", url, headers=headers, data=payload)
    soup = BeautifulSoup(respone.content, "html.parser")
    table_info = soup.find("table", {"class" : "table table-hover table-striped"})
    spec = {}
    spec.update({"Scientist Name" : name})
    for row_in_table in table_info.find_all("tr"):
        title, properties = row_in_table.findAll("td")[0].text.replace("\n",""), row_in_table.findAll("td")[1] 
        if title == "Care (info)":
            title = "Care"
            properties = [img["title"] for img in properties.findAll("img")]
        else:
            properties = properties.text.replace("\n", "").replace(u'\xa0', u"")
        spec.update({title : properties})
    container = soup.select('div[class="container"]')[4]
    for span in container.select('span[id*="ContentPlaceHolder1_"]'):
        if span["id"] == "ContentPlaceHolder1_lblPhystatment":
            spec.update({"Description" : span.text})
        if span["id"] == "ContentPlaceHolder1_lblSynonyms":
            spec.update({"Synonyms" : span.text})
        if span["id"] == "ContentPlaceHolder1_lblhabitats":
            spec.update({"Habitats" : span.text})
        if span["id"] == "ContentPlaceHolder1_txtEdibleUses":
            spec.update({"Edible Uses" : span.text})
        if span["id"] == "ContentPlaceHolder1_txtMediUses":
            # print(span.string)
            text = " ".join(span.find_all(text=True,recursive=False)).lstrip().rstrip()
            spec.update({"Medicinal Uses": text})
        if span["id"] == "ContentPlaceHolder1_txtOtherUses":
            text = " ".join(span.find_all(text=True,recursive=False)).lstrip().rstrip()
            spec.update({"Other Uses": text})
        if span["id"] == "ContentPlaceHolder1_txtCultivationDetails":
            spec.update({"Cultivation" : span.text})
    listSpeciesDes.append(spec)
    print("Success {}".format(url))
def desWeed():
    ""
    df = json.load(open("weeds_data/pretty_weeds_species.json"))
    listName = [species["name"] for species in df]
    p = Pool(20)
    p.map(getDesWeed, listName)
    # for species in df:
    #     name = species["name"].replace(" ", "+")
    #     getDesWeed(f'https://pfaf.org/user/Plant.aspx?LatinName={name}')
    # getDesWeed("https://pfaf.org/user/Plant.aspx?LatinName=Adonis+aestivalis")
    with open("weed_des.json", "w") as outfile:
        outfile.write(json.dumps(list(listSpeciesDes), indent=4))

#https://plants.ces.ncsu.edu/plants/abeliophyllum-distichum/
def getListPlant():
    ""
    listSpecies = []
    for i in range(97):
        res = requests.request("get", f'https://plants.ces.ncsu.edu/find_a_plant/?page={i+1}')
        soup = BeautifulSoup(res.content, "html.parser")
        main = soup.find("main")
        for h2 in main.find_all("h2"):
            listSpecies.append(h2.text)

    with open("listSpeciesPlants_ces_ncsu_edu.json", "w") as outfile:
        outfile.write(json.dumps(listSpecies, indent=4))
        outfile.close()
def crawlDesSpecies(name):
    officialName = name.replace("\n", "")
    name = name.replace("\n", "").replace("  ", " ").replace(" ", "-").replace("'", "").replace("(", "").replace(")","").lower()
    try:
        
        res = requests.request("get", f'https://plants.ces.ncsu.edu/plants/{name}/')
        soup = BeautifulSoup(res.content, "html.parser")
        main = soup.find("main")
        common_name_space = main.find("div", {"class" : "common_name_space"})
        info_1 = main.find("div", {"class" : "info_1"})
        info_2 = main.find("div", {"class" : "info_2"})
        common_names = []
        des = ""
        attribute = {}
        if common_name_space != None:
            common_names = [li.text for li in common_name_space.find_all("li")]   
        if info_1 != None:
            for dd in info_1.find_all("dd"):
                if dd.find_previous_sibling("dt").text == "Description":
                    des = dd.text
        if info_2 != None:
            for ul in info_2.find_all("ul", {"class" : "list-group brick"}):
                groupName = ul.find("span", {"class" : "group_name"}).text[:-1]
                att = {}
                for dt in ul.find_all("dt"):
                    att.update({dt.text[:-1]:""})
                for dd in ul.find_all("dd"):
                    nameAtt = dd.find_previous_sibling("dt").text[:-1]
                    text = dd.text.replace("\n", "")
                    text = f'{att[nameAtt]}++{text}'
                    att.update({nameAtt:text})
                attribute.update({groupName:att})
        properties = {
            "commmonName" : " ".join(common_names),
            "Description" : des,
            "Properites" : attribute
        }
        with open(f'../dataPlant/description/{officialName}.json', "w") as outfile:
            outfile.write(json.dumps(properties, indent=4))
            outfile.close()


    except Exception as e:
        print(e)
        listSPeciesError.append(name)
def crawlDes():
    df = json.load(open("listSpeciesPlants_ces_ncsu_edu.json", "r"))
    # for species in df:
    p = Pool(20)
    p.map(crawlDesSpecies, df)
    with open("error.txt", "w") as out:
        for error in listSPeciesError:
            out.write(f'{error}\n')
        out.close()

#https://www.rhs.org.uk/plants/search-results?pageSize=20&startFrom=40
def crawlListSpeciesRHS():
    ""
    url = "https://plants.rhs.org.uk/api/Plant/AdvanceSearch"
    df = []

    headers = {
    'Content-Type': 'application/json'
    }
    start = 0
    while (start  <= 9900):
        print(start)
        payload = json.dumps({
            "startFrom":start,
            "pageSize":100,
            "includeAggregation":False,
            "colourWithAttributes":[],
            "sunlight":[],
            "aspect":[],
            "soilType":[],
            "sortBy": 2,
            "moisture" : [],
            "pHTypes" : ["1"],
            "sortIsAscending":True
        })
        response = requests.request("POST", url, headers=headers, data=payload)
        start += 100
        df = df +  response.json()["hits"]
        np_df = np.array(df)
        with open("rhsSpecies_pH_1_0.pkl", "wb") as outfile:
            pickle.dump(np_df, outfile)
            outfile.close()
    
    start = 0
    df = []
    while (start  <= 9900):
        print(start)
        payload = json.dumps({
           "startFrom":start,
            "pageSize":100,
            "includeAggregation":False,
            "colourWithAttributes":[],
            "sunlight":[],
            "aspect":[],
            "soilType":[],
            "sortBy": 2,
            "moisture" : [],
            "pHTypes" : ["2"],
            "sortIsAscending":True
        })
        response = requests.request("POST", url, headers=headers, data=payload)
        start += 100
        df = df +  response.json()["hits"]
        np_df = np.array(df)
        with open("rhsSpecies_pH_2_0.pkl", "wb") as outfile:
            pickle.dump(np_df, outfile)
            outfile.close()

    start = 0
    df = []
    while (start  <= 9900):
        print(start)
        payload = json.dumps({
           "startFrom":start,
            "pageSize":100,
            "includeAggregation":False,
            "colourWithAttributes":[],
            "sunlight":[],
            "aspect":[],
            "soilType":[],
            "sortBy": 2,
            "moisture" : [],
            "pHTypes" : ["3"],
            "sortIsAscending":True
        })
        response = requests.request("POST", url, headers=headers, data=payload)
        start += 100
        df = df +  response.json()["hits"]
        np_df = np.array(df)
        with open("rhsSpecies_pH_3_0.pkl", "wb") as outfile:
            pickle.dump(np_df, outfile)
            outfile.close()
def summarizeSpeciesRHS():
    pickleFiles = glob.glob("*.pkl")
    species = np.array([])
    for file in pickleFiles:
        subSpecies = pickle.load(open(file, "rb"))
        species = np.append(species, subSpecies)
    species = list({v["id"] : v for v in species}.values())
    with open("summary_Species_RHS.pkl", "wb") as outfile:
        pickle.dump(species, outfile)
def crawlOneSpeciesRHS(url):
    id = url.split("/")[-3]
    resp = requests.request("GET", url)
    if resp.status_code == 200:
        with open("../../dataPlant/RHS_Description/{}.html".format(id), "wb") as outfile:
            outfile.write(resp.content)
            outfile.close()
        print("Succces {}".format(url))
    else:
        print("Failed {}".format(url))
def crawlErrorSpeciesRHS():
    listSpecies = pickle.load(open("errorSpecies.pkl", "rb"))
    for species in listSpecies:
        soup = BeautifulSoup(species["botanicalName"], "html.parser")
        name = soup.text.replace("'", "-").replace(" ", "-").replace(".", "").replace("&", "").replace("+", "").replace("/","-")
        urlSpecies_0 = f'https://www.rhs.org.uk/plants/{species["id"]}/{name}/details'
        crawlOneSpeciesRHS(urlSpecies_0)
def processInfoOneSpeciesRHS(htmlFile):
    soup = BeautifulSoup(open(htmlFile), "html.parser")
    if soup.find("h1", {"class" : "h1--alt"}) != None:
        name = soup.find("h1", {"class" : "h1--alt"}).text
        if soup.find("p", {"class" : "ng-star-inserted"}) != None:
            description = soup.find("p", {"class" : "ng-star-inserted"}).text
        else :
            description = ""
        properties = {}
        if soup.find("div", class_="flag__body u-w-auto") != None:
            properties.update({"Position" : soup.find("div", class_="flag__body u-w-auto").text})
        i = 1
        for panel in soup.find_all("div", class_="plant-attributes__panel"):
            if i != 3:
                namePanel = panel.find("h6", {"class" : "plant-attributes__heading"}).text
                for plat_body in panel.find_all("div", {"class" : "l-module"}):
                    tag_namePro = plat_body.find("h6")
                    if plat_body.find("div", {"class" : "flag__body"}) != None:
                        valuePro = plat_body.find("div", {"class" : "flag__body"}).find(text=True, recursive=False)
                    else :
                        if plat_body.find("span") != None:
                            valuePro = plat_body.find("span", recursive=False).text
                        else :
                            valuePro = ""
                    if tag_namePro != None:
                        namePro = tag_namePro.find(text=True, recursive=False)
                        properties.update({namePro : valuePro})
                    else :
                        if namePanel in properties.keys():
                            properties.update({namePanel : properties[namePanel] + ", " + valuePro})
                        else :
                            properties.update({namePanel : valuePro})
            i += 1
        # content = soup.find("div", class_="content")
        # print(content)
        print(name, description)
        print(properties)
    
def crawlRHS():
    # crawlListSpeciesRHS()
    # summarizeSpeciesRHS()
    # listSpecies = pickle.load(open("errorSpecies.pkl", "rb"))
    # succesSpecies = [os.path.basename(species)[:-5] for species in glob.glob("../../dataPlant/RHS_Description/*")]
    # errorSpecies = np.array([])
    # for species in listSpecies:
    #     # name = species["botanicalName"].replace("<em>", "").replace("</em>", "").replace("  ", " ").replace("'", "").replace(" ", "-").replace("/", "-").lower()
    #     # urlSpecies_0 = f'https://www.rhs.org.uk/plants/{species["id"]}/{name}/details'
    #     # crawlOneSpeciesRHS(urlSpecies_0)
    #     if str(species["id"]) not in succesSpecies:
    #         errorSpecies = np.append(errorSpecies, species)
    # # print(errorSpecies)
    # with open("errorSpecies.pkl", "wb") as outfile:
    #     pickle.dump(errorSpecies, outfile)
    #     outfile.close()
    # print(errorSpecies)
    # print(len(errorSpecies))
    # crawlErrorSpeciesRHS()
    for htmlFile in glob.glob("../../dataPlant/RHS_Description/*"):
        print(htmlFile)
        processInfoOneSpeciesRHS(htmlFile)
    # processInfoOneSpeciesRHS("../../dataPlant/RHS_Description/74514.html")
# https://plants.ces.ncsu.edu/plants/camellia-japonica/
def processLeftTaskBar(left_taskbar):
    ""
    contents = {}
    contentName = ""
    contentDetail = []
    for tag in left_taskbar.children:
        if tag.name == "dt":
            if len(contentName) != 0:
                contents.update({contentName : contentDetail})
            contentName = tag.find(text=True, recursive=False).strip()
            contentDetail = []
        else :
            text = tag.text.strip()
            if len(text) != 0 :
                contentDetail.append(text)
    contents.update({contentName : contentDetail})
    return contents
def processInfo_1(info_1):
    ""
    for dd in info_1.find_all("dd"):
        dt = dd.find_previous_sibling("dt")
        if dt.text == "Description":
            return {"Description" : dd.text}
    return {"Description" : ""}
def processCommonName(common_name):
    ""
    try : 
        listCommonName = []
        for li in common_name.find_all("li"):
            listCommonName.append(li.text.strip())
        return {"Common Name" : listCommonName}
    except:
        return {"Common Name" : ""}
def processTable(table):
    ""
    contents = {}
    for dl in table.find_all("dl"):
        groupName = dl.find("span", {"class" : "group_name"}).text
        contentTable = {}
        contentName = ""
        contentDetail = []
        for tag in dl.children:
            if tag.name in ["dt", "dd"]:
                if tag.name == "dt":
                    if len(contentName) > 0:    
                        contentTable.update({contentName : contentDetail})
                    contentName = tag.text[:-1].strip()
                    contentDetail = []
                else :
                    text = tag.text.strip()
                    if len(text) != 0 :
                        contentDetail.append(text)
        contents.update({groupName : contentTable})
    return contents
def crawlDesOneSpecies(url):
    ""
    url = url[:-1]
    name = os.path.basename(url)
    print(name)
    resp = requests.request("GET", f'https://plants.ces.ncsu.edu/{url}')
    soup = BeautifulSoup(resp.content, "html.parser")
    left_taskbar = soup.find("dl", {"class" : "list-group"})
    info_1 = soup.find("main").find("div", {"class" : "info_1"})
    table = soup.find("div", {"class" : "bricks"})
    common_name = soup.find("main").find("ul", {"id" : "common_names"})
    content = {}
    content.update(processLeftTaskBar(left_taskbar=left_taskbar))
    content.update(processInfo_1(info_1=info_1))
    content.update(processCommonName(common_name))
    content.update(processTable(table=table))
    with open(f'../../dataPlant/PlantCesDescription/Common_Name/{name}.json', "w") as file:
        file.write(json.dumps(content, indent=4))
        file.close()
def crawlDesSpeciesPlantCes(p):
    with open("../json/plant_ces_commomname.txt", "r") as file:
        listUrlSpecies = file.read().splitlines()
        file.close()
    listUrlSpecies = sorted(listUrlSpecies)
    p.map(crawlDesOneSpecies, listUrlSpecies)
    # crawlDesOneSpecies(listUrlSpecies[0])

# Picture This 
def processTaxonomyPicture(taxonomy):
    taxo = {}
    for item in taxonomy.find_all("div", {"class" : "grid_item"}):
        taxoLabel = item.find("div", {"class" : "grid_item_label"}).text
        taxoName = item.find("div", {"class" : "grid_item_text"}).text
        taxo.update({taxoLabel : taxoName})
    return {"Taxonomy" : taxo}
def processDetailContent(detail_content):
    content = {}
    for detail_content_field in detail_content.find_all("div", {"class" : "detail_content_field"}):
        ""
    return content
def crawlDesOneSpeciesPicture(info):
    ""
    latinName, commonName, synonyms = info[0], info[1], info[2]
    url = f'https://www.picturethisai.com/wiki/{latinName.replace(" ", "_")}.html'
    resp = requests.request("GET", url)
    soup = BeautifulSoup(resp.content, "html.parser")
    detail_content = soup.find("div", {"class" : "name_detail"}).find("div", {"class" : "detail_content"})
    taxonomy = soup.find("div", {"class" : "name_detail"}).find("div", {"class" : "detail_description"}).find("div", {"class" : "taxonomy"})
    content = {
        "commonNames" : commonName,
        "synonyms" : synonyms
    }
    content.update(processDetailContent(detail_content=detail_content))
    content.update(processTaxonomyPicture(taxonomy=taxonomy))
    with open(f'../../dataPlant/PictureThis/{latinName}.json', "w") as file:
        file.write(json.dumps(content, indent=4))
        file.close()
def crawlDesSpeciesPicture(p):
    ""
    df_species = json.load(open("../json/ListSpecies PictureThis.json"))
    listInfoSPecies = [(species["latinName"], species["commonNames"], species["synonyms"]) for species in df_species]
    # p.map(crawlDesOneSpeciesPicture, listInfoSPecies)
    crawlDesOneSpeciesPicture(listInfoSPecies[0])
if __name__ == "__main__":
    # desWeed()

    #https://plants.ces.ncsu.edu/plants/abeliophyllum-distichum/
    # crawlDes()
    crawlRHS()
    p = Pool(25)
    # crawlDesSpeciesPlantCes(p)
    # crawlDesSpeciesPicture(p)