from base64 import encode
from contextlib import nullcontext
from textwrap import indent
import numpy as np
from bs4 import BeautifulSoup
import requests
import pandas as pd
import json
# from crawl_sele import *
from multiprocessing import Pool
import os
import urllib
import glob
from requestPlantNet import requestPlant
from requestRabbit import *
session = requests.Session()
def crawlListFruits():
    list_fruits = []
    res = requests.get("https://www.halfyourplate.ca/fruits-and-veggies/fruits-a-z/")
    soup = BeautifulSoup(res.content, "html.parser")
    all_content = soup.find("div", "left-column")
    i = 0
    for a_tag in all_content.find_all("a"):
        if i%2 == 1:
            list_fruits.append(a_tag.text)
        i += 1
    fruitFrame = pd.DataFrame({"name" : list_fruits})
    fruitFrame.to_csv("listFruits.csv", header=None, index=False)
    # print(list_fruits)

def crawlFruits():
    ""
    crawlListFruits()
def crawlImageSpecies(info):
    local, url = info[0], info[1]
    try:
        urllib.request.urlretrieve(url, local)
    except Exception as e:
        print(url)
def crawlThumbImageName(info):
    ""
    thumb, icon, url_thumb, url_icon = info[0], info[1], info[2], info[3]

    urllib.request.urlretrieve(url_thumb, thumb)
    urllib.request.urlretrieve(url_icon, icon)
def crawlThumbFamilyGenera(path_family_json, path_genera_json, group):
    os.mkdir(f'../dataPlant/{group}/family')
    os.mkdir(f'../dataPlant/{group}/genera')
    df_families =  json.load(open(path_family_json, "r"))
    list_info_family_crawls = [(f'../dataPlant/{group}/family/{family["name"]}_thumb.jpg', f'../dataPlant/{group}/family/{family["name"]}_icon.jpg', family["image"]["o"], family["image"]["s"]) for family in df_families]
    df_generas =  json.load(open(path_genera_json, "r"))
    list_info_genera_crawls = [(f'../dataPlant/{group}/genera/{genera["name"]}_thumb.jpg', f'../dataPlant/{group}/genera/{genera["name"]}_icon.jpg', genera["image"]["o"], genera["image"]["s"]) for genera in df_generas]
    p = Pool(20)
    p.map(crawlThumbImageName, list_info_family_crawls+list_info_genera_crawls)
def crawlJsonHierachy(group):

    listFamilies = requestPlant(f'https://api.plantnet.org/v1/projects/{group}/families?pageSize=10000')
    with open(f'listFamilies {group}.json', "w") as outfile:
        outfile.write(json.dumps(listFamilies, indent=4))
        outfile.close()
    listGeneras = requestPlant(f'https://api.plantnet.org/v1/projects/{group}/genera?pageSize=100000')
    with open(f'listGeneras {group}.json', "w") as outfile:
        outfile.write(json.dumps(listGeneras, indent=4))
        outfile.close()
    listSpecies = requestPlant(f'https://api.plantnet.org/v1/projects/{group}/species?pageSize=100000')
    with open(f'listSpecies {group}.json', "w") as outfile:
        outfile.write(json.dumps(listSpecies, indent=4))
        outfile.close()
def getJsonSpecies(info_crawl):
        group, url, name = info_crawl[0], info_crawl[1], info_crawl[2]
        print(url)
        response = requestPlant(url)
        with open(f'../dataPlant/{group}/api/{name}.json', "w") as outfile:
            outfile.write(json.dumps(response, indent=4))
            outfile.close()
def apiSpecies(group, path_species_json):
    df_species = json.load(open(path_species_json, "r"))
    os.mkdir(f'../dataPlant/{group}/api')
    list_info_crawls = []
    for species in df_species:
        if species["author"] is None:
            url = f'https://api.plantnet.org/v1/projects/{group[:-4]}/species/{species["name"]}'
        else:
            url = f'https://api.plantnet.org/v1/projects/{group[:-4]}/species/{species["name"]} {species["author"]}'
        list_info_crawls.append((group, url, species["name"]))
    p = Pool(20)
    p.map(getJsonSpecies, list_info_crawls)

def crawlImageDataset(group):
    print(group[:-4])
    listSpeciesInS3 = []
    # with open("listSpeciesInS3.txt", "r") as file:
    #     listSpeciesInS3 = file.read().splitlines()
    listSpeciesInS3 = set(listSpeciesInS3)
    os.mkdir(f'../dataPlant/{group}')
    crawlJsonHierachy(group[:-4])
    crawlThumbFamilyGenera(f'listFamilies {group[:-4]}.json', f'listGeneras {group[:-4]}.json', group)
    apiSpecies(group, f'listSpecies {group[:-4]}.json')
    listSpecies = [species for species in  glob.glob(f'../dataPlant/{group}/api/*') if os.path.basename(species)[:-5] not in listSpeciesInS3]
    listSpecies = sorted(listSpecies)
    print(len(listSpecies))
    p = Pool(30)
    if not os.path.exists(f'../dataPlant/{group}/species'):
        os.mkdir(f'../dataPlant/{group}/species')
    index = 0
    for species in listSpecies:
        print(species)
        print(index)
        with open(species, "r") as file:
            df_species = json.load(file)
            file.close()
        os.mkdir(f'../dataPlant/{group}/species/{df_species["species"]["name"]}')
        images = df_species["images"]
        
        list_images = []
        for subimage in images:
            os.mkdir(f'../dataPlant/{group}/species/{df_species["species"]["name"]}/{subimage}')
            for img in images[subimage][:(min(200, len(images[subimage]) - 1))]:
                list_images.append((f'../dataPlant/{group}/species/{df_species["species"]["name"]}/{subimage}/{img["id"]}', img["o"]))
        p.map(crawlImageSpecies, list_images)
        requestRa(f'../dataPlant/{group}/species/{df_species["species"]["name"]}')
        print("Success {}".format(species))
        index += 1
def crawlLessImage():
    df = json.load(open("Summary_Final_Species_Less_image.json", "r"))
    for species in df:
        print(species["name"])
if __name__=="__main__":
    # crawlFruits()
    # crawlImageName("weeds_data/api/Abutilon theophrasti.json")
    # crawlWeedImagePlaneNet()
    # crawlUseFulePlant()

    # usefulData
    # crawlImageDataset("usefulData")
    # USA data
    # crawlImageDataset("usefulData")
    #canada
    # crawlImageDataset("canadaData")
    #amazon
    # crawlImageDataset("guyaneData")
    # colombia
    # crawlImageDataset("colombiaData")
    # west European
    # crawlImageDataset("weuropeData")
    # North Africa
    # crawlImageDataset("afnData")
    # central-america
    # crawlImageDataset("central-americaData")
    # martinique
    # crawlImageDataset("martiniqueData")
    # Tropical Africa
    # crawlImageDataset("aftData")
    # # Réunion
    # crawlImageDataset("reunionData")
    # Mauritius
    # crawlImageDataset("mauriceData")
    # Japanese
    # crawlImageDataset("japanData")
    # Nepal
    # crawlImageDataset("nepaldata")
    # Malaysia 
    # crawlImageDataset("malaysiaData")
    crawlLessImage()