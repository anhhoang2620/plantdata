import boto3
import json
import numpy as np 
from PIL import Image
import io
from multiprocessing import Pool
import os

s3 = boto3.client('s3', aws_access_key_id='AKIAQ2Z7QFBSXS7EFHWN', aws_secret_access_key='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')
paginator = s3.get_paginator('list_objects')

def make_square(im, min_size=384, fill_color=(0, 0, 0)):
    x, y = im.size
    size = max(x, y)
    new_im = Image.new('RGB', (size, size), fill_color)
    new_im.paste(im, (int((size - x) / 2), int((size - y) / 2)))
    new_im = new_im.resize((min_size, min_size), Image.Resampling.LANCZOS)
    return new_im
def downloadImage(info):
    ""
    # print(info)
    try:
        object, folder = info[1], info[0]
        new_obj = s3.get_object(Bucket="data-raw-identifier", Key=object)
        image_dl = new_obj['Body'].read()
        image = Image.open(io.BytesIO(image_dl))
        image = make_square(image)
        image.save(f'/mnt/89bd7cdc-f33c-4124-b444-df205e725bcd/plantTrain/data/{folder}/{os.path.basename(object)}.jpg')
    except Exception as e:
        print(info)
def download(info):
    name = info[0]
    region = info[1]
    operations = {
    "Bucket":"data-raw-identifier",
    "Prefix":f'dataPlant/{region}/species/{name}/'
    }
    result = paginator.paginate(**operations)
    listSpecies = []
    try:
        for page in result:
            for obj in page['Contents']:
                listSpecies.append((name, obj["Key"]))
        if not os.path.exists(f'/mnt/89bd7cdc-f33c-4124-b444-df205e725bcd/plantTrain/data/{name}'):
            os.mkdir(f'/mnt/89bd7cdc-f33c-4124-b444-df205e725bcd/plantTrain/data/{name}')
        p = Pool(15)
        p.map(downloadImage, listSpecies)
        print("SUCESS {}".format(name))
    except Exception as e:
        print("Failed {}".format(info))
    
if __name__ == "__main__":
    df = json.load(open("Summary_Final_Species.json"))
    for species in df:
        if species["Region"] not in ["namerica", "useful", "weeds"]:
            download((species["name"], f'{species["Region"]}Data'))
