import pika
import time
import json
import os
import glob
import boto3
from multiprocessing import Pool

credentials = pika.PlainCredentials('guest', 'guest')
parameters = pika.ConnectionParameters(host='localhost', port=5672, credentials=credentials, heartbeat=600, blocked_connection_timeout=300)
s3 = boto3.client('s3', aws_access_key_id='AKIAQ2Z7QFBSXS7EFHWN', aws_secret_access_key='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')


def uploadFileToS3(info):
    path, nameOnS3 = info[0], info[1]
    s3.upload_file(path, "data-raw-identifier", nameOnS3, ExtraArgs={'ContentType': "image.jpeg"})

def on_receive_message(ch, method, props, body):
    data = body.decode('utf8')
    print(data)
    folder_root = "/".join(data.split("/")[-4:])
    listimages = [(image, f'{folder_root}/{"/".join(image.split("/")[-2:])}') for image in glob.glob(f'{data}/*/*')]
    p = Pool(20)
    p.map(uploadFileToS3, listimages)
    ch.basic_ack(delivery_tag=method.delivery_tag)
    os.system('rm -r "{}"'.format(data))
def consumer():
    try:
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.basic_qos(prefetch_count=1)
        channel.basic_consume('uploadS3', on_receive_message)
        print('Start receive message...')
        try:
            if channel:
                channel.start_consuming()
        except KeyboardInterrupt:
            channel.stop_consuming()
        if connection:
            connection.close()
    except Exception as e:
        print("Caught exception: %s" % repr(e))
        time.sleep(30)
        consumer()


consumer()