from google.cloud import storage
import json
import numpy as np 
from PIL import Image
import io
from multiprocessing import Pool
import os
from requestRabbit import *
client = storage.Client()
bucket = client.get_bucket("data-raw-identifier")
blobs = bucket.list_blobs(prefix = "dataPlant/weedsData/species/", delimiter='/')
[b for b in blobs]
prefixes = list(blobs.prefixes)
print(prefixes)
def downloadImage(prefix):
    blobs = bucket.list_blobs(prefix = prefix)
    for blob in blobs:
        image_name = os.path.basename(blob.name) + ".jpg"
        species_name = blob.name.split("/")[-3]
        blob.download_to_filename(f'../dataPlant/data/{species_name}_{image_name}')
        requestRa(f'../dataPlant/data/{species_name}_{image_name}')


p = Pool(20)
p.map(downloadImage, prefixes)

    # listSpecies.append((blob, image_name, species_name))