from subprocess import list2cmdline
from google.cloud import storage
import json
import numpy as np 
from PIL import Image
import io
from multiprocessing import Pool, Manager
import os
import pandas as pd

manager = Manager()
listImages = manager.list()
client = storage.Client()
bucket = client.get_bucket("data-raw-identifier")
blobs = bucket.list_blobs(prefix = "diseaseImage/plant-identifier/diseaseImage/", delimiter='/')
[b for b in blobs]
prefixes = list(blobs.prefixes)
print(prefixes)
def downloadImage(prefix):
    blobs = bucket.list_blobs(prefix = prefix)
    for blob in blobs:
        label = blob.name.split("/")[-2]
        listImages.append((f'gs://data-raw-identifier/{blob.name}', label))


p = Pool(20)
p.map(downloadImage, prefixes)
images = [image[0] for image in listImages]
labels = [image[1] for image in listImages]

df = pd.DataFrame({"image" : images, "label" : labels})

df.to_csv("vertextJob.csv", index=None)