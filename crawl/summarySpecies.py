import pandas as pd
import numpy as np 
from multiprocessing import Pool, Manager
import json
import glob    
manager = Manager()
dictSpecies = manager.dict()
def summary(jsonSpecies):
    print(jsonSpecies)
    nameRegion = jsonSpecies.split('.')[0].split(' ')[1]
    df = json.load(open(jsonSpecies, "r"))
    for species in df:
        if species["imagesCount"] >= 20:
            dictSpecies.update({f'{species["name"]}' : nameRegion})


if __name__ == "__main__":
    listJsons = [jsonSpecies for jsonSpecies in glob.glob("listSpecies *.json")]
    p = Pool(20)
    p.map(summary, listJsons)
    finalSpecies = []
    for species in dictSpecies:
        finalSpecies.append({
            "name" : species,
            "Region" : dictSpecies[species]
        })
    with open("Summary_Final_Species.json", "w") as outfile:
        outfile.write(json.dumps(finalSpecies, indent=4))
    print(len(finalSpecies))