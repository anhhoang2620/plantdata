import torch.nn as nn 
import torch.nn.functional as F
from data import *
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import StratifiedKFold
import timm
import torch
from transformers import get_cosine_schedule_with_warmup
import os
if torch.cuda.is_available():
    DEVICE = torch.device("cuda:0")
else:
    DEVICE = torch.device("cpu")
if __name__ == "__main__":
    training_set = plantData(
        csv_file = "train.csv", 
        is_train = True, 
        augmentations = Augments.train_augments
    )
    train_DataLoader = DataLoader(training_set, batch_size=36, num_workers=4, shuffle=True, pin_memory=True)

    val_set = plantData(
        csv_file = "val.csv", 
        is_train = True, 
        augmentations = Augments.val_augments
    )
    val_DataLoader = DataLoader(val_set, batch_size=24, num_workers=3, shuffle=True, pin_memory=True)
    model = timm.create_model("vit_base_patch16_224_in21k", pretrained=True, in_chans=3, num_classes=nLabel)
    # model.head.out_features = nLabel
    model.load_state_dict(torch.load("checkpoints_1/18/model.pth"))
    model = model.to(DEVICE)
    criterion = nn.CrossEntropyLoss()
    epochs = 200
    optimizer = torch.optim.AdamW(model.parameters(), lr=0.000007, weight_decay=0.00001)
    scheduler = get_cosine_schedule_with_warmup(optimizer, 
                                            num_warmup_steps=len(train_DataLoader)*5, 
                                            num_training_steps=len(train_DataLoader)*epochs)
    max_acc_5 = 0
    for epoch in range(17, epochs):
        model.train()
        epoch_train_loss = 0
        for image, label in tqdm(train_DataLoader):
            image = image.to(DEVICE)
            label = label.to(DEVICE)
            optimizer.zero_grad()
            output = model(image)
            loss = criterion(output, label)
            loss.backward()
            optimizer.step()
            scheduler.step()
            epoch_train_loss += loss.item()
        if not os.path.exists(f'checkpoints_1/{epoch+1}'):
            os.mkdir(f'checkpoints_1/{epoch+1}')
        torch.save(model.state_dict(), f'checkpoints_1/{epoch+1}/model.pth')
        epoch_val_loss = 0
        correct_5 = 0
        all_targets = []
        all_predictions = []
        correct_1 = 0
        with torch.no_grad():
            for image, label in tqdm(val_DataLoader):
                image = image.to(DEVICE)
                label = label.to(DEVICE)
                output = model(image)
                # pred_1 = output.argmax(dim=1)
                # all_targets.extend(label.cpu().detach().numpy().tolist())
                # all_predictions.extend(torch.sigmoid(output).cpu().detach().numpy().tolist())
                # correct_1 += torch.eq(pred_1, label).sum().float().item()
                # maxk = max((1,5))
                # _, pred = output.topk(maxk, 1, True, True)
                # label_resize = label.view(-1, 1)
            
                # val_roc_auc = roc_auc_score(all_targets, all_predictions)
                # correct_5 += torch.eq(pred, label_resize).sum().float().item()
                # epoch_val_loss += loss.item()
                pred_1 = output.argmax(dim=1)
                correct_1 += torch.eq(pred_1, label).sum().float().item()
                maxk = max((1,5))
                _, pred = output.topk(maxk, 1, True, True)
                label_resize = label.view(-1, 1)
                loss = criterion(output, label)
                # print(len(gemStoneDataset_val))
                # print(pred)
                correct_5 += torch.eq(pred, label_resize).sum().float().item()
                # print(len(val_loader))
                epoch_val_loss += loss.item()

        with open(f'checkpoints_1/{epoch+1}/metric.txt', "w") as file:
            file.write(f'Train data loss : {epoch_train_loss/len(train_DataLoader):.7f} \n')
            file.write(f'Val data loss : {epoch_val_loss/len(val_DataLoader):.7f} \n')
            file.write(f'ACC_1 : {correct_1/len(val_DataLoader)*100}% \n')
            file.write(f'ACC_5 : {correct_5/len(val_DataLoader)*100}% \n')
            file.close()
        if (correct_5 > max_acc_5) :
            torch.save(model.state_dict(), 'best_model.pth')
            max_acc_5 = correct_5
        print(f'EPOCH {epoch}')
        print(f'Train data loss : {epoch_train_loss/len(train_DataLoader):.7f}')
        print(f'Val data loss : {epoch_val_loss/len(val_DataLoader):.7f}')


