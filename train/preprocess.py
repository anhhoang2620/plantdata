import cv2
from multiprocessing import Pool
import glob
import os

def recolor(info):
    index, image = info[0], info[1]
    try:
        img = cv2.imread(image)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    except Exception as e:
        print("Image Error: {}".format(image))
        os.remove(image)
def removeNoImage():
    p = Pool(20)
    images = glob.glob("/mnt/89bd7cdc-f33c-4124-b444-df205e725bcd/plantTrain/data/*/*")
    print(len(images))
    for i in range(len(images)):
        if i % 10000 == 0:
            print(i)
        recolor((i, images[i]))

removeNoImage()
