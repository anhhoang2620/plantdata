import torch
import glob
from albumentations import (
    HorizontalFlip, VerticalFlip, IAAPerspective, ShiftScaleRotate, CLAHE, RandomRotate90, Resize,
    Transpose, ShiftScaleRotate, Blur, MotionBlur, MedianBlur, OneOf, Compose, Normalize, ShiftScaleRotate, RandomCrop
)
from albumentations.pytorch import ToTensorV2
from torch.utils.data import Dataset, DataLoader
from albumentations.core.composition import Compose, OneOf
import pandas as pd
import cv2 
import numpy as np
from tqdm import tqdm
class Augments:
    train_augments = Compose([
        RandomCrop(224, 224, p=1),
        HorizontalFlip(p=0.5),
        VerticalFlip(p=0.5),
        ShiftScaleRotate(p=0.5, shift_limit=0.2, scale_limit=0.2, rotate_limit=20, border_mode=0, value=0, mask_value=0),
        RandomRotate90(),
        OneOf([
            MotionBlur(p=0.2),
            MedianBlur(blur_limit=3, p=0.1),
            Blur(blur_limit=3, p=0.1),
        ], p=0.2),
        Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
        ToTensorV2(p=1.0),
    ], p=1)

    val_augments = Compose([
        Resize(224, 224, 1),
        Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
        ToTensorV2(p=1.0)
    ], p = 1)
labels = []
dfLabels  = pd.read_csv("labels.csv")
for i in range(len(dfLabels)):
    labels.append(dfLabels.values[i,0])
nLabel = len(labels)
class plantData(Dataset):
    def __init__(self, csv_file, is_train=True, augmentations=None):
        self.df = pd.read_csv(csv_file, header=0)
        self.is_train = is_train
        self.augmentations = augmentations
    def __len__(self):
        return len(self.df)
    def __getitem__(self, index):
        path_image, label = self.df.values[index][0], int(self.df.values[index][1][1:])
        image = cv2.imread(path_image)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        if self.augmentations :
            image = self.augmentations(image=image)['image']
        return image, label

if __name__ == "__main__":
    training_set = plantData(
        csv_file = "train.csv",
        is_train = True, 
        # augmentations = Augments.train_augments
    )
    train_DataLoader = DataLoader(training_set, batch_size=62, num_workers=4, shuffle=True)
    for (image, label) in tqdm(train_DataLoader):
        i = 1