import os
import shutil
import sys
import timm
import cv2
import numpy as np 
import onnx
import torch
import tensorflow as tf 
from PIL import Image
from torchvision import transforms
from torchvision.models import *
from torchsummary import summary
from onnx_tf.backend import prepare
from data import *

def convert_torch_to_onnx(onnx_path, image_path, model=None, torch_path=None):
    """
        Coverts Pytorch model file to ONNX
        :param torch_path: Torch model path to load
        :param onnx_path: ONNX model path to save
        :param image_path: Path to test image to use in export progress
    """

    pytorch_model = model
    
    image, _, torch_image = get_example_input(image_path)

    torch.onnx.export(
        model = pytorch_model,
        args = torch_image,
        f = onnx_path,
        verbose = False,
        export_params=True,
        do_constant_folding = False,
        input_names = ['input'],
        opset_version = 10,
        output_names = ['output'])

def get_example_input(image_file):
    """
        Load image from disk and converts to compatible shape
        :param image_file: Path to single image file
        :return: Orginal image, numpy.ndarray instance image, torch.Tensor image
    """
    transform = transforms.Compose([
        transforms.Resize((224,224)),
        transforms.ToTensor(),
    ])

    image = cv2.imread(image_file)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    pil_img = Image.fromarray(image)
    torch_img = transform(pil_img)
    torch_img = torch_img.unsqueeze(0)
    torch_img = torch_img.to(torch.device("cpu"))
    print(torch_img.shape)
    return image, torch_img.numpy(), torch_img

model = timm.create_model("vit_base_patch16_224_in21k", pretrained=False, in_chans=3)
model.load_state_dict(torch.load("checkpoints/15/model.pth"))
model.head.out_features = nLabel
model.eval()

onnx_model_path ='model/model.onnx'
image_path = "test.jpg"

convert_torch_to_onnx(onnx_model_path,image_path, model=model)

def convert_onnx_to_tf(onnx_path, tf_path):
    """
        Converts ONNX model to TF 2.X saved file
        :param onnx_path: ONNX model path to load
        :param tf_path: TF model path to save
    """
    onnx_model = onnx.load(onnx_path)
    onnx.checker.check_model(onnx_model)
    tf_rep = prepare(onnx_model)  #Prepare TF representation
    tf_rep.export_graph(tf_path)  #Export the model