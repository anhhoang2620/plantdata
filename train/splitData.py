import json
import glob 
import os
import shutil
import numpy as np 
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
import pandas as pd 
def splitLessImage():
    "" 
    species = glob.glob("/mnt/89bd7cdc-f33c-4124-b444-df205e725bcd/plantTrain/data/*")
    lessSpecies = [spe for spe in species if len(glob.glob(f'{spe}/*')) < 20]
    os.mkdir("/mnt/89bd7cdc-f33c-4124-b444-df205e725bcd/plantTrain/dataLessImage")
    for spe in lessSpecies:
        shutil.move(spe, "/mnt/89bd7cdc-f33c-4124-b444-df205e725bcd/plantTrain/dataLessImage/")
def createCSVTrainTest(output_file, data, listLabels):
    labelImages = [f'h{listLabels.index(image.split("/")[-2])}' for image in data]
    df = pd.DataFrame({"image" : data, "label" : labelImages})
    df.to_csv(output_file, index=False)
def spliTrainValTest():
    listLabels = [os.path.basename(label) for label in glob.glob("/mnt/89bd7cdc-f33c-4124-b444-df205e725bcd/plantTrain/data/*")]
    df = pd.DataFrame({"labels" : listLabels})
    df.to_csv("labels.csv", index=False)
    train, val, test = [], [], []
    for label in listLabels:
        listImages = glob.glob(f'/mnt/89bd7cdc-f33c-4124-b444-df205e725bcd/plantTrain/data/{label}/*')
        sub_train, sub_test_val = train_test_split(listImages, test_size=0.2, shuffle=True)
        sub_test, sub_val = train_test_split(sub_test_val, test_size=0.5, shuffle=True)
        train = train + sub_train
        val = val + sub_val
        test = test + sub_test
    train = shuffle(train, random_state=42)
    createCSVTrainTest("train.csv", train, listLabels)
    createCSVTrainTest("val.csv", val, listLabels)
    createCSVTrainTest("test.csv", test, listLabels)
if __name__ == '__main__':
    # splitLessImage()
    spliTrainValTest()