from ast import dump
import json


def summary():
    group_articles_list = json.load(open("disease-group-of-articles-list.json"))
    list_detail_content = json.load(open("disease-articles-list-detailed-content.json"))

    set_detail_content = {}
    for content in list_detail_content:
        set_detail_content.update({content["id"] : content})

    result = []
    set_articles_list = {}
    for articles in group_articles_list:
        articles.update({"contentType" : "articles"})
        if "imageId" in articles.keys():
            del articles["imageId"]
        # if "id" in articles.keys():
        #     del articles["id"]
        for articleDescription in articles["articleDescriptions"]: 
            if "id" in articleDescription.keys():
                if articleDescription["id"] in set_detail_content.keys():
                    articleDescription.update({"detailedContent" : set_detail_content[articleDescription["id"]]["detailedContent"]})
                # del articleDescription["id"]
            if "imageId" in articleDescription.keys():
                del articleDescription["imageId"]
        set_articles_list.update({articles["id"] : articles})

    for articles in group_articles_list:
        articles.update({"contentType" : "articles"})
        if "id" in articles.keys():
            del articles["id"]
        for articleDescription in articles["articleDescriptions"]: 
            if "id" in articleDescription.keys():
                if articleDescription["id"] in set_detail_content.keys():
                    articleDescription.update({"detailedContent" : set_detail_content[articleDescription["id"]]["detailedContent"]})
                else :
                    articleDescription.update({"articleDescriptions" : set_articles_list[articleDescription["id"]]["articleDescriptions"]})
                del articleDescription["id"]
        result.append(articles)
    for content in list_detail_content:
        if "id" in content.keys():
            del content["id"]
        if "imageId" in content.keys():
            del content["imageId"]
        result.append(content)
    print(len(result))
    with open("diseases.json", "w") as outfile:
        outfile.write(json.dumps(result, indent=4))
def getNameDisease():
    df = json.load(open("diseases.json"))
    name = []
    for dis in df:
        name.append(dis["title"])
    name = sorted(name)
    with open("diseaseName.txt" ,  "w") as outfile:
        for n in name:
            outfile.write(f'{n}\n')
if __name__ == "__main__":
    # summary()
    getNameDisease()